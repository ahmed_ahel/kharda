package com.khorda;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;
import com.khorda.CustomViews.EditText;
import com.khorda.CustomViews.TextView;
import com.khorda.Models.CountriesWithCities;
import com.khorda.Models.Payment;
import com.khorda.Models.Products;
import com.khorda.Models.UserInfo;
import com.khorda.Utils.MyContextWrapper;
import com.khorda.Utils.PreferenceEditor;
import com.khorda.Utils.QuickstartPreferences;
import com.khorda.Utils.ToolsUtils;
import com.khorda.interfaces.ApiClient;
import com.khorda.interfaces.ApiInterface;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DeliveryActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private EditText fullName,
            email,
            mobileNumber,
            address;
    private TextView city, country;
    private int cityId = -1, countryId = -1;
    private Toolbar toolbar;
    private TextView toolbar_title;
    private AppCompatTextView textViewCartCounter;
    private String DataJson;
    private AppCompatImageView header_image;
    private UserInfo UserData;
    private CountriesWithCities Countries;
    private SwipeRefreshLayout swipe;


    @Override
    protected void attachBaseContext(Context newBase) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String lang = PreferenceEditor.getInstance().getStringPreference(QuickstartPreferences.CURRENT_LANGUAGE);
            super.attachBaseContext(MyContextWrapper.wrap(newBase, lang));
        }else {
            super.attachBaseContext(newBase);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery);
        header_image = (AppCompatImageView) findViewById(R.id.image);
        swipe = (SwipeRefreshLayout) findViewById(R.id.swipe);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        textViewCartCounter = (AppCompatTextView) findViewById(R.id.textViewCartCounter);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        swipe.setOnRefreshListener(this);
        getAllCartCount();

        if (ToolsUtils.getDeviceLang().equalsIgnoreCase(QuickstartPreferences.AR_LAN)) {
            header_image.setImageResource(R.drawable.rtl_ic_delivery);
        } else {
            header_image.setImageResource(R.drawable.ic_delivery);
        }


        toolbar_title.setText(R.string.delivery_title);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        fullName = (EditText) findViewById(R.id.fullName);
        email = (EditText) findViewById(R.id.email);
        mobileNumber = (EditText) findViewById(R.id.mobileNumber);
        city = (TextView) findViewById(R.id.city);
        country = (TextView) findViewById(R.id.country);
        address = (EditText) findViewById(R.id.address);


        country.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Countries != null) {
                    CountryListDialog countryListDialog = new CountryListDialog(DeliveryActivity.this);
                    countryListDialog.setCountries(Countries);
                    countryListDialog.setSelectCountry(new CountryListDialog.SelectedCountry() {
                        @Override
                        public void getSelectedCountry(CountriesWithCities.CountriesBean SelectedCountry) {
                            country.setText(SelectedCountry.getDisplay_title());
                            countryId = SelectedCountry.getId();
                        }
                    });
                    countryListDialog.show();
                } else {
                    Toast.makeText(getApplicationContext(), R.string.swipe_to_refresh, Toast.LENGTH_LONG).show();
                }
            }
        });

        city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Countries != null) {

                    if (countryId == -1) {
                        Toast.makeText(getApplicationContext(), R.string.err_msg_select_country, Toast.LENGTH_LONG).show();
                        return;
                    }

                    List<CountriesWithCities.CountriesBean.CitiesBean> citiesBeanList = new ArrayList<CountriesWithCities.CountriesBean.CitiesBean>();
                    for (CountriesWithCities.CountriesBean country : Countries.getCountries()) {
                        if (country.getId() == countryId) {
                            citiesBeanList.addAll(country.getCities());
                        }
                    }
                    System.out.println("citiesBeanList size"+citiesBeanList.size());

                    CitiesListDialog2 citiesListDialog = new CitiesListDialog2(DeliveryActivity.this);
                    citiesListDialog.setCites(citiesBeanList);
                    citiesListDialog.setSelectCity(new CitiesListDialog2.SelectedCity() {
                        @Override
                        public void getSelectedCity(CountriesWithCities.CountriesBean.CitiesBean SelectedCity) {
                            city.setText(SelectedCity.getDisplay_title());
                            cityId = SelectedCity.getId();
                        }
                    });
                    citiesListDialog.show();
                } else {
                    Toast.makeText(getApplicationContext(), R.string.swipe_to_refresh, Toast.LENGTH_LONG).show();
                }
            }
        });


        try {
            UserData = new Gson().fromJson(PreferenceEditor.getInstance().getStringPreference(QuickstartPreferences.USER_INFO), UserInfo.class);

            if (UserData != null) {
                cityId = UserData.getUser().getCountry_id().getId();
                city.setText(UserData.getUser().getCountry_id().getDisplay_title());
                fullName.setText(UserData.getUser().getFullname());
                email.setText(UserData.getUser().getEmail());
                mobileNumber.setText(UserData.getUser().getMobile());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        findViewById(R.id.check_out_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (fullName.getText().toString().isEmpty()) {
                    Toast.makeText(getApplicationContext(), R.string.err_msg_full_name, Toast.LENGTH_SHORT).show();
                    return;
                }

                if (email.getText().toString().isEmpty()) {
                    Toast.makeText(getApplicationContext(), R.string.err_msg_email, Toast.LENGTH_SHORT).show();
                    return;
                }

                if (!ToolsUtils.isValidEmail(email.getText().toString())) {
                    Toast.makeText(getApplicationContext(), R.string.err_msg_valid_email, Toast.LENGTH_SHORT).show();
                    return;
                }

                if (city.getText().toString().isEmpty()) {
                    Toast.makeText(getApplicationContext(), R.string.err_msg_city, Toast.LENGTH_SHORT).show();
                    return;
                }

                if (mobileNumber.getText().toString().isEmpty()) {
                    Toast.makeText(getApplicationContext(), R.string.err_msg_mobile, Toast.LENGTH_SHORT).show();
                    return;
                }

                if (mobileNumber.getText().toString().length() < 11) {
                    Toast.makeText(getApplicationContext(), R.string.err_msg_mobile_length, Toast.LENGTH_SHORT).show();
                    return;
                }

                if (country.getText().toString().isEmpty()) {
                    Toast.makeText(getApplicationContext(), R.string.err_msg_select_country, Toast.LENGTH_SHORT).show();
                    return;
                }

                if (city.getText().toString().isEmpty()) {
                    Toast.makeText(getApplicationContext(), R.string.err_msg_city, Toast.LENGTH_SHORT).show();
                    return;
                }


                if (address.getText().toString().isEmpty()) {
                    Toast.makeText(getApplicationContext(), R.string.err_msg_address, Toast.LENGTH_SHORT).show();
                    return;
                }

                try {
                    List<Payment> list = Payment.listAll(Payment.class);
                    if (list != null) {
                        Payment.deleteAll(Payment.class);
                    }

                    Payment p = new Payment();
                    p.setFullName(fullName.getText().toString());
                    p.setEmail(email.getText().toString());
                    p.setMobile(mobileNumber.getText().toString());
                    p.setCountryId(countryId);
                    p.setCityId(cityId);
                    p.setAddress(address.getText().toString());

                    if (DataJson != null) {
                        p.setData(DataJson);
                        p.save();
                        startActivity(new Intent(DeliveryActivity.this, PaymentActivity.class));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        requestGetCountries();

    }

    private void getAllCartCount() {
        try {
            List<Products.ProductsBean.DataBean> list =  ToolsUtils.getAllCartProducts();
            textViewCartCounter.setText("" + list.size());
            try {
                JSONArray jsonArray = new JSONArray();
                for (Products.ProductsBean.DataBean item : list) {
                    JSONObject obj = new JSONObject();
                    obj.put("product_id", item.getProduct_id());
                    obj.put("quantity", item.getSelectedQuantity());
                    jsonArray.put(obj);
                }
                DataJson = jsonArray.toString();
            } catch (Exception e) {
                e.printStackTrace();
                DataJson = null;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void requestGetCountries() {
        if (ToolsUtils.isNetworkAvailable()) {

            swipe.setRefreshing(true);

            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<CountriesWithCities> call = apiService.getCountriesWithCities();

            call.enqueue(new Callback<CountriesWithCities>() {
                @Override
                public void onResponse(Call<CountriesWithCities> call, Response<CountriesWithCities> response) {

                    System.out.println("Code" + response.code());
                    System.out.println("raw" + response.raw());
                    System.out.println("Code" + response.message());
                    swipe.setRefreshing(false);

                    if (response.code() == 200 && response.body().isStatus()) {

                        Countries = response.body();
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<CountriesWithCities> call, Throwable t) {
                    swipe.setRefreshing(false);
                    Toast.makeText(getApplicationContext(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            swipe.setRefreshing(false);
            Toast.makeText(getApplicationContext(), R.string.err_msg_no_internet_connection, Toast.LENGTH_SHORT).show();
        }
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onRefresh() {
        cityId = -1;
        city.setText("");
        requestGetCountries();
    }
}
