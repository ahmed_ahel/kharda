package com.khorda;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.khorda.Adapter.Adapter_walkthrough;
import com.khorda.CustomViews.AVLoadingIndicatorDialog;
import com.khorda.Utils.MyContextWrapper;
import com.khorda.interfaces.ApiClient;
import com.khorda.interfaces.ApiInterface;
import com.khorda.Models.WalkThrough;
import com.khorda.Utils.PreferenceEditor;
import com.khorda.Utils.QuickstartPreferences;
import com.khorda.Utils.ToolsUtils;

import java.util.ArrayList;
import java.util.List;

import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WalkthroughActivity extends AppCompatActivity {


    private ViewPager viewPager;
    private Adapter_walkthrough myViewPagerAdapter;
    private CircleIndicator indicator;
    private Button btn_get_started;
    private List<WalkThrough.SplashBean> walkthroughtItemList;
    private boolean btn_get_startedClicked =  false ;

    @Override
    protected void attachBaseContext(Context newBase) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String lang = PreferenceEditor.getInstance().getStringPreference(QuickstartPreferences.CURRENT_LANGUAGE);
            super.attachBaseContext(MyContextWrapper.wrap(newBase, lang));
        }else {
            super.attachBaseContext(newBase);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_walkthrough);
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        indicator = (CircleIndicator) findViewById(R.id.layoutDots);
        btn_get_started = (Button) findViewById(R.id.btn_get_started);

        walkthroughtItemList = new ArrayList<>();
        // making notification bar transparent
        changeStatusBarColor();
        RequestWalkThrough();
        btn_get_started.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!btn_get_startedClicked) {
                    btn_get_startedClicked = true ;
                    launchHomeScreen();
                }
            }
        });

    }


    private void RequestWalkThrough() {

        if (ToolsUtils.isNetworkAvailable()) {

            final AVLoadingIndicatorDialog dialog = ToolsUtils.CreateDialog(this, null);
            dialog.show();


            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<WalkThrough> call = apiService.getWalkThrough();

            call.enqueue(new Callback<WalkThrough>() {
                @Override
                public void onResponse(Call<WalkThrough> call, Response<WalkThrough> response) {

                    System.out.println("Code" + response.code());
                    System.out.println("raw" + response.raw());
                    System.out.println("Code" + response.message());
                    dialog.hide();

                    if (response.code() == 200 && response.body().isStatus()) {

                        if (response.body().getSplash().size() > 0) {
                            walkthroughtItemList.addAll(response.body().getSplash());
                            myViewPagerAdapter = new Adapter_walkthrough(getApplicationContext(), walkthroughtItemList);
                            viewPager.setAdapter(myViewPagerAdapter);

                            double index =  Math.ceil(new Double(response.body().getSplash().size()/2));
                            viewPager.setCurrentItem((int)index,true);
                            int margin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 60 * 2, getResources().getDisplayMetrics());
                            viewPager.setPageMargin(-margin);
                            indicator.setViewPager(viewPager);



                        } else {
                            /// Empty List
                            launchHomeScreen();
                        }

                    } else {
                        Toast.makeText(WalkthroughActivity.this, R.string.err_msg_server, Toast.LENGTH_SHORT).show();

                    }

                }

                @Override
                public void onFailure(Call<WalkThrough> call, Throwable t) {
                    dialog.hide();
                    Toast.makeText(WalkthroughActivity.this, R.string.err_msg_server, Toast.LENGTH_SHORT).show();

                }
            });

        } else {
            // ToolsUtils.ShowNoInternetConnection(this);
            startActivity(new Intent(WalkthroughActivity.this, HomeActivity.class));
            finish();
            Toast.makeText(WalkthroughActivity.this, R.string.err_msg_no_internet_connection, Toast.LENGTH_SHORT).show();

        }
    }


    private void launchHomeScreen() {
        PreferenceEditor.getInstance().setBooleanPreference(QuickstartPreferences.IS_NOT_FIRST_TIME_LAUNCH, true);
        startActivity(new Intent(WalkthroughActivity.this, HomeActivity.class));
        finish();
    }


    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.WHITE);
        }
    }


}