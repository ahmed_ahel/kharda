package com.khorda;

import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Toast;
import com.khorda.Adapter.Adapter_Colors_Dialog;
import com.khorda.CustomViews.AVLoadingIndicatorDialog;
import com.khorda.Models.Colors;
import com.khorda.Utils.ToolsUtils;
import com.khorda.interfaces.ApiClient;
import com.khorda.interfaces.ApiInterface;
import com.khorda.interfaces.OnItemClickListener;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ez on 8/17/2017.
 */

public class ColorsListDialog {
    private SelectedColor colorSelect;
    private Dialog dialog;
    private AppCompatActivity  activity;
    private Adapter_Colors_Dialog adapterFilter;
    private ArrayList<Colors.ColorsBean> filterList=new ArrayList<>();
    private RecyclerView recyclerViewError;
    private Colors colors ;

    public ColorsListDialog(final AppCompatActivity activity){
        this.activity= activity;
        dialog = new Dialog(activity);

    }

    public void setSelectColor(SelectedColor colorSelect) {
        this.colorSelect = colorSelect;
    }

    public void setColors(Colors colors) {
        this.colors = colors;
    }

    public void show() {


        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.select_colors_dialog_layout);

        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);


        recyclerViewError = (RecyclerView) dialog.findViewById(R.id.colors_list);
        recyclerViewError.setLayoutManager(new LinearLayoutManager(activity));
        adapterFilter = new Adapter_Colors_Dialog(activity, filterList);
        recyclerViewError.setAdapter(adapterFilter);

        adapterFilter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int position) {
                try {
                    colorSelect.getSelectedColor(adapterFilter.DataList.get(position));
                    dialog.dismiss();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        filterList.addAll(colors.getColors());
        adapterFilter.notifyDataSetChanged();
        dialog.show();


       /* if (ToolsUtils.isNetworkAvailable()) {

            final AVLoadingIndicatorDialog avLoadingIndicatorDialog = ToolsUtils.CreateDialog(activity, null);
            avLoadingIndicatorDialog.show();


            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<Colors> call = apiService.getColors();

            call.enqueue(new Callback<Colors>() {
                @Override
                public void onResponse(Call<Colors> call, Response<Colors> response) {

                    System.out.println("Code" + response.code());
                    System.out.println("raw" + response.raw());
                    System.out.println("Code" + response.message());
                    avLoadingIndicatorDialog.hide();

                    if (response.code() == 200 && response.body().isStatus()) {

                        if (response.body().getColors().size() > 0) {
                            filterList.addAll(response.body().getColors());
                            adapterFilter.notifyDataSetChanged();
                            dialog.show();
                        } else {
                            Toast.makeText(activity, R.string.err_products_empty, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(activity, R.string.err_msg_server, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<Colors> call, Throwable t) {
                    avLoadingIndicatorDialog.hide();
                    Toast.makeText(activity, R.string.err_msg_server, Toast.LENGTH_SHORT).show();

                }
            });

        } else {

            Toast.makeText(activity, R.string.err_msg_no_internet_connection, Toast.LENGTH_SHORT).show();

        }*/






    }
     public interface SelectedColor {
        public void getSelectedColor(Colors.ColorsBean SelectedColor);
    }
}