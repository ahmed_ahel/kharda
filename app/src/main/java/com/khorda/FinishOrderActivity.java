package com.khorda;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import com.khorda.CustomViews.AVLoadingIndicatorDialog;
import com.khorda.CustomViews.Button;
import com.khorda.CustomViews.TextView;
import com.khorda.Models.Payment;
import com.khorda.Models.PaymentResponse;
import com.khorda.Models.Products;
import com.khorda.Models.RequestResponse;
import com.khorda.Utils.MyContextWrapper;
import com.khorda.Utils.PreferenceEditor;
import com.khorda.Utils.QuickstartPreferences;
import com.khorda.Utils.ToolsUtils;
import com.khorda.interfaces.ApiClient;
import com.khorda.interfaces.ApiInterface;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;

public class FinishOrderActivity extends AppCompatActivity {

    private TextView tv_order_status, tv_order_status_desc;
    private AppCompatImageView img_order_status;
    private Button btn_shopping_again, btn_try_again;
    private Toolbar toolbar;
    private TextView toolbar_title;
    private AppCompatTextView textViewCartCounter;
    private Payment payment;
    private boolean OrderDone = false;
    private String order = null;
    private AppCompatImageView header_image;
    private boolean CheckPaymentSend = false ;


    @Override
    protected void attachBaseContext(Context newBase) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String lang = PreferenceEditor.getInstance().getStringPreference(QuickstartPreferences.CURRENT_LANGUAGE);
            super.attachBaseContext(MyContextWrapper.wrap(newBase, lang));
        }else {
            super.attachBaseContext(newBase);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finish_order);



        header_image = (AppCompatImageView) findViewById(R.id.image);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        textViewCartCounter = (AppCompatTextView) findViewById(R.id.textViewCartCounter);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar_title.setText(R.string.order_title);
        textViewCartCounter.setText(""+ToolsUtils.getAllCartProducts().size());
        if (ToolsUtils.getDeviceLang().equalsIgnoreCase(QuickstartPreferences.AR_LAN)) {
            header_image.setImageResource(R.drawable.rtl_ic_order);
        } else {
            header_image.setImageResource(R.drawable.ic_order);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tv_order_status = (TextView) findViewById(R.id.tv_order_status);
        tv_order_status_desc = (TextView) findViewById(R.id.tv_order_status_desc);
        img_order_status = (AppCompatImageView) findViewById(R.id.img_order_status);
        btn_try_again = (Button) findViewById(R.id.btn_try_again);
        btn_shopping_again = (Button) findViewById(R.id.btn_shopping_again);
        btn_shopping_again.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LunachHome();
            }
        });
        btn_try_again.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (payment.getType() == 0) {
                    Log.d("payment", "requestOrderCashOnDelivery");
                    requestOrderCashOnDelivery();
                } else if (payment.getType() == 1) {
                    Log.d("payment", "requestOrderPayment");
                    requestOrderPayment();
                }
            }
        });

        try {
            List<Payment> list = Payment.listAll(Payment.class);
            if (list == null) {
                finish();
            }
            payment = list.get(list.size() - 1);
            if (payment.getType() == 0) {
                Log.d("payment", "requestOrderCashOnDelivery");
                requestOrderCashOnDelivery();
            } else if (payment.getType() == 1) {

                Log.d("payment", "requestOrderPayment");
                requestOrderPayment();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void LunachHome() {
        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void requestOrderCashOnDelivery() {
        if (ToolsUtils.isNetworkAvailable()) {
            final AVLoadingIndicatorDialog dialog = ToolsUtils.CreateDialog(this, null);
            dialog.show();
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            RequestBody full_name = RequestBody.create(MediaType.parse("text/plain"), payment.getFullName().trim());
            RequestBody emailAddress = RequestBody.create(MediaType.parse("text/plain"), payment.getEmail().trim());
            RequestBody mobileNum = RequestBody.create(MediaType.parse("text/plain"), payment.getMobile().trim());
            RequestBody address1 = RequestBody.create(MediaType.parse("text/plain"), "" + payment.getAddress());
            RequestBody country_id = RequestBody.create(MediaType.parse("text/plain"), "" + payment.getCountryId());
            RequestBody city_id = RequestBody.create(MediaType.parse("text/plain"), "" + payment.getCityId());
            RequestBody data = RequestBody.create(MediaType.parse("text/plain"), "" + payment.getData());
            Log.d("payment", payment.toString());
            Map<String, RequestBody> map = new HashMap<>();
            map.put("fullname", full_name);
            map.put("email", emailAddress);
            map.put("mobile", mobileNum);
            map.put("country_id", country_id);
            map.put("city_id", city_id);
            map.put("address1", address1);
            map.put("data", data);
            Call<RequestResponse> call = apiService.sendCashOnDeliveryOrder(map);
            call.enqueue(new retrofit2.Callback<RequestResponse>() {
                @Override
                public void onResponse(Call<RequestResponse> call, Response<RequestResponse> response) {
                    System.out.println("Code" + response.code());
                    System.out.println("raw" + response.raw());
                    System.out.println("Code" + response.message());
                    dialog.hide();
                    if (response.code() == 200) {
                        if (response.body().isStatus()) {
                            Products.ProductsBean.DataBean.deleteAll(Products.ProductsBean.DataBean.class);
                            Payment.deleteAll(Payment.class);
                            textViewCartCounter.setText("0");
                            tv_order_status.setText(R.string.thank_you_title);
                            img_order_status.setImageResource(R.drawable.ic_order_status_success);
                            tv_order_status_desc.setText(R.string.order_status_success_desc);
                            btn_shopping_again.setVisibility(View.VISIBLE);
                            btn_try_again.setVisibility(View.GONE);
                            OrderDone = true;
                        } else {
                            OrderDone = false;
                            tv_order_status.setText(R.string.failed_try_again_title);
                            img_order_status.setImageResource(R.drawable.ic_order_status_failed);
                            tv_order_status_desc.setText(R.string.order_status_failed_desc);
                            btn_shopping_again.setVisibility(View.GONE);
                            btn_try_again.setVisibility(View.VISIBLE);
                        }
                    } else {
                        OrderDone = false;
                        btn_shopping_again.setVisibility(View.GONE);
                        btn_try_again.setVisibility(View.VISIBLE);

                        Toast.makeText(getApplicationContext(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<RequestResponse> call, Throwable t) {
                    t.printStackTrace();
                    OrderDone = false;
                    dialog.hide();
                    System.out.println("onFailure");
                    Toast.makeText(getApplicationContext(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();
                    btn_shopping_again.setVisibility(View.GONE);
                    btn_try_again.setVisibility(View.VISIBLE);
                }
            });
        } else {
            OrderDone = false;
            btn_shopping_again.setVisibility(View.GONE);
            btn_try_again.setVisibility(View.VISIBLE);
            Toast.makeText(getApplicationContext(), R.string.err_msg_no_internet_connection, Toast.LENGTH_SHORT).show();
        }
    }


    private void requestOrderPayment() {
        if (ToolsUtils.isNetworkAvailable()) {

            final AVLoadingIndicatorDialog dialog = ToolsUtils.CreateDialog(this, null);
            dialog.show();
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            RequestBody full_name = RequestBody.create(MediaType.parse("text/plain"), payment.getFullName().trim());
            RequestBody emailAddress = RequestBody.create(MediaType.parse("text/plain"), payment.getEmail().trim());
            RequestBody mobileNum = RequestBody.create(MediaType.parse("text/plain"), payment.getMobile().trim());
            RequestBody address1 = RequestBody.create(MediaType.parse("text/plain"), "" + payment.getAddress());
            RequestBody country_id = RequestBody.create(MediaType.parse("text/plain"), "" + payment.getCountryId());
            RequestBody city_id = RequestBody.create(MediaType.parse("text/plain"), "" + payment.getCityId());
            RequestBody data = RequestBody.create(MediaType.parse("text/plain"), "" + payment.getData());


            Map<String, RequestBody> map = new HashMap<>();
            map.put("fullname", full_name);
            map.put("email", emailAddress);
            map.put("mobile", mobileNum);
            map.put("country_id",country_id);
            map.put("city_id", city_id);
            map.put("address1", address1);
            map.put("data", data);

            Call<PaymentResponse> call = apiService.sendPaymentOrder(map);
            call.enqueue(new retrofit2.Callback<PaymentResponse>() {
                @Override
                public void onResponse(Call<PaymentResponse> call, Response<PaymentResponse> response) {

                    System.out.println("Code" + response.code());
                    System.out.println("raw" + response.raw());
                    System.out.println("Code" + response.message());
                    dialog.hide();

                    if (response.code() == 200) {
                        if (response.body().isStatus()) {
//
//                            Toast.makeText(FinishOrderActivity.this, response.body().getDisplay_message(), Toast.LENGTH_SHORT).show();
                            order = response.body().getOrders().getOrder();
//                            webView.setVisibility(View.VISIBLE);
//                            webView.loadUrl(order.getLink());


                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(response.body().getOrders().getLink()));
                            startActivity(browserIntent);


                        } else {
                            btn_shopping_again.setVisibility(View.GONE);
                            btn_try_again.setVisibility(View.VISIBLE);
                            Toast.makeText(FinishOrderActivity.this, response.body().getDisplay_message(), Toast.LENGTH_SHORT).show();

                        }
                    } else {
                        btn_shopping_again.setVisibility(View.GONE);
                        btn_try_again.setVisibility(View.VISIBLE);

                        Toast.makeText(getApplicationContext(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();

                    }

                }

                @Override
                public void onFailure(Call<PaymentResponse> call, Throwable t) {
                    btn_shopping_again.setVisibility(View.GONE);
                    btn_try_again.setVisibility(View.VISIBLE);

                    t.printStackTrace();
                    dialog.hide();
                    System.out.println("onFailure");
                    Toast.makeText(getApplicationContext(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();

                }
            });

        } else {
            btn_shopping_again.setVisibility(View.GONE);
            btn_try_again.setVisibility(View.VISIBLE);

            Toast.makeText(getApplicationContext(), R.string.err_msg_no_internet_connection, Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (OrderDone) {
            LunachHome();
        } else {
            order = null ;
            finish();
        }

    }

    private void requestCheckOrderPayment() {


        if (ToolsUtils.isNetworkAvailable()) {

            final AVLoadingIndicatorDialog dialog = ToolsUtils.CreateDialog(this, null);
            dialog.show();
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<RequestResponse> call = apiService.checkOrderPayment(order);

            Log.d("Err","request Check Order Payment");
            Log.d("Err","Order"+order);

            call.enqueue(new retrofit2.Callback<RequestResponse>() {
                @Override
                public void onResponse(Call<RequestResponse> call, Response<RequestResponse> response) {

                    System.out.println("Code" + response.code());
                    System.out.println("raw" + response.raw());
                    System.out.println("Code" + response.message());
                    dialog.hide();

                    if (response.code() == 200) {
                        if (response.body().isStatus()) {

                            Toast.makeText(FinishOrderActivity.this, response.body().getDisplay_message(), Toast.LENGTH_SHORT).show();
                            Products.ProductsBean.DataBean.deleteAll(Products.ProductsBean.DataBean.class);
                            Payment.deleteAll(Payment.class);
                            textViewCartCounter.setText("0");

                            tv_order_status.setText(R.string.thank_you_title);
                            img_order_status.setImageResource(R.drawable.ic_order_status_success);
                            tv_order_status_desc.setText(R.string.order_status_success_desc);

                            btn_shopping_again.setVisibility(View.VISIBLE);
                            btn_try_again.setVisibility(View.GONE);
                            OrderDone = true;
                            order = null;
                            CheckPaymentSend = true ;

                        } else {

                            Toast.makeText(FinishOrderActivity.this, response.body().getDisplay_message(), Toast.LENGTH_SHORT).show();
                            OrderDone = false;
                            tv_order_status.setText(R.string.failed_try_again_title);
                            img_order_status.setImageResource(R.drawable.ic_order_status_failed);
                            tv_order_status_desc.setText(R.string.order_status_failed_desc);

                            btn_shopping_again.setVisibility(View.GONE);
                            btn_try_again.setVisibility(View.VISIBLE);
                        }
                    } else {
                        OrderDone = false;
                        btn_shopping_again.setVisibility(View.GONE);
                        btn_try_again.setVisibility(View.VISIBLE);

                        Toast.makeText(getApplicationContext(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<RequestResponse> call, Throwable t) {
                    t.printStackTrace();
                    OrderDone = false;
                    dialog.hide();
                    System.out.println("onFailure");
                    Toast.makeText(getApplicationContext(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();
                    btn_shopping_again.setVisibility(View.GONE);
                    btn_try_again.setVisibility(View.VISIBLE);

                }
            });

        } else {
            OrderDone = false;
            btn_shopping_again.setVisibility(View.GONE);
            btn_try_again.setVisibility(View.VISIBLE);

            Toast.makeText(getApplicationContext(), R.string.err_msg_no_internet_connection, Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("TAG","CheckPaymentSend"+CheckPaymentSend);
        if(order != null && !CheckPaymentSend) {
            Log.d("TAG","onResume");
            Log.d("TAG","requestCheckOrderPayment");

            requestCheckOrderPayment();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
       Log.d("Err","onDestroy");
        order = null ;
    }




}
