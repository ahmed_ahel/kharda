package com.khorda;

import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Toast;

import com.khorda.Adapter.Adapter_Category_Dialog;
import com.khorda.Adapter.Adapter_Price_Dialog;
import com.khorda.CustomViews.AVLoadingIndicatorDialog;
import com.khorda.Models.Categories;
import com.khorda.Models.Prices;
import com.khorda.Utils.ToolsUtils;
import com.khorda.interfaces.ApiClient;
import com.khorda.interfaces.ApiInterface;
import com.khorda.interfaces.OnItemClickListener;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ez on 8/17/2017.
 */

public class PriceListDialog {
    private SelectedPrice priceSelect;
    private Dialog dialog;
    private AppCompatActivity activity;
    private Adapter_Price_Dialog adapterFilter;
    private ArrayList<Prices.PricesBean> filterList = new ArrayList<>();
    private RecyclerView recyclerViewError;
    private Prices prices ;

    public PriceListDialog(final AppCompatActivity activity) {
        this.activity = activity;
        dialog = new Dialog(activity);

    }

    public void setSelectPrice(SelectedPrice priceSelect) {
        this.priceSelect = priceSelect;
    }

    public void setPrices(Prices prices) {
        this.prices = prices;
    }

    public void show() {
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.select_price_dialog_layout);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        recyclerViewError = (RecyclerView) dialog.findViewById(R.id.prices_list);
        recyclerViewError.setLayoutManager(new LinearLayoutManager(activity));
        adapterFilter = new Adapter_Price_Dialog(activity, filterList);
        recyclerViewError.setAdapter(adapterFilter);
        adapterFilter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int position) {
                try {
                    priceSelect.getSelectedPrice(adapterFilter.DataList.get(position));
                    dialog.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        filterList.addAll(prices.getPrices());
        adapterFilter.notifyDataSetChanged();
        dialog.show();



        /*if (ToolsUtils.isNetworkAvailable()) {

            final AVLoadingIndicatorDialog avLoadingIndicatorDialog = ToolsUtils.CreateDialog(activity, null);
            avLoadingIndicatorDialog.show();
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            Call<Prices> call = apiService.getPrices();
            call.enqueue(new Callback<Prices>() {
                @Override
                public void onResponse(Call<Prices> call, Response<Prices> response) {

                    System.out.println("Code" + response.code());
                    System.out.println("raw" + response.raw());
                    System.out.println("Code" + response.message());
                    avLoadingIndicatorDialog.hide();
                    if (response.code() == 200 && response.body().isStatus()) {

                        if (response.body().getPrices().size() > 0) {
                            filterList.addAll(response.body().getPrices());
                            adapterFilter.notifyDataSetChanged();
                            dialog.show();
                        } else {
                            /// Empty List
                            Toast.makeText(activity, R.string.err_products_empty, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(activity, R.string.err_msg_server, Toast.LENGTH_SHORT).show();
                    }
                }
                @Override
                public void onFailure(Call<Prices> call, Throwable t) {
                    avLoadingIndicatorDialog.hide();
                    Toast.makeText(activity, R.string.err_msg_server, Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(activity, R.string.err_msg_no_internet_connection, Toast.LENGTH_SHORT).show();
        }*/
    }

    public interface SelectedPrice {
        public void getSelectedPrice(Prices.PricesBean SelectedPrice);
    }
}