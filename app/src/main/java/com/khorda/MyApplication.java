package com.khorda;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.support.multidex.MultiDex;

import com.google.firebase.FirebaseApp;
import com.orm.SugarApp;
import com.twitter.sdk.android.core.Twitter;

/**
 * Created by AHMED-PC on 12/8/2016.
 */

public class MyApplication extends SugarApp {

    public static final String TAG = MyApplication.class
            .getSimpleName();


    private static MyApplication instance;


    public static synchronized MyApplication getInstance() {
        return instance;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }


    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    // This method is for use in emulated process environments.
    // It will never be called on a production Android device
    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    @Override
    public void onCreate() {
        super.onCreate();
         instance = this;
        Twitter.initialize(this);
    }


}
