package com.khorda;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;
import android.widget.Toast;

import com.khorda.CustomViews.AVLoadingIndicatorDialog;
import com.khorda.CustomViews.TextView;
import com.khorda.Models.TermsConditions;
import com.khorda.Utils.MyContextWrapper;
import com.khorda.Utils.PreferenceEditor;
import com.khorda.Utils.QuickstartPreferences;
import com.khorda.Utils.ToolsUtils;
import com.khorda.interfaces.ApiClient;
import com.khorda.interfaces.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ez on 8/21/2017.
 */

public class TermAndConditionActivity extends AppCompatActivity {

    private WebView web_view;
    private Toolbar toolbar;
    private TextView toolbar_title;

    @Override
    protected void attachBaseContext(Context newBase) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String lang = PreferenceEditor.getInstance().getStringPreference(QuickstartPreferences.CURRENT_LANGUAGE);
            super.attachBaseContext(MyContextWrapper.wrap(newBase, lang));
        }else {
            super.attachBaseContext(newBase);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_term_conditions);


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        web_view = (WebView) findViewById(R.id.web_view);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar_title.setText(R.string.terms_conditions);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        if (ToolsUtils.isNetworkAvailable()) {

            final AVLoadingIndicatorDialog dialog = ToolsUtils.CreateDialog(this, null);
            dialog.show();


            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<TermsConditions> call = apiService.getTermsConditions();

            call.enqueue(new Callback<TermsConditions>() {
                @Override
                public void onResponse(Call<TermsConditions> call, Response<TermsConditions> response) {

                    System.out.println("Code" + response.code());
                    System.out.println("raw" + response.raw());
                    System.out.println("Code" + response.message());
                    dialog.hide();
                    if (response.code() == 200 && response.body().isStatus()) {
                        ToolsUtils.setupWebView(web_view);
                        web_view.loadData(response.body().getTerms_conditions(), "text/html; charset=UTF-8", null);
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<TermsConditions> call, Throwable t) {
                    dialog.hide();
                    Toast.makeText(getApplicationContext(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();

                }
            });

        } else {

            Toast.makeText(getApplicationContext(), R.string.err_msg_no_internet_connection, Toast.LENGTH_SHORT).show();

        }

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}