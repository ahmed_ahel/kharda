package com.khorda;

import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.khorda.Adapter.Adapter_Cities_Dialog;
import com.khorda.Adapter.Adapter_Countries_Dialog;
import com.khorda.Models.Countries;
import com.khorda.Models.CountriesWithCities;
import com.khorda.interfaces.OnItemClickListener;

import java.util.ArrayList;

/**
 * Created by ez on 8/17/2017.
 */

public class CountryListDialog {
    private SelectedCountry countrySelect;
    private Dialog dialog;
    private AppCompatActivity activity;
    private Adapter_Countries_Dialog adapterFilter;
    private ArrayList<CountriesWithCities.CountriesBean> filterList = new ArrayList<>();
    private RecyclerView recyclerViewError;
    private CountriesWithCities countries ;

    public CountryListDialog(final AppCompatActivity activity) {
        this.activity = activity;
        dialog = new Dialog(activity);

    }

    public void setCountries(CountriesWithCities countries) {
        this.countries = countries;
    }

    public void setSelectCountry(SelectedCountry countrySelect) {
        this.countrySelect = countrySelect;
    }


    public void show() {


        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.select_city_dialog_layout);

        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);


        recyclerViewError = (RecyclerView) dialog.findViewById(R.id.cities_list);
        recyclerViewError.setLayoutManager(new LinearLayoutManager(activity));
        adapterFilter = new Adapter_Countries_Dialog(activity, filterList);
        recyclerViewError.setAdapter(adapterFilter);

        adapterFilter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int position) {
                try {
                    countrySelect.getSelectedCountry(adapterFilter.DataList.get(position));
                    dialog.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        filterList.addAll(countries.getCountries());
        adapterFilter.notifyDataSetChanged();
        dialog.show();



       /* if (ToolsUtils.isNetworkAvailable()) {

            final AVLoadingIndicatorDialog avLoadingIndicatorDialog = ToolsUtils.CreateDialog(activity, null);
            avLoadingIndicatorDialog.show();


            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<Countries> call = apiService.getCountries();

            call.enqueue(new Callback<Countries>() {
                @Override
                public void onResponse(Call<Countries> call, Response<Countries> response) {

                    System.out.println("Code" + response.code());
                    System.out.println("raw" + response.raw());
                    System.out.println("Code" + response.message());
                    avLoadingIndicatorDialog.hide();

                    if (response.code() == 200 && response.body().isStatus()) {

                        if (response.body().getCountries().size() > 0) {
                            filterList.addAll(response.body().getCountries());
                            adapterFilter.notifyDataSetChanged();
                            dialog.show();
                        } else {
                            Toast.makeText(activity, R.string.err_products_empty, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(activity, R.string.err_msg_server, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<Countries> call, Throwable t) {
                    avLoadingIndicatorDialog.hide();
                    Toast.makeText(activity, R.string.err_msg_server, Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(activity, R.string.err_msg_no_internet_connection, Toast.LENGTH_SHORT).show();
        }*/

    }

    public interface SelectedCountry {
        public void getSelectedCountry(CountriesWithCities.CountriesBean SelectedCity);
    }
}