package com.khorda.CustomViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.khorda.R;


public class TextView extends android.support.v7.widget.AppCompatTextView {
    String assetsFont = "";

    public TextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context);
    }

    public TextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.TextView);
        int speed = array.getInt(R.styleable.TextView_fontType, 0);

        switch (speed) {
            case 0:
                assetsFont = "";
                break;
            case 1:
                assetsFont = "mohand_font.ttf";
                break;
            case 2:
                assetsFont = "october_twilight.ttf";
                break;
        }
        if (assetsFont.length() > 2)
            UpdateFont(context);
    }



    public void setAssetsFont(Context context, String assetsFont) {
        this.assetsFont = assetsFont;
        UpdateFont(context);
    }

    Typeface getAssetsFont(Context context) {
        Typeface tf = Typeface.createFromAsset(context.getAssets(), "" + assetsFont);
        return tf;
        //setTypeface(tf);
    }


    void UpdateFont(Context context) {
        setTypeface(getAssetsFont(context));

    }


}
