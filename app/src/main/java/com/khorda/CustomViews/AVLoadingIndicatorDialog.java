package com.khorda.CustomViews;

/**
 * Created by ez on 8/17/2017.
 */

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;

import com.khorda.R;


public class AVLoadingIndicatorDialog extends AlertDialog {

    private AppCompatTextView mMessageView;

    public AVLoadingIndicatorDialog(Context context) {
        super(context);
        View view = LayoutInflater.from(getContext()).inflate(R.layout.progress_avld, null);
        mMessageView = (AppCompatTextView) view.findViewById(R.id.message);
        setView(view);
    }


    @Override
    public void setMessage(CharSequence message) {
        mMessageView.setText(message);
    }
}