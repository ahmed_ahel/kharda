package com.khorda.CustomViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.khorda.R;


public class Button extends android.support.v7.widget.AppCompatButton {
    String assetsFont = "";

    public Button(Context context) {
        super(context);
    }

    public Button(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.TextView);
        int speed = array.getInt(R.styleable.TextView_fontType, 0);
        //Log.e("TypeFont", "" + speed);
        //----------------------------->
        switch (speed) {
            case 0:
                assetsFont = "";
                break;
            case 1:
                assetsFont = "mohand_font.ttf";
                break;
            case 2:
                assetsFont = "october_twilight.ttf";
                break;
        }
        if (assetsFont.length() > 2)
            UpdateFont(context);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!this.isEnabled()) setAlpha(0.5f);
        else setAlpha(1);
    }

    public void setAssetsFont(Context context, String assetsFont) {
        this.assetsFont = assetsFont;
        UpdateFont(context);
    }

    Typeface getAssetsFont(Context context) {
        Typeface tf = Typeface.createFromAsset(context.getAssets(), "" + assetsFont);
        return tf;
        //setTypeface(tf);
    }


    void UpdateFont(Context context) {
        setTypeface(getAssetsFont(context));

    }
}
