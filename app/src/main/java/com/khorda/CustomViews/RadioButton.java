package com.khorda.CustomViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatRadioButton;
import android.util.AttributeSet;

import com.khorda.R;


public class RadioButton extends AppCompatRadioButton {

    String assetsFont = "";

    public RadioButton(Context context) {
        super(context);
    }

    public RadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.TextView);
        int speed = array.getInt(R.styleable.TextView_fontType, 0);
        // Log.e("TypeFont", "" + speed);

        switch (speed) {
            case 0:
                assetsFont = "";
                break;
            case 1:
                assetsFont = "mohand_font.ttf";
                break;
            case 2:
                assetsFont = "october_twilight.ttf";
                break;
        }
        if (assetsFont.length() > 2)
            UpdateFont(context);
    }

    public void setAssetsFont(Context context, String assetsFont) {
        this.assetsFont = assetsFont;
        UpdateFont(context);
    }

    Typeface getAssetsFont(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "" + assetsFont);
    }


    void UpdateFont(Context context) {
        setTypeface(getAssetsFont(context));
    }
}
