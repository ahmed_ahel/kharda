package com.khorda;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageButton;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.gson.Gson;
import com.khorda.CustomViews.AVLoadingIndicatorDialog;
import com.khorda.CustomViews.TextView;
import com.khorda.Utils.MyContextWrapper;
import com.khorda.interfaces.ApiClient;
import com.khorda.interfaces.ApiInterface;
import com.khorda.Models.RequestResponse;
import com.khorda.Models.UserInfo;
import com.khorda.Utils.PreferenceEditor;
import com.khorda.Utils.QuickstartPreferences;
import com.khorda.Utils.ToolsUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VerificationActivity extends AppCompatActivity implements View.OnClickListener {

    private AppCompatImageButton send;
    private String mobile;
    private TextView code;

    @Override
    protected void attachBaseContext(Context newBase) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String lang = PreferenceEditor.getInstance().getStringPreference(QuickstartPreferences.CURRENT_LANGUAGE);
            super.attachBaseContext(MyContextWrapper.wrap(newBase, lang));
        }else {
            super.attachBaseContext(newBase);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);

        code = (TextView) findViewById(R.id.code);
        send = (AppCompatImageButton) findViewById(R.id.send);
        findViewById(R.id.num1).setOnClickListener(this);
        findViewById(R.id.num2).setOnClickListener(this);
        findViewById(R.id.num3).setOnClickListener(this);
        findViewById(R.id.num4).setOnClickListener(this);
        findViewById(R.id.num5).setOnClickListener(this);
        findViewById(R.id.num6).setOnClickListener(this);
        findViewById(R.id.num7).setOnClickListener(this);
        findViewById(R.id.num8).setOnClickListener(this);
        findViewById(R.id.num9).setOnClickListener(this);
        findViewById(R.id.plus).setOnClickListener(this);
        findViewById(R.id.num0).setOnClickListener(this);


        if (getIntent().getExtras() != null) {
            Bundle bundle = getIntent().getExtras();
            mobile = bundle.getString("mobile");
            RequestVerificationCode();
        }

        changeStatusBarColor();


        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (code.getText().length() == 0) {
                    Toast.makeText(VerificationActivity.this, R.string.err_msg_enter_verification_code, Toast.LENGTH_SHORT).show();
                    return;
                }

                RequestCheckCode();

            }
        });

    }

    private void RequestCheckCode() {

        if (ToolsUtils.isNetworkAvailable()) {

            final AVLoadingIndicatorDialog dialog = ToolsUtils.CreateDialog(this, null);
            dialog.show();


            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<UserInfo> call = apiService.checkCode(mobile,code.getText().toString());

            call.enqueue(new Callback<UserInfo>() {
                @Override
                public void onResponse(Call<UserInfo> call, Response<UserInfo> response) {

                    System.out.println("Code" + response.code());
                    System.out.println("raw" + response.raw());
                    System.out.println("Code" + response.message());
                    dialog.hide();

                    if (response.code() == 200) {

                        if (response.body().isSuccess()) {
                            PreferenceEditor.getInstance().setStringPreference(QuickstartPreferences.USER_INFO, new Gson().toJson(response.body()));
                            startActivity(new Intent(getApplicationContext(), HomeActivity.class));

                         } else {
                            Toast.makeText(VerificationActivity.this, R.string.err_msg_verification_code, Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Toast.makeText(getApplicationContext(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();

                    }

                }

                @Override
                public void onFailure(Call<UserInfo> call, Throwable t) {
                    dialog.hide();
                    Toast.makeText(getApplicationContext(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();

                }
            });

        } else {

            Toast.makeText(getApplicationContext(), R.string.err_msg_no_internet_connection, Toast.LENGTH_SHORT).show();

        }
    }


    private void RequestVerificationCode() {

        if (ToolsUtils.isNetworkAvailable()) {

            final AVLoadingIndicatorDialog dialog = ToolsUtils.CreateDialog(this, null);
            dialog.show();


            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<RequestResponse> call = apiService.requestNewCode(mobile);

            call.enqueue(new Callback<RequestResponse>() {
                @Override
                public void onResponse(Call<RequestResponse> call, Response<RequestResponse> response) {

                    System.out.println("Code" + response.code());
                    System.out.println("raw" + response.raw());
                    System.out.println("Code" + response.message());
                    dialog.hide();

                    if (response.code() == 200 && response.body().isStatus()) {

                        if (response.body().isStatus()) {
                            Toast.makeText(VerificationActivity.this, R.string.verification_code_sent, Toast.LENGTH_SHORT).show();
                        } else {
                            //
                        }

                    } else {
                        Toast.makeText(getApplicationContext(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();

                    }

                }

                @Override
                public void onFailure(Call<RequestResponse> call, Throwable t) {
                    dialog.hide();
                    Toast.makeText(getApplicationContext(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();

                }
            });

        } else {
           // ToolsUtils.ShowNoInternetConnection(this);
            Toast.makeText(getApplicationContext(), R.string.err_msg_no_internet_connection, Toast.LENGTH_SHORT).show();

        }
    }

    public void BackClicked(View v) {
        finish();
    }

    public void CleanClicked(View v) {
        String str = code.getText().toString();
        if (str.length() > 0) {
            str = str.substring(0, str.length() - 1);
            code.setText(str);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.verificationColor));
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.num0:
                UpdateCode("0");
                break;
            case R.id.num1:
                UpdateCode("1");
                break;
            case R.id.num2:
                UpdateCode("2");
                break;
            case R.id.num3:
                UpdateCode("3");
                break;
            case R.id.num4:
                UpdateCode("4");
                break;
            case R.id.num5:
                UpdateCode("5");
                break;
            case R.id.num6:
                UpdateCode("6");
                break;
            case R.id.num7:
                UpdateCode("7");
                break;
            case R.id.num8:
                UpdateCode("8");
                break;
            case R.id.num9:
                UpdateCode("9");
                break;
            case R.id.plus:
                UpdateCode("+");
                break;
        }
    }


    private void UpdateCode(String value) {

        if (code.getText().length() > 0 && code.getText().length() < 4) {
            code.append(value);
        } else if (code.getText().length() == 0) {
            code.setText(value);
        }

    }


}
