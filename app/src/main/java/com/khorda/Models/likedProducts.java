package com.khorda.Models;

import com.khorda.Utils.QuickstartPreferences;
import com.khorda.Utils.ToolsUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ahmed on 8/15/17.
 */

public class likedProducts implements Serializable{


    /**
     * status : true
     * messageA :
     * messageE :
     * products : [{"name":"منتج","nameE":"test","company_nameE":"Company name","company_name":"إسم الشركة","company_phone":"+925-8857-552","description":"<p>anjasnjansjnasj<\/p>","descriptionE":"<p>mdfjn<\/p>","category_id":{"id":9,"title_ar":"خردة بالجملة","title_en":"Kharda Offers"},"price":"5.00","quantity":"6","likes":"1","views":"0","colors":{"id":3,"title_ar":"أبيض","title_en":"white"},"image":"http://khardakw.com/images/products/bZuBGCz2l6VNE0OlkVnmTwhIfEgEQ1KlVPSQvmce.png","isdelete":"0","created_at":"2017-11-29 07:13:41","updated_at":"2017-11-29 13:13:41","deleted_at":null,"is_liked":1,"product_id":"24","images":["http://khardakw.com/images/products/YtZ7t90zw6E5XELpwOqW8zPRuJTTcNtYRVTYXpSm.png","http://khardakw.com/images/products/JlqBrUPUbQgRu6N3l5sQJphvwTOqpi8Ti3bP4R8G.png","http://khardakw.com/images/products/SkoVm0PfBhukeP6YQtpHhyx1DswAdT7qysBGEWKi.png","http://khardakw.com/images/products/raFrENF40Av6N3jfbObNvQUd9VCxh4ACuMOGtm7D.png","http://khardakw.com/images/products/bZuBGCz2l6VNE0OlkVnmTwhIfEgEQ1KlVPSQvmce.png"],"rate_product":{"rate":4,"count":1}},{"name":"منتج رائع","nameE":"Brenna Lopez","company_nameE":"Robinson and Hopkins Plc","company_name":"شركة رائعة","company_phone":"Decker Pitts LLC","description":"<p>Ullam necessitatibus rem omnis eligendi sapiente voluptatem. A libero illo minim dolore adipisicing voluptas aliquam amet, tenetur itaque aliquip nihil voluptatem ullam sunt dolorem incidunt.<\/p>","descriptionE":"<p>Inventore voluptas aliquam aut dolores atque cumque deserunt voluptatem. Alias qui molestias animi, qui voluptatibus ut aute aut elit, rerum sed hic in sit ex incididunt officia sapiente quo quibusdam tempora Nam deserunt perferendis maxime Nam impedit, dolor rerum fuga. Quaerat necessitatibus blanditiis nihil consequuntur velit ea amet, ipsam vitae magnam rerum omnis dolor qui voluptatem.<\/p>","category_id":{"id":0,"title_ar":"الكل","title_en":"ALL"},"price":"1.00","quantity":"6","likes":"1","views":"0","colors":{"id":5,"title_ar":"أسود","title_en":"Black"},"image":"http://khardakw.com/images/products/tGTdd4oBpyFQx1slFvxOCyA5Dxgb7DBvp3jgweLS.png","isdelete":"0","created_at":"2017-11-29 03:49:24","updated_at":"2017-11-24 15:10:05","deleted_at":null,"is_liked":1,"product_id":"32","images":["http://khardakw.com/images/products/tGTdd4oBpyFQx1slFvxOCyA5Dxgb7DBvp3jgweLS.png"],"rate_product":{"rate":0,"count":0}},{"name":"مثلجات","nameE":"iced","company_nameE":"abo alez","company_name":"بو العز","company_phone":"56666500","description":"<p><strong>قوالب مثلجات&nbsp;<\/strong><\/p>","descriptionE":"<p>iced<\/p>","category_id":{"id":9,"title_ar":"خردة بالجملة","title_en":"Kharda Offers"},"price":"0.75","quantity":"6","likes":"1","views":"0","colors":{"id":14,"title_ar":"الكل","title_en":"All"},"image":"http://khardakw.com/images/products/ZXAzYM4dt3cKtKCUzULuJxTZIL4ACRAZwdEnAnsn.jpeg","isdelete":"0","created_at":"2017-11-29 03:49:15","updated_at":"2017-11-22 18:05:57","deleted_at":null,"is_liked":1,"product_id":"34","images":["http://khardakw.com/images/products/ZXAzYM4dt3cKtKCUzULuJxTZIL4ACRAZwdEnAnsn.jpeg"],"rate_product":{"rate":0,"count":0}}]
     */

    private boolean status;
    private String messageA;
    private String messageE;
    private List<ProductsBean> products;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessageA() {
        return messageA;
    }

    public void setMessageA(String messageA) {
        this.messageA = messageA;
    }

    public String getMessageE() {
        return messageE;
    }

    public void setMessageE(String messageE) {
        this.messageE = messageE;
    }

    public List<ProductsBean> getProducts() {
        if(products == null){
            products = new ArrayList<>();
        }
        return products;
    }

    public void setProducts(List<ProductsBean> products) {
        this.products = products;
    }

    public static class ProductsBean implements Serializable{
        /**
         * name : منتج
         * nameE : test
         * company_nameE : Company name
         * company_name : إسم الشركة
         * company_phone : +925-8857-552
         * description : <p>anjasnjansjnasj</p>
         * descriptionE : <p>mdfjn</p>
         * category_id : {"id":9,"title_ar":"خردة بالجملة","title_en":"Kharda Offers"}
         * price : 5.00
         * quantity : 6
         * likes : 1
         * views : 0
         * colors : {"id":3,"title_ar":"أبيض","title_en":"white"}
         * image : http://khardakw.com/images/products/bZuBGCz2l6VNE0OlkVnmTwhIfEgEQ1KlVPSQvmce.png
         * isdelete : 0
         * created_at : 2017-11-29 07:13:41
         * updated_at : 2017-11-29 13:13:41
         * deleted_at : null
         * is_liked : 1
         * product_id : 24
         * images : ["http://khardakw.com/images/products/YtZ7t90zw6E5XELpwOqW8zPRuJTTcNtYRVTYXpSm.png","http://khardakw.com/images/products/JlqBrUPUbQgRu6N3l5sQJphvwTOqpi8Ti3bP4R8G.png","http://khardakw.com/images/products/SkoVm0PfBhukeP6YQtpHhyx1DswAdT7qysBGEWKi.png","http://khardakw.com/images/products/raFrENF40Av6N3jfbObNvQUd9VCxh4ACuMOGtm7D.png","http://khardakw.com/images/products/bZuBGCz2l6VNE0OlkVnmTwhIfEgEQ1KlVPSQvmce.png"]
         * rate_product : {"rate":4,"count":1}
         */

        private String name;
        private String nameE;
        private String company_nameE;
        private String company_name;
        private String company_phone;
        private String description;
        private String descriptionE;
        private CategoryIdBean category_id;
        private String price;
        private String quantity;
        private String likes;
        private String views;
        private ColorsBean colors;
        private String image;
        private String isdelete;
        private String created_at;
        private String updated_at;
        private Object deleted_at;
        private int is_liked;
        private String product_id;
        private RateProductBean rate_product;
        private List<String> images;
        private String display_name ;



        public String getDisplay_name() {

            if (ToolsUtils.getDeviceLang().equalsIgnoreCase(QuickstartPreferences.AR_LAN)) {
                display_name =  this.getName();
            } else {
                display_name =  this.getNameE();
            }
            return display_name.trim();


        }


        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getNameE() {
            return nameE;
        }

        public void setNameE(String nameE) {
            this.nameE = nameE;
        }

        public String getCompany_nameE() {
            return company_nameE;
        }

        public void setCompany_nameE(String company_nameE) {
            this.company_nameE = company_nameE;
        }

        public String getCompany_name() {
            return company_name;
        }

        public void setCompany_name(String company_name) {
            this.company_name = company_name;
        }

        public String getCompany_phone() {
            return company_phone;
        }

        public void setCompany_phone(String company_phone) {
            this.company_phone = company_phone;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getDescriptionE() {
            return descriptionE;
        }

        public void setDescriptionE(String descriptionE) {
            this.descriptionE = descriptionE;
        }

        public CategoryIdBean getCategory_id() {
            return category_id;
        }

        public void setCategory_id(CategoryIdBean category_id) {
            this.category_id = category_id;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public String getLikes() {
            return likes;
        }

        public void setLikes(String likes) {
            this.likes = likes;
        }

        public String getViews() {
            return views;
        }

        public void setViews(String views) {
            this.views = views;
        }

        public ColorsBean getColors() {
            return colors;
        }

        public void setColors(ColorsBean colors) {
            this.colors = colors;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getIsdelete() {
            return isdelete;
        }

        public void setIsdelete(String isdelete) {
            this.isdelete = isdelete;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public Object getDeleted_at() {
            return deleted_at;
        }

        public void setDeleted_at(Object deleted_at) {
            this.deleted_at = deleted_at;
        }

        public int getIs_liked() {
            return is_liked;
        }

        public void setIs_liked(int is_liked) {
            this.is_liked = is_liked;
        }

        public String getProduct_id() {
            return product_id;
        }

        public void setProduct_id(String product_id) {
            this.product_id = product_id;
        }

        public RateProductBean getRate_product() {
            return rate_product;
        }

        public void setRate_product(RateProductBean rate_product) {
            this.rate_product = rate_product;
        }

        public List<String> getImages() {
            if(images == null){
                images = new ArrayList<>();
            }
            return images;
        }

        public void setImages(List<String> images) {
            this.images = images;
        }

        public static class CategoryIdBean implements Serializable{
            /**
             * id : 9
             * title_ar : خردة بالجملة
             * title_en : Kharda Offers
             */

            private int id;
            private String title_ar;
            private String title_en;
            private String display_title;


            public String getDisplay_title() {
                if (ToolsUtils.getDeviceLang().equalsIgnoreCase(QuickstartPreferences.AR_LAN)) {
                    display_title =  this.getTitle_ar();
                } else {
                    display_title =  this.getTitle_en();
                }
                return display_title.trim();
            }


            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getTitle_ar() {
                return title_ar;
            }

            public void setTitle_ar(String title_ar) {
                this.title_ar = title_ar;
            }

            public String getTitle_en() {
                return title_en;
            }

            public void setTitle_en(String title_en) {
                this.title_en = title_en;
            }
        }

        public static class ColorsBean implements Serializable{
            /**
             * id : 3
             * title_ar : أبيض
             * title_en : white
             */

            private int id;
            private String title_ar;
            private String title_en;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getTitle_ar() {
                return title_ar;
            }

            public void setTitle_ar(String title_ar) {
                this.title_ar = title_ar;
            }

            public String getTitle_en() {
                return title_en;
            }

            public void setTitle_en(String title_en) {
                this.title_en = title_en;
            }
        }

        public static class RateProductBean implements Serializable{
            /**
             * rate : 4
             * count : 1
             */

            private int rate;
            private int count;

            public int getRate() {
                return rate;
            }

            public void setRate(int rate) {
                this.rate = rate;
            }

            public int getCount() {
                return count;
            }

            public void setCount(int count) {
                this.count = count;
            }
        }
    }
}
