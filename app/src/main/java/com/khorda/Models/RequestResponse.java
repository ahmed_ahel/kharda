package com.khorda.Models;

import com.khorda.Utils.QuickstartPreferences;
import com.khorda.Utils.ToolsUtils;

/**
 * Created by ahmed on 8/15/17.
 */

public class RequestResponse {

    /**
     * status : true
     */

    private boolean status;
    private boolean success ;
    private String errors ,messageA,messageE,display_message;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getErrors() {
        return errors;
    }

    public void setErrors(String errors) {
        this.errors = errors;
    }

    public String getMessageA() {
        return messageA;
    }

    public void setMessageA(String messageA) {
        this.messageA = messageA;
    }

    public String getMessageE() {
        return messageE;
    }

    public void setMessageE(String messageE) {
        this.messageE = messageE;
    }

    public String getDisplay_message() {
        if (ToolsUtils.getDeviceLang().equalsIgnoreCase(QuickstartPreferences.AR_LAN)) {
            display_message =  this.getMessageA();
        } else {
            display_message =  this.getMessageE();
        }
        return display_message;

    }

    public void setDisplay_message(String display_message) {
        this.display_message = display_message;
    }
}
