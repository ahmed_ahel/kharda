package com.khorda.Models;

import com.khorda.Utils.QuickstartPreferences;
import com.khorda.Utils.ToolsUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ahmed on 8/15/17.
 */

public class CountriesWithCities {


    /**
     * status : true
     * messageA :
     * messageE :
     * countries : [{"id":1,"title_ar":"الكويت","title_en":"Kuwait","created_at":"2017-08-14 16:20:38","updated_at":"2017-10-23 09:17:11","deleted_at":null,"cities":[{"id":1,"title_ar":"مبارك","title_en":"Mobarak","country_id":"1","created_at":"2017-08-08 05:30:18","updated_at":"-0001-11-30 00:00:00"},{"id":2,"title_ar":"الاحمدي","title_en":"alAhmady","country_id":"1","created_at":"2017-08-08 05:30:18","updated_at":"-0001-11-30 00:00:00"},{"id":3,"title_ar":"حولي","title_en":"Hawali","country_id":"1","created_at":"2017-08-08 05:30:18","updated_at":"-0001-11-30 00:00:00"},{"id":4,"title_ar":"السرة","title_en":"Al sura","country_id":"1","created_at":"2017-08-08 05:30:18","updated_at":"-0001-11-30 00:00:00"},{"id":5,"title_ar":"السالمية","title_en":"Alsalmiya","country_id":"1","created_at":"2017-08-08 05:30:18","updated_at":"-0001-11-30 00:00:00"},{"id":6,"title_ar":"فحيحيل","title_en":"Fahahel","country_id":"1","created_at":"2017-08-08 05:30:18","updated_at":"-0001-11-30 00:00:00"},{"id":7,"title_ar":"منقف","title_en":"al mangaf","country_id":"1","created_at":"2017-08-08 05:30:18","updated_at":"-0001-11-30 00:00:00"},{"id":8,"title_ar":"الدسمة","title_en":"Aldasma","country_id":"1","created_at":"2017-08-08 05:30:18","updated_at":"-0001-11-30 00:00:00"},{"id":9,"title_ar":"شرق","title_en":"sharq","country_id":"1","created_at":"2017-08-08 05:30:18","updated_at":"-0001-11-30 00:00:00"},{"id":10,"title_ar":"ميدان حولي","title_en":"midan hawali","country_id":"1","created_at":"2017-08-08 05:30:18","updated_at":"-0001-11-30 00:00:00"},{"id":11,"title_ar":"الجهراء","title_en":"aljahra","country_id":"1","created_at":"2017-08-08 05:30:18","updated_at":"-0001-11-30 00:00:00"},{"id":12,"title_ar":"جابر الاحمد","title_en":"jaber al ahmed","country_id":"1","created_at":"2017-08-08 05:30:18","updated_at":"-0001-11-30 00:00:00"}]},{"id":2,"title_ar":"السعودية","title_en":"KSA","created_at":"2017-08-14 16:20:38","updated_at":"2017-10-23 09:17:22","deleted_at":null,"cities":[{"id":13,"title_ar":"p","title_en":"pE","country_id":"2","created_at":"2017-08-08 05:30:18","updated_at":"-0001-11-30 00:00:00"},{"id":14,"title_ar":"k","title_en":"kE","country_id":"2","created_at":"2017-08-08 05:30:18","updated_at":"-0001-11-30 00:00:00"}]},{"id":3,"title_ar":"الامارات","title_en":"UAE","created_at":"2017-08-14 16:20:38","updated_at":"2017-10-23 09:17:32","deleted_at":null,"cities":[{"id":15,"title_ar":"h","title_en":"hE","country_id":"3","created_at":"2017-08-08 05:30:18","updated_at":"-0001-11-30 00:00:00"}]},{"id":4,"title_ar":"البحرين","title_en":"Al Bahrain","created_at":"2017-08-14 16:20:38","updated_at":"2017-10-23 09:17:46","deleted_at":null,"cities":[{"id":16,"title_ar":"c","title_en":"cE","country_id":"4","created_at":"2017-08-08 05:30:18","updated_at":"-0001-11-30 00:00:00"}]},{"id":5,"title_ar":"اليمن","title_en":"Yamen","created_at":"2017-08-14 16:20:38","updated_at":"2017-10-23 09:18:00","deleted_at":null,"cities":[{"id":17,"title_ar":"n","title_en":"nE","country_id":"5","created_at":"2017-08-08 05:30:18","updated_at":"-0001-11-30 00:00:00"},{"id":18,"title_ar":"z","title_en":"zE","country_id":"5","created_at":"2017-08-08 05:30:18","updated_at":"-0001-11-30 00:00:00"},{"id":19,"title_ar":"v","title_en":"vE","country_id":"5","created_at":"2017-08-08 05:30:18","updated_at":"-0001-11-30 00:00:00"}]},{"id":6,"title_ar":"قطر","title_en":"Qatar","created_at":"2017-08-14 16:20:38","updated_at":"2017-10-23 09:18:17","deleted_at":null,"cities":[{"id":20,"title_ar":"o","title_en":"oE","country_id":"6","created_at":"2017-08-08 05:30:18","updated_at":"-0001-11-30 00:00:00"}]},{"id":7,"title_ar":"عمان","title_en":"Oman","created_at":"2017-08-14 16:20:38","updated_at":"2017-10-23 09:18:57","deleted_at":null,"cities":[]},{"id":8,"title_ar":"مصر","title_en":"Egypt","created_at":"2017-08-14 16:20:38","updated_at":"2017-10-23 09:19:20","deleted_at":null,"cities":[]},{"id":9,"title_ar":"اردن","title_en":" jordan","created_at":"2017-08-14 16:20:38","updated_at":"2017-10-23 10:45:24","deleted_at":null,"cities":[]},{"id":10,"title_ar":"ميدان حولي","title_en":"midan hawali","created_at":"2017-08-14 16:20:38","updated_at":"2017-10-23 10:45:12","deleted_at":null,"cities":[]},{"id":11,"title_ar":"الجهراء","title_en":"aljahra","created_at":"2017-11-08 12:20:29","updated_at":"2017-11-08 12:20:29","deleted_at":null,"cities":[]},{"id":12,"title_ar":"جابر الاحمد","title_en":"jaber al ahmed","created_at":"2017-11-08 12:20:53","updated_at":"2017-11-08 12:20:53","deleted_at":null,"cities":[]}]
     */

    private boolean status;
    private String messageA;
    private String messageE;
    private List<CountriesBean> countries;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessageA() {
        return messageA;
    }

    public void setMessageA(String messageA) {
        this.messageA = messageA;
    }

    public String getMessageE() {
        return messageE;
    }

    public void setMessageE(String messageE) {
        this.messageE = messageE;
    }

    public List<CountriesBean> getCountries() {
        if(countries ==  null){
            countries = new ArrayList<>();
        }
        return countries;
    }

    public void setCountries(List<CountriesBean> countries) {
        this.countries = countries;
    }

    public static class CountriesBean {
        /**
         * id : 1
         * title_ar : الكويت
         * title_en : Kuwait
         * created_at : 2017-08-14 16:20:38
         * updated_at : 2017-10-23 09:17:11
         * deleted_at : null
         * cities : [{"id":1,"title_ar":"مبارك","title_en":"Mobarak","country_id":"1","created_at":"2017-08-08 05:30:18","updated_at":"-0001-11-30 00:00:00"},{"id":2,"title_ar":"الاحمدي","title_en":"alAhmady","country_id":"1","created_at":"2017-08-08 05:30:18","updated_at":"-0001-11-30 00:00:00"},{"id":3,"title_ar":"حولي","title_en":"Hawali","country_id":"1","created_at":"2017-08-08 05:30:18","updated_at":"-0001-11-30 00:00:00"},{"id":4,"title_ar":"السرة","title_en":"Al sura","country_id":"1","created_at":"2017-08-08 05:30:18","updated_at":"-0001-11-30 00:00:00"},{"id":5,"title_ar":"السالمية","title_en":"Alsalmiya","country_id":"1","created_at":"2017-08-08 05:30:18","updated_at":"-0001-11-30 00:00:00"},{"id":6,"title_ar":"فحيحيل","title_en":"Fahahel","country_id":"1","created_at":"2017-08-08 05:30:18","updated_at":"-0001-11-30 00:00:00"},{"id":7,"title_ar":"منقف","title_en":"al mangaf","country_id":"1","created_at":"2017-08-08 05:30:18","updated_at":"-0001-11-30 00:00:00"},{"id":8,"title_ar":"الدسمة","title_en":"Aldasma","country_id":"1","created_at":"2017-08-08 05:30:18","updated_at":"-0001-11-30 00:00:00"},{"id":9,"title_ar":"شرق","title_en":"sharq","country_id":"1","created_at":"2017-08-08 05:30:18","updated_at":"-0001-11-30 00:00:00"},{"id":10,"title_ar":"ميدان حولي","title_en":"midan hawali","country_id":"1","created_at":"2017-08-08 05:30:18","updated_at":"-0001-11-30 00:00:00"},{"id":11,"title_ar":"الجهراء","title_en":"aljahra","country_id":"1","created_at":"2017-08-08 05:30:18","updated_at":"-0001-11-30 00:00:00"},{"id":12,"title_ar":"جابر الاحمد","title_en":"jaber al ahmed","country_id":"1","created_at":"2017-08-08 05:30:18","updated_at":"-0001-11-30 00:00:00"}]
         */

        private int id;
        private String title_ar;
        private String title_en;
        private String created_at;
        private String updated_at;
        private Object deleted_at;
        private List<CitiesBean> cities;
        private String display_title ;


        public String getDisplay_title() {
            if (ToolsUtils.getDeviceLang().equalsIgnoreCase(QuickstartPreferences.AR_LAN)) {
                display_title =  this.getTitle_ar();
            } else {
                display_title =  this.getTitle_en();
            }
            return display_title.trim();

        }

        public void setDisplay_title(String display_title) {
            this.display_title = display_title;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTitle_ar() {
            return title_ar;
        }

        public void setTitle_ar(String title_ar) {
            this.title_ar = title_ar;
        }

        public String getTitle_en() {
            return title_en;
        }

        public void setTitle_en(String title_en) {
            this.title_en = title_en;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public Object getDeleted_at() {
            return deleted_at;
        }

        public void setDeleted_at(Object deleted_at) {
            this.deleted_at = deleted_at;
        }

        public List<CitiesBean> getCities() {
            if(cities ==  null){
                new ArrayList<>();
            }
            return cities;
        }

        public void setCities(List<CitiesBean> cities) {
            this.cities = cities;
        }

        public static class CitiesBean {
            /**
             * id : 1
             * title_ar : مبارك
             * title_en : Mobarak
             * country_id : 1
             * created_at : 2017-08-08 05:30:18
             * updated_at : -0001-11-30 00:00:00
             */

            private int id;
            private String title_ar;
            private String title_en;
            private String country_id;
            private String created_at;
            private String updated_at;
            private String display_title ;

            public String getDisplay_title() {
                if (ToolsUtils.getDeviceLang().equalsIgnoreCase(QuickstartPreferences.AR_LAN)) {
                    display_title =  this.getTitle_ar();
                } else {
                    display_title =  this.getTitle_en();
                }
                return display_title.trim();

            }

            public void setDisplay_title(String display_title) {
                this.display_title = display_title;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getTitle_ar() {
                return title_ar;
            }

            public void setTitle_ar(String title_ar) {
                this.title_ar = title_ar;
            }

            public String getTitle_en() {
                return title_en;
            }

            public void setTitle_en(String title_en) {
                this.title_en = title_en;
            }

            public String getCountry_id() {
                return country_id;
            }

            public void setCountry_id(String country_id) {
                this.country_id = country_id;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }
        }
    }
}
