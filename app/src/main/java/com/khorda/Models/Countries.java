package com.khorda.Models;

import com.khorda.Utils.QuickstartPreferences;
import com.khorda.Utils.ToolsUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ahmed on 8/15/17.
 */

public class Countries {


    /**
     * status : true
     * messageA :
     * messageE :
     * countries : [{"id":1,"title_ar":"مدينة 1","title_en":"country 1","created_at":"2017-08-14 16:20:38","updated_at":"-0001-11-30 00:00:00","deleted_at":null},{"id":2,"title_ar":"مدينة 2","title_en":"country 2","created_at":"2017-08-14 16:20:38","updated_at":"-0001-11-30 00:00:00","deleted_at":null},{"id":3,"title_ar":"مدينة 3","title_en":"country 3","created_at":"2017-08-14 16:20:38","updated_at":"-0001-11-30 00:00:00","deleted_at":null},{"id":4,"title_ar":"مدينة 4","title_en":"country 4\r\n","created_at":"2017-08-14 16:20:38","updated_at":"-0001-11-30 00:00:00","deleted_at":null},{"id":5,"title_ar":"مدينة 5","title_en":"country 5","created_at":"2017-08-14 16:20:38","updated_at":"-0001-11-30 00:00:00","deleted_at":null},{"id":6,"title_ar":"مدينة 6","title_en":"country 6","created_at":"2017-08-14 16:20:38","updated_at":"-0001-11-30 00:00:00","deleted_at":null},{"id":7,"title_ar":"مدينة 7","title_en":"country 7","created_at":"2017-08-14 16:20:38","updated_at":"-0001-11-30 00:00:00","deleted_at":null},{"id":8,"title_ar":"مدينة 8","title_en":"country 8","created_at":"2017-08-14 16:20:38","updated_at":"-0001-11-30 00:00:00","deleted_at":null},{"id":9,"title_ar":"مدينة 9","title_en":"country 9","created_at":"2017-08-14 16:20:38","updated_at":"-0001-11-30 00:00:00","deleted_at":null},{"id":10,"title_ar":"مدينة 10","title_en":"country 10","created_at":"2017-08-14 16:20:38","updated_at":"-0001-11-30 00:00:00","deleted_at":null}]
     */

    private boolean status;
    private String messageA;
    private String messageE;
    private List<CountriesBean> countries;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessageA() {
        return messageA;
    }

    public void setMessageA(String messageA) {
        this.messageA = messageA;
    }

    public String getMessageE() {
        return messageE;
    }

    public void setMessageE(String messageE) {
        this.messageE = messageE;
    }

    public List<CountriesBean> getCountries() {

        if(countries == null){
            countries = new ArrayList<>();
        }
        return countries;
    }

    public void setCountries(List<CountriesBean> countries) {
        this.countries = countries;
    }

    public static class CountriesBean {
        /**
         * id : 1
         * title_ar : مدينة 1
         * title_en : country 1
         * created_at : 2017-08-14 16:20:38
         * updated_at : -0001-11-30 00:00:00
         * deleted_at : null
         */

        private int id;
        private String title_ar;
        private String title_en;
        private String created_at;
        private String updated_at;
        private Object deleted_at;
        private String display_title ;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTitle_ar() {
            return title_ar;
        }

        public void setTitle_ar(String title_ar) {
            this.title_ar = title_ar;
        }

        public String getTitle_en() {
            return title_en;
        }

        public void setTitle_en(String title_en) {
            this.title_en = title_en;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public Object getDeleted_at() {
            return deleted_at;
        }

        public void setDeleted_at(Object deleted_at) {
            this.deleted_at = deleted_at;
        }

        public String getDisplay_title() {
            if (ToolsUtils.getDeviceLang().equalsIgnoreCase(QuickstartPreferences.AR_LAN)) {
                display_title =  this.getTitle_ar();
            } else {
                display_title =  this.getTitle_en();
            }
            return display_title.trim();
        }

        public void setDisplay_title(String display_title) {
            this.display_title = display_title;
        }
    }
}
