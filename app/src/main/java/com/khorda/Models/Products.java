package com.khorda.Models;

import com.khorda.Utils.QuickstartPreferences;
import com.khorda.Utils.ToolsUtils;
import com.orm.SugarRecord;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ahmed on 8/8/17.
 */

public class Products implements Serializable {

    private boolean status;
    private String messageA;
    private String messageE;
    private ProductsBean products;
    private List<AdsBean> ads;


    public List<AdsBean> getAds() {
        if (ads == null) {
            ads = new ArrayList<>();
        }
        return ads;
    }

    public void setAds(List<AdsBean> ads) {
        this.ads = ads;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessageA() {
        return messageA;
    }

    public void setMessageA(String messageA) {
        this.messageA = messageA;
    }

    public String getMessageE() {
        return messageE;
    }

    public void setMessageE(String messageE) {
        this.messageE = messageE;
    }

    public ProductsBean getProducts() {
        return products;
    }

    public void setProducts(ProductsBean products) {
        this.products = products;
    }

    public static class ProductsBean implements Serializable {
        /**
         * current_page : 1
         * data : [{"id":1,"name":"الجلد الايطالي 1","nameE":"1italian skin","description":"<p>هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق. إذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد النص العربى مفيد لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان أن يطلع على صورة حقيقية لتصميم الموقع.1<\/p>","descriptionE":"<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).1<\/p>","category_id":{"id":1,"title_ar":"رياضة","title_en":"Sport"},"price":"150","likes":"4","views":"167","colors":[{"id":1,"title_ar":"aa","title_en":"aa"},{"id":2,"title_ar":"asاحمر","title_en":"redas"},{"id":3,"title_ar":"أزرق","title_en":"Blue"},{"id":4,"title_ar":"أخضر","title_en":"Green"},{"id":5,"title_ar":"أسود","title_en":"Black"},{"id":6,"title_ar":"bb","title_en":"bb"},{"id":7,"title_ar":"cc","title_en":"cc"},{"id":8,"title_ar":"dd","title_en":"dd"},{"id":9,"title_ar":"ابيض  xxx","title_en":"white xxx"}],"image":"http://localhost:8000/images/products/T0xx2n2LC9ZhpnZ20CKegX6ZRU3CvPQG63l07Kyh.jpeg","isdelete":"0","created_at":"2017-08-15 11:13:55","updated_at":"2017-08-15 16:13:55","deleted_at":null},{"id":2,"name":"ملابس الشتاء","nameE":"autumn shirt","description":"ومن هنا وجب على المصمم أن يضع نصوصا مؤقتة على التصميم ليظهر للعميل الشكل كاملاً،دور مولد النص العربى أن يوفر على المصمم عناء البحث عن نص بديل لا علاقة له بالموضوع الذى يتحدث عنه التصميم فيظهر بشكل لا يليق.\nهذا النص يمكن أن يتم تركيبه على أي تصميم دون مشكلة فلن يبدو وكأنه نص منسوخ، غير منظم، غير منسق، أو حتى غير مفهوم. لأنه مازال نصاً بديلاً ومؤقتاً.","descriptionE":"There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.","category_id":{"id":2,"title_ar":"صحة","title_en":"Health"},"price":"20","likes":"1","views":"115","colors":[{"id":2,"title_ar":"asاحمر","title_en":"redas"},{"id":4,"title_ar":"أخضر","title_en":"Green"},{"id":6,"title_ar":"bb","title_en":"bb"},{"id":9,"title_ar":"ابيض  xxx","title_en":"white xxx"}],"image":"http://kharda.linekw.com/images/products/T0xx2n2LC9ZhpnZ20CKegX6ZRU3CvPQG63l07Kyh.jpeg","isdelete":"0","created_at":"2017-08-06 09:52:08","updated_at":"2017-07-26 06:55:49","deleted_at":null},{"id":3,"name":"منتج","nameE":"product","description":"","descriptionE":"","category_id":{"id":3,"title_ar":"علوم","title_en":"Science"},"price":"2500","likes":"1","views":"0","colors":[{"id":1,"title_ar":"aa","title_en":"aa"},{"id":3,"title_ar":"أزرق","title_en":"Blue"},{"id":4,"title_ar":"أخضر","title_en":"Green"}],"image":"http://kharda.linekw.com/images/products/T0xx2n2LC9ZhpnZ20CKegX6ZRU3CvPQG63l07Kyh.jpeg","isdelete":"0","created_at":"2017-08-14 05:42:37","updated_at":"2017-08-14 02:42:37","deleted_at":null},{"id":4,"name":"منتج 2","nameE":"product 2","description":"<p>تفاصيل 2<\/p>\r\n","descriptionE":"<p>details 2<\/p>\r\n","category_id":{"id":4,"title_ar":"سياسة","title_en":"Politics"},"price":"2222","likes":"1","views":"0","colors":[{"id":2,"title_ar":"asاحمر","title_en":"redas"}],"image":"http://kharda.linekw.com/images/products/T0xx2n2LC9ZhpnZ20CKegX6ZRU3CvPQG63l07Kyh.jpeg","isdelete":"0","created_at":"2017-08-15 11:12:49","updated_at":"2017-08-15 16:12:49","deleted_at":null},{"id":5,"name":"منتج 3","nameE":"product 3","description":"<p>تفاصيل<\/p>\r\n","descriptionE":"<p>details<\/p>\r\n","category_id":{"id":5,"title_ar":"مطاعم","title_en":"Restaurants"},"price":"665","likes":"0","views":"0","colors":[{"id":5,"title_ar":"أسود","title_en":"Black"},{"id":9,"title_ar":"ابيض  xxx","title_en":"white xxx"}],"image":"http://kharda.linekw.com/images/products/T0xx2n2LC9ZhpnZ20CKegX6ZRU3CvPQG63l07Kyh.jpeg","isdelete":"0","created_at":"2017-08-06 09:52:40","updated_at":"2017-07-30 12:07:00","deleted_at":null}]
         * from : 1
         * last_page : 2
         * next_page_url : http://kharda.linekw.com/api/products?page=2
         * path : http://kharda.linekw.com/api/products
         * per_page : 5
         * prev_page_url : null
         * to : 5
         * total : 10
         */

        private int current_page;
        private int from;
        private int last_page;
        private String next_page_url;
        private String path;
        private int per_page;
        private Object prev_page_url;
        private int to;
        private int total;
        private List<DataBean> data;

        public int getCurrent_page() {
            return current_page;
        }

        public void setCurrent_page(int current_page) {
            this.current_page = current_page;
        }

        public int getFrom() {
            return from;
        }

        public void setFrom(int from) {
            this.from = from;
        }

        public int getLast_page() {
            return last_page;
        }

        public void setLast_page(int last_page) {
            this.last_page = last_page;
        }

        public String getNext_page_url() {
            return next_page_url;
        }

        public void setNext_page_url(String next_page_url) {
            this.next_page_url = next_page_url;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public int getPer_page() {
            return per_page;
        }

        public void setPer_page(int per_page) {
            this.per_page = per_page;
        }

        public Object getPrev_page_url() {
            return prev_page_url;
        }

        public void setPrev_page_url(Object prev_page_url) {
            this.prev_page_url = prev_page_url;
        }

        public int getTo() {
            return to;
        }

        public void setTo(int to) {
            this.to = to;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public List<DataBean> getData() {

            if (data == null) {
                data = new ArrayList<>();
            }
            return data;
        }

        public void setData(List<DataBean> data) {
            this.data = data;
        }

        public static class DataBean extends SugarRecord<DataBean> implements Serializable {

            private String product_id;
            private String name;
            private String nameE;
            private String description;
            private String descriptionE;
            private boolean isInCart ;
            private String display_description;
            private CategoryIdBean category_id;
            private String is_liked;
            private int SelectedQuantity = 1;
            private int quantity;

            public boolean isInCart() {
                return isInCart;
            }

            public void setInCart(boolean inCart) {
                isInCart = inCart;
            }

            public int getQuantity() {
                return quantity;
            }

            public void setQuantity(int quantity) {
                this.quantity = quantity;
            }

            public int getSelectedQuantity() {
                return SelectedQuantity;
            }

            public void setSelectedQuantity(int selectedQuantity) {
                this.SelectedQuantity = selectedQuantity;
            }

            public String getProduct_id() {
                return product_id;
            }

            public void setProduct_id(String product_id) {
                this.product_id = product_id;
            }

            public String getIs_liked() {
                return is_liked;
            }

            public void setIs_liked(String is_liked) {
                this.is_liked = is_liked;
            }

            public String getDisplay_description() {

                if (ToolsUtils.getDeviceLang().equalsIgnoreCase(QuickstartPreferences.AR_LAN)) {
                    display_description = this.getDescription();
                } else {
                    display_description = this.getDescriptionE();
                }
                return display_description.trim();
            }

            public void setDisplay_description(String display_description) {
                this.display_description = display_description;
            }

            private float price;
            private String likes;
            private String views;
            private List<String> images;
            private String isdelete;
            private String created_at;
            private String updated_at;
            private Object deleted_at;
            private ColorsBean colors;
            private String image;
            private String cat_Json = "";
            private Rate_product rate_product;

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                 this.image = image ;
            }

            public Rate_product getRate_product() {
                return rate_product;
            }

            public void setRate_product(Rate_product rate_product) {
                this.rate_product = rate_product;
            }

            public String getCat_Json() {
                return cat_Json;
            }

            public void setCat_Json(String cat_Json) {
                this.cat_Json = cat_Json;
            }

            public String getDisplay_name() {

                if (ToolsUtils.getDeviceLang().equalsIgnoreCase(QuickstartPreferences.AR_LAN)) {
                    display_name = this.getName();
                } else {
                    display_name = this.getNameE();
                }
                return display_name.trim();

            }

            public void setDisplay_name(String display_name) {
                this.display_name = display_name;
            }

            private String display_name;


            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getNameE() {
                return nameE;
            }

            public void setNameE(String nameE) {
                this.nameE = nameE;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getDescriptionE() {
                return descriptionE;
            }

            public void setDescriptionE(String descriptionE) {
                this.descriptionE = descriptionE;
            }

            public CategoryIdBean getCategory_id() {
                return category_id;
            }

            public void setCategory_id(CategoryIdBean category_id) {
                this.category_id = category_id;
            }

            public float getPrice() {
                return price;
            }

            public void setPrice(float price) {
                this.price = price;
            }

            public String getLikes() {
                return likes;
            }

            public void setLikes(String likes) {
                this.likes = likes;
            }

            public String getViews() {
                return views;
            }

            public void setViews(String views) {
                this.views = views;
            }

            public List<String> getImages() {

                if (images == null) {
                    images = new ArrayList<>();
                }
                return images;
            }

            public void setImages(List<String> images) {
                this.images = images;
            }

            public String getIsdelete() {
                return isdelete;
            }

            public void setIsdelete(String isdelete) {
                this.isdelete = isdelete;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }

            public Object getDeleted_at() {
                return deleted_at;
            }

            public void setDeleted_at(Object deleted_at) {
                this.deleted_at = deleted_at;
            }

            public ColorsBean getColors() {
                return colors;
            }

            public void setColors(ColorsBean colors) {
                this.colors = colors;
            }

            public static class CategoryIdBean implements Serializable {
                /**
                 * id : 1
                 * title_ar : رياضة
                 * title_en : Sport
                 */

                private int id;
                private String title_ar;
                private String title_en;
                private String display_title;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getTitle_ar() {
                    return title_ar;
                }

                public void setTitle_ar(String title_ar) {
                    this.title_ar = title_ar;
                }

                public String getTitle_en() {
                    return title_en;
                }

                public void setTitle_en(String title_en) {
                    this.title_en = title_en;
                }

                public String getDisplay_title() {
                    if (ToolsUtils.getDeviceLang().equalsIgnoreCase(QuickstartPreferences.AR_LAN)) {
                        display_title = this.getTitle_ar();
                    } else {
                        display_title = this.getTitle_en();
                    }
                    return display_title.trim();

                }

                public void setDisplay_title(String display_title) {
                    this.display_title = display_title;
                }


            }

            public static class ColorsBean implements Serializable {
                /**
                 * id : 1
                 * title_ar : aa
                 * title_en : aa
                 */

                private int id;
                private String title_ar;
                private String title_en;
                private String display_title;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getTitle_ar() {
                    return title_ar;
                }

                public void setTitle_ar(String title_ar) {
                    this.title_ar = title_ar;
                }

                public String getTitle_en() {
                    return title_en;
                }

                public void setTitle_en(String title_en) {
                    this.title_en = title_en;
                }

                public String getDisplay_title() {

                    if (ToolsUtils.getDeviceLang().equalsIgnoreCase(QuickstartPreferences.AR_LAN)) {
                        display_title = this.getTitle_ar();
                    } else {
                        display_title = this.getTitle_en();
                    }
                    return this.display_title;
                }

                public void setDisplay_title(String display_title) {
                    this.display_title = display_title;
                }
            }

            public static class Rate_product implements Serializable {
                private int rate;
                private int count;

                public int getRate() {
                    return rate;
                }

                public void setRate(int rate) {
                    this.rate = rate;
                }

                public int getCount() {
                    return count;
                }

                public void setCount(int count) {
                    this.count = count;
                }
            }


        }
    }

    public static class AdsBean implements Serializable {
        /**
         * id : 1
         * image : http://kharda.linekw.com/images/ads/1503928774_29803.jpg
         * created_at : 2017-08-28 12:00:49
         * updated_at : 2017-08-28 13:59:34
         */

        private int id;
        private String image;
        private String created_at;
        private String updated_at;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }
    }
}
