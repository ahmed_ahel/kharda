package com.khorda.Models;

import com.khorda.Utils.QuickstartPreferences;
import com.khorda.Utils.ToolsUtils;

/**
 * Created by ahmed on 8/15/17.
 */

public class UserInfo {


    /**
     * status : true
     * messageA : تمت العملية بنجاح
     * messageE : done successfully
     * user : {"id":65,"fullname":"ahmed","country_id":{"id":2,"title_ar":"مدينة 2","title_en":"country 2"},"email":"ahmed@gmail.com","mobile":"132123123000","api_token":"x8Ci0Oe8giBK807tR8A3UDG4wkXEUWIAYYuhRtsjtYEM1gLyeI5Yd1FhHo7I"}
     */

    private boolean status;
    private String messageA;
    private String messageE;
    private String display_message;
    private UserBean user;
    private boolean success ;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessageA() {
        return messageA;
    }

    public void setMessageA(String messageA) {
        this.messageA = messageA;
    }

    public String getMessageE() {
        return messageE;
    }

    public void setMessageE(String messageE) {
        this.messageE = messageE;
    }

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }
    public String getDisplay_message() {
        if (ToolsUtils.getDeviceLang().equalsIgnoreCase(QuickstartPreferences.AR_LAN)) {
            display_message =  this.getMessageA();
        } else {
            display_message =  this.getMessageE();
        }
        return display_message;

    }

    public void setDisplay_message(String display_message) {
        this.display_message = display_message;
    }

    public static class UserBean {
        /**
         * id : 65
         * fullname : ahmed
         * country_id : {"id":2,"title_ar":"مدينة 2","title_en":"country 2"}
         * email : ahmed@gmail.com
         * mobile : 132123123000
         * api_token : x8Ci0Oe8giBK807tR8A3UDG4wkXEUWIAYYuhRtsjtYEM1gLyeI5Yd1FhHo7I
         */

        private int id;
        private String fullname;
        private CountryIdBean country_id;
        private String email;
        private String mobile;
        private String api_token;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getFullname() {
            return fullname;
        }

        public void setFullname(String fullname) {
            this.fullname = fullname;
        }

        public CountryIdBean getCountry_id() {
            return country_id;
        }

        public void setCountry_id(CountryIdBean country_id) {
            this.country_id = country_id;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getApi_token() {
            return api_token;
        }

        public void setApi_token(String api_token) {
            this.api_token = api_token;
        }

        public static class CountryIdBean {
            /**
             * id : 2
             * title_ar : مدينة 2
             * title_en : country 2
             */

            private int id;
            private String title_ar;
            private String title_en;
            private String display_title ;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getTitle_ar() {
                return title_ar;
            }

            public void setTitle_ar(String title_ar) {
                this.title_ar = title_ar;
            }

            public String getTitle_en() {
                return title_en;
            }

            public void setTitle_en(String title_en) {
                this.title_en = title_en;
            }

            public String getDisplay_title() {
                if (ToolsUtils.getDeviceLang().equalsIgnoreCase(QuickstartPreferences.AR_LAN)) {
                    display_title =  this.getTitle_ar();
                } else {
                    display_title =  this.getTitle_en();
                }
                return display_title.trim();
            }

            public void setDisplay_title(String display_title) {
                this.display_title = display_title;
            }
        }
    }
}
