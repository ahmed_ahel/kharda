package com.khorda.Models;

import com.khorda.Utils.QuickstartPreferences;
import com.khorda.Utils.ToolsUtils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ahmed on 8/15/17.
 */

public class Colors {


    /**
     * status : true
     * messageA :
     * messageE :
     * colors : [{"id":1,"title_ar":"aa","title_en":"aa","created_at":"2017-08-06 09:50:14","updated_at":"-0001-11-30 00:00:00"},{"id":2,"title_ar":"asاحمر","title_en":"redas","created_at":"2017-08-05 09:24:56","updated_at":"2017-08-06 02:50:45"},{"id":3,"title_ar":"أزرق","title_en":"Blue","created_at":"2017-08-05 09:24:56","updated_at":"-0001-11-30 00:00:00"},{"id":4,"title_ar":"أخضر","title_en":"Green","created_at":"2017-08-05 09:24:56","updated_at":"-0001-11-30 00:00:00"},{"id":5,"title_ar":"أسود","title_en":"Black","created_at":"2017-08-05 09:24:56","updated_at":"-0001-11-30 00:00:00"},{"id":6,"title_ar":"bb","title_en":"bb","created_at":"2017-08-06 09:50:14","updated_at":"-0001-11-30 00:00:00"},{"id":7,"title_ar":"cc","title_en":"cc","created_at":"2017-08-06 09:50:14","updated_at":"-0001-11-30 00:00:00"},{"id":8,"title_ar":"dd","title_en":"dd","created_at":"2017-08-06 09:50:14","updated_at":"-0001-11-30 00:00:00"},{"id":9,"title_ar":"ابيض  xxx","title_en":"white xxx","created_at":"2017-08-05 23:31:14","updated_at":"2017-08-06 02:26:33"}]
     */

    private boolean status;
    private String messageA;
    private String messageE;
    private List<ColorsBean> colors;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessageA() {
        return messageA;
    }

    public void setMessageA(String messageA) {
        this.messageA = messageA;
    }

    public String getMessageE() {
        return messageE;
    }

    public void setMessageE(String messageE) {
        this.messageE = messageE;
    }

    public List<ColorsBean> getColors() {
        if(colors == null){
            colors = new ArrayList<>();
        }
        return colors;
    }

    public void setColors(List<ColorsBean> colors) {
        this.colors = colors;
    }

    public static class ColorsBean {
        /**
         * id : 1
         * title_ar : aa
         * title_en : aa
         * created_at : 2017-08-06 09:50:14
         * updated_at : -0001-11-30 00:00:00
         */

        private int id;
        private String title_ar;
        private String title_en;
        private String created_at;
        private String updated_at;
        private String display_title ;

        public String getDisplay_title() {

            if (ToolsUtils.getDeviceLang().equalsIgnoreCase(QuickstartPreferences.AR_LAN)) {
                display_title =  this.getTitle_ar();
            } else {
                display_title =  this.getTitle_en();
            }
            return display_title.trim();
        }

        public void setDisplay_title(String display_title) {
            this.display_title = display_title;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTitle_ar() {
            return title_ar;
        }

        public void setTitle_ar(String title_ar) {
            this.title_ar = title_ar;
        }

        public String getTitle_en() {
            return title_en;
        }

        public void setTitle_en(String title_en) {
            this.title_en = title_en;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }
    }
}
