package com.khorda.Models;

import com.orm.SugarRecord;

import java.io.Serializable;

/**
 * Created by ez on 8/21/2017.
 */

public class Payment extends SugarRecord<Payment> implements Serializable {


    String FullName;
    String Email;
    String mobile;
    int CityId;
    int CountryId ;
    String address;
    String Data;
    int Type = 0;

    public int getCountryId() {
        return CountryId;
    }

    public void setCountryId(int countryId) {
        CountryId = countryId;
    }

    public String getData() {
        return Data;
    }

    public void setData(String data) {
        Data = data;
    }

    public int getType() {
        return Type;
    }

    public void setType(int type) {
        Type = type;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }


    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public int getCityId() {
        return CityId;
    }

    public void setCityId(int cityId) {
        CityId = cityId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Full Name:" + getFullName() +
                        "Mobile :" + getMobile() +
                        "Email:" + getEmail() +
                        "City Id:" + getCityId() +
                        "Address:" + getAddress()+
                        "Data:" + getData()+
                        "Type:" + getType();
    }
}
