package com.khorda.Models;

import com.khorda.Utils.QuickstartPreferences;
import com.khorda.Utils.ToolsUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ahmed on 8/15/17.
 */

public class Categories {


    /**
     * status : true
     * messageA :
     * messageE :
     * categories : [{"id":1,"title_ar":"رياضة","title_en":"Sport","image":"http://kharda.linekw.com/images/categories/sport.png","description_ar":"Sunt voluptas ex porro non. Voluptatem deleniti quod autem. Vero nulla et reiciendis inventore. Reprehenderit illum ea debitis est autem reiciendis sed.","description_en":"Iste et adipisci cumque ipsa corrupti illo suscipit. Ut omnis facere minus. Sed iusto non perspiciatis molestiae. Laboriosam architecto repudiandae velit et provident.","colour":"#4286f4","created_at":"2017-07-15 05:06:28","updated_at":"2017-07-14 04:50:13"},{"id":2,"title_ar":"صحة","title_en":"Health","image":"http://kharda.linekw.com/images/categories/health.png","description_ar":"Veniam et voluptas sint ut doloribus quam corrupti. Deleniti ut a atque dolor dolorem sit. Quasi impedit alias est deserunt. Aliquam natus distinctio dignissimos itaque fugit consequatur.","description_en":"Inventore porro et sequi sint aliquid repellat sunt sunt. Cum optio qui est. Et nihil est placeat ut.","colour":"#0dba1e","created_at":"2017-07-15 05:07:08","updated_at":"2017-07-14 04:50:13"},{"id":3,"title_ar":"علوم","title_en":"Science","image":"http://kharda.linekw.com/images/categories/science.png","description_ar":"Libero aspernatur aut placeat soluta voluptas sit. Sed pariatur beatae officiis dolorem consequatur. Rerum exercitationem quo dicta omnis atque non. Molestiae laudantium quae doloribus et est.","description_en":"Maiores fugit nihil repellendus ipsa dignissimos. Quos possimus exercitationem corrupti temporibus adipisci et quidem. Fuga consequatur animi non quis pariatur. Harum non molestias facilis est est.","colour":"#db068d","created_at":"2017-07-15 05:06:39","updated_at":"2017-07-14 04:50:13"},{"id":4,"title_ar":"سياسة","title_en":"Politics","image":"http://kharda.linekw.com/images/categories/politics.png","description_ar":"Est ipsam quas dolorem minus. Error dolore et a nisi.\nSunt inventore tempora et enim. Necessitatibus iusto provident quis labore eligendi eius maiores est.","description_en":"Cupiditate omnis officiis sint voluptas et quo. Quasi quia provident exercitationem et ipsa. Labore soluta consequatur cupiditate nobis. Nihil et voluptatum quae laudantium.","colour":"#f20428","created_at":"2017-07-15 05:07:00","updated_at":"2017-07-14 04:50:13"},{"id":5,"title_ar":"مطاعم","title_en":"Restaurants","image":"http://kharda.linekw.com/images/categories/restaurants.png","description_ar":"Saepe accusamus aliquid eligendi rerum atque quaerat. Sit ea nostrum illum consequatur fugiat. Qui quos voluptatem veritatis rerum.","description_en":"Eligendi porro qui odio aperiam. Ducimus iusto ut itaque nihil. In accusamus quia officiis recusandae. Aut eum cumque quo eveniet illum.","colour":"#f17703","created_at":"2017-07-15 05:06:48","updated_at":"2017-07-14 04:50:13"},{"id":6,"title_ar":"تصنيف جديد","title_en":"new category","image":"http://kharda.linekw.com/images/categories/wLz4Xi8UrQdNjFZh5tFYSbKAGDTLHM8d8Dj3fCzh.jpeg","description_ar":"تصنيف جديد تصنيف جديد تصنيف جديد تصنيف جديد تصنيف جديد","description_en":"new category new category new category new category new category","colour":"#c7c7c7","created_at":"2017-07-20 05:19:40","updated_at":"2017-07-20 05:19:40"},{"id":7,"title_ar":"as","title_en":"as","image":"http://kharda.linekw.com/images/categories/n7WSLL9XC8c6LCuCiFwvXInJzmE6rrxJ6VyHzVaC.jpeg","description_ar":"<p>as<\/p>","description_en":"<p>as<\/p>","colour":"#80ff00","created_at":"2017-08-06 02:22:45","updated_at":"2017-08-06 02:22:45"}]
     */

    private boolean status;
    private String messageA;
    private String messageE;
    private List<CategoriesBean> categories;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessageA() {
        return messageA;
    }

    public void setMessageA(String messageA) {
        this.messageA = messageA;
    }

    public String getMessageE() {
        return messageE;
    }

    public void setMessageE(String messageE) {
        this.messageE = messageE;
    }

    public List<CategoriesBean> getCategories() {

        if(categories == null){
            categories = new ArrayList<>();
        }
        return categories;
    }

    public void setCategories(List<CategoriesBean> categories) {
        this.categories = categories;
    }

    public static class CategoriesBean {
        /**
         * id : 1
         * title_ar : رياضة
         * title_en : Sport
         * image : http://kharda.linekw.com/images/categories/sport.png
         * description_ar : Sunt voluptas ex porro non. Voluptatem deleniti quod autem. Vero nulla et reiciendis inventore. Reprehenderit illum ea debitis est autem reiciendis sed.
         * description_en : Iste et adipisci cumque ipsa corrupti illo suscipit. Ut omnis facere minus. Sed iusto non perspiciatis molestiae. Laboriosam architecto repudiandae velit et provident.
         * colour : #4286f4
         * created_at : 2017-07-15 05:06:28
         * updated_at : 2017-07-14 04:50:13
         */

        private int id;
        private String title_ar;
        private String title_en;
        private String display_title ;
        private String image;
        private String description_ar;
        private String description_en;
        private String colour;
        private String created_at;
        private String updated_at;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getDisplay_title() {
            if (ToolsUtils.getDeviceLang().equalsIgnoreCase(QuickstartPreferences.AR_LAN)) {
                display_title =  this.getTitle_ar();
            } else {
                display_title =  this.getTitle_en();
            }
            return display_title.trim();
        }

        public void setDisplay_title(String display_title) {
            this.display_title = display_title;
        }

        public String getTitle_ar() {
            return title_ar;
        }

        public void setTitle_ar(String title_ar) {
            this.title_ar = title_ar;
        }

        public String getTitle_en() {
            return title_en;
        }

        public void setTitle_en(String title_en) {
            this.title_en = title_en;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getDescription_ar() {
            return description_ar;
        }

        public void setDescription_ar(String description_ar) {
            this.description_ar = description_ar;
        }

        public String getDescription_en() {
            return description_en;
        }

        public void setDescription_en(String description_en) {
            this.description_en = description_en;
        }

        public String getColour() {
            return colour;
        }

        public void setColour(String colour) {
            this.colour = colour;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }
    }
}
