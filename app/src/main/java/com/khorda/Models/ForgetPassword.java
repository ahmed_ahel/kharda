package com.khorda.Models;

import com.khorda.Utils.QuickstartPreferences;
import com.khorda.Utils.ToolsUtils;

/**
 * Created by ahmed on 8/19/17.
 */

public class ForgetPassword {


    /**
     * status : false
     * code : 400
     * message_ar : We can't find a user with that e-mail address.
     * message_en : We can't find a user with that e-mail address.
     * pages : {"total":null,"per_page":null,"current_page":null,"last_page":null,"next_page_url":null,"prev_page_url":null,"from":null,"to":null}
     * items : null
     */

    private boolean status;
    private int code;
    private String message_ar;
    private String message_en;
    private String display_message ;
    private PagesBean pages;
    private Object items;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage_ar() {
        return message_ar;
    }

    public void setMessage_ar(String message_ar) {
        this.message_ar = message_ar;
    }

    public String getMessage_en() {
        return message_en;
    }

    public void setMessage_en(String message_en) {
        this.message_en = message_en;
    }

    public PagesBean getPages() {
        return pages;
    }

    public String getDisplay_message() {

        if (ToolsUtils.getDeviceLang().equalsIgnoreCase(QuickstartPreferences.AR_LAN)) {
            display_message =  this.getMessage_ar();
        } else {
            display_message =  this.getMessage_en();
        }
        return display_message.trim();

    }

    public void setDisplay_message(String display_message) {
        this.display_message = display_message;
    }

    public void setPages(PagesBean pages) {
        this.pages = pages;

    }

    public Object getItems() {
        return items;
    }

    public void setItems(Object items) {
        this.items = items;
    }

    public static class PagesBean {
        /**
         * total : null
         * per_page : null
         * current_page : null
         * last_page : null
         * next_page_url : null
         * prev_page_url : null
         * from : null
         * to : null
         */

        private Object total;
        private Object per_page;
        private Object current_page;
        private Object last_page;
        private Object next_page_url;
        private Object prev_page_url;
        private Object from;
        private Object to;

        public Object getTotal() {
            return total;
        }

        public void setTotal(Object total) {
            this.total = total;
        }

        public Object getPer_page() {
            return per_page;
        }

        public void setPer_page(Object per_page) {
            this.per_page = per_page;
        }

        public Object getCurrent_page() {
            return current_page;
        }

        public void setCurrent_page(Object current_page) {
            this.current_page = current_page;
        }

        public Object getLast_page() {
            return last_page;
        }

        public void setLast_page(Object last_page) {
            this.last_page = last_page;
        }

        public Object getNext_page_url() {
            return next_page_url;
        }

        public void setNext_page_url(Object next_page_url) {
            this.next_page_url = next_page_url;
        }

        public Object getPrev_page_url() {
            return prev_page_url;
        }

        public void setPrev_page_url(Object prev_page_url) {
            this.prev_page_url = prev_page_url;
        }

        public Object getFrom() {
            return from;
        }

        public void setFrom(Object from) {
            this.from = from;
        }

        public Object getTo() {
            return to;
        }

        public void setTo(Object to) {
            this.to = to;
        }
    }
}
