package com.khorda.Models;

import com.khorda.Utils.QuickstartPreferences;
import com.khorda.Utils.ToolsUtils;

/**
 * Created by ahmed on 8/22/17.
 */

public class PaymentResponse {


    /**
     * status : true
     * messageA : تم ارسال طلبك بنجاح
     * messageE : your order has been sent successfully
     * orders : {"link":"https://tahseeel.com/o/?id=65&hash=599bfa2184ffd","order":"1503394336_4343_7kXg"}
     */

    private boolean status;
    private String messageA;
    private String messageE;
    private OrdersBean orders;
    private String display_message ;


    public String getDisplay_message() {
        if (ToolsUtils.getDeviceLang().equalsIgnoreCase(QuickstartPreferences.AR_LAN)) {
            display_message =  this.getMessageA();
        } else {
            display_message =  this.getMessageE();
        }
        return display_message;


    }

    public void setDisplay_message(String display_message) {
        display_message = display_message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessageA() {
        return messageA;
    }

    public void setMessageA(String messageA) {
        this.messageA = messageA;
    }

    public String getMessageE() {
        return messageE;
    }

    public void setMessageE(String messageE) {
        this.messageE = messageE;
    }

    public OrdersBean getOrders() {
        return orders;
    }

    public void setOrders(OrdersBean orders) {
        this.orders = orders;
    }

    public static class OrdersBean {
        /**
         * link : https://tahseeel.com/o/?id=65&hash=599bfa2184ffd
         * order : 1503394336_4343_7kXg
         */

        private String link;
        private String order;

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }

        public String getOrder() {
            return order;
        }

        public void setOrder(String order) {
            this.order = order;
        }
    }
}
