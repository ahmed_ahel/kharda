package com.khorda.Models;

import com.khorda.Utils.QuickstartPreferences;
import com.khorda.Utils.ToolsUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ahmed on 8/19/17.
 */

public class MyPurchases {


    /**
     * status : true
     * messageA :
     * messageE :
     * purchases : {"current_page":1,"data":[{"id":6,"fullname":"apple","mobile":"","email":"aa@aa.com","user_id":"58","product_id":"3","quantity":"6","country_id":"1","address1":"address 1","status":"0","created_at":"2017-08-07 04:27:43","updated_at":"2017-08-08 01:06:24","deleted_at":null,"product":{"id":3,"name":"منتج","nameE":"product","description":"","descriptionE":"","category_id":{"id":3,"title_ar":"علوم","title_en":"Science"},"price":"2500","likes":"2","views":"0","colors":[{"id":7,"title_ar":"cc","title_en":"cc"}],"image":"http://kharda.linekw.com/images/products/APoM2MFoQnGKZcG0WxH3qpQ4b8y2XUt0TqRdZ8gv.jpeg","isdelete":"0","created_at":"2017-08-19 03:42:47","updated_at":"2017-08-19 08:34:34","deleted_at":null},"country":{"id":1,"title_ar":"مدينة 1","title_en":"country 1","created_at":"2017-08-14 16:20:38","updated_at":"-0001-11-30 00:00:00","deleted_at":null}},{"id":7,"fullname":"apple","mobile":"appleMark","email":"aa@aa.com","user_id":"58","product_id":"3","quantity":"6","country_id":"1","address1":"address 1","status":"1","created_at":"2017-08-14 13:41:30","updated_at":"2017-08-17 09:31:18","deleted_at":null,"product":{"id":3,"name":"منتج","nameE":"product","description":"","descriptionE":"","category_id":{"id":3,"title_ar":"علوم","title_en":"Science"},"price":"2500","likes":"2","views":"0","colors":[{"id":7,"title_ar":"cc","title_en":"cc"}],"image":"http://kharda.linekw.com/images/products/APoM2MFoQnGKZcG0WxH3qpQ4b8y2XUt0TqRdZ8gv.jpeg","isdelete":"0","created_at":"2017-08-19 03:42:47","updated_at":"2017-08-19 08:34:34","deleted_at":null},"country":{"id":1,"title_ar":"مدينة 1","title_en":"country 1","created_at":"2017-08-14 16:20:38","updated_at":"-0001-11-30 00:00:00","deleted_at":null}},{"id":8,"fullname":"apple","mobile":"98554774","email":"aa@aa.com","user_id":"58","product_id":"3","quantity":"6","country_id":"1","address1":"address 1","status":"0","created_at":"2017-08-14 13:41:59","updated_at":"2017-08-14 13:41:59","deleted_at":null,"product":{"id":3,"name":"منتج","nameE":"product","description":"","descriptionE":"","category_id":{"id":3,"title_ar":"علوم","title_en":"Science"},"price":"2500","likes":"2","views":"0","colors":[{"id":7,"title_ar":"cc","title_en":"cc"}],"image":"http://kharda.linekw.com/images/products/APoM2MFoQnGKZcG0WxH3qpQ4b8y2XUt0TqRdZ8gv.jpeg","isdelete":"0","created_at":"2017-08-19 03:42:47","updated_at":"2017-08-19 08:34:34","deleted_at":null},"country":{"id":1,"title_ar":"مدينة 1","title_en":"country 1","created_at":"2017-08-14 16:20:38","updated_at":"-0001-11-30 00:00:00","deleted_at":null}},{"id":9,"fullname":"apple","mobile":"98554774","email":"aa@aa.com","user_id":"58","product_id":"3","quantity":"6","country_id":"1","address1":"address 1","status":"2","created_at":"2017-08-16 03:46:23","updated_at":"2017-08-17 09:31:21","deleted_at":null,"product":{"id":3,"name":"منتج","nameE":"product","description":"","descriptionE":"","category_id":{"id":3,"title_ar":"علوم","title_en":"Science"},"price":"2500","likes":"2","views":"0","colors":[{"id":7,"title_ar":"cc","title_en":"cc"}],"image":"http://kharda.linekw.com/images/products/APoM2MFoQnGKZcG0WxH3qpQ4b8y2XUt0TqRdZ8gv.jpeg","isdelete":"0","created_at":"2017-08-19 03:42:47","updated_at":"2017-08-19 08:34:34","deleted_at":null},"country":{"id":1,"title_ar":"مدينة 1","title_en":"country 1","created_at":"2017-08-14 16:20:38","updated_at":"-0001-11-30 00:00:00","deleted_at":null}},{"id":10,"fullname":"apple","mobile":"985547741111","email":"aa@aa.com","user_id":"58","product_id":"3","quantity":"6","country_id":"1","address1":"address 1","status":"0","created_at":"2017-08-16 13:50:18","updated_at":"2017-08-16 13:50:18","deleted_at":null,"product":{"id":3,"name":"منتج","nameE":"product","description":"","descriptionE":"","category_id":{"id":3,"title_ar":"علوم","title_en":"Science"},"price":"2500","likes":"2","views":"0","colors":[{"id":7,"title_ar":"cc","title_en":"cc"}],"image":"http://kharda.linekw.com/images/products/APoM2MFoQnGKZcG0WxH3qpQ4b8y2XUt0TqRdZ8gv.jpeg","isdelete":"0","created_at":"2017-08-19 03:42:47","updated_at":"2017-08-19 08:34:34","deleted_at":null},"country":{"id":1,"title_ar":"مدينة 1","title_en":"country 1","created_at":"2017-08-14 16:20:38","updated_at":"-0001-11-30 00:00:00","deleted_at":null}},{"id":11,"fullname":"apple","mobile":"889977445566","email":"aa@aa.com","user_id":"58","product_id":"3","quantity":"6","country_id":"1","address1":"address 1","status":"0","created_at":"2017-08-16 13:50:31","updated_at":"2017-08-16 13:50:31","deleted_at":null,"product":{"id":3,"name":"منتج","nameE":"product","description":"","descriptionE":"","category_id":{"id":3,"title_ar":"علوم","title_en":"Science"},"price":"2500","likes":"2","views":"0","colors":[{"id":7,"title_ar":"cc","title_en":"cc"}],"image":"http://kharda.linekw.com/images/products/APoM2MFoQnGKZcG0WxH3qpQ4b8y2XUt0TqRdZ8gv.jpeg","isdelete":"0","created_at":"2017-08-19 03:42:47","updated_at":"2017-08-19 08:34:34","deleted_at":null},"country":{"id":1,"title_ar":"مدينة 1","title_en":"country 1","created_at":"2017-08-14 16:20:38","updated_at":"-0001-11-30 00:00:00","deleted_at":null}},{"id":12,"fullname":"apple","mobile":"889977445566","email":"aa@aa.com","user_id":"58","product_id":"3","quantity":"6","country_id":"1","address1":"address 1","status":"0","created_at":"2017-08-16 18:44:59","updated_at":"2017-08-16 18:44:59","deleted_at":null,"product":{"id":3,"name":"منتج","nameE":"product","description":"","descriptionE":"","category_id":{"id":3,"title_ar":"علوم","title_en":"Science"},"price":"2500","likes":"2","views":"0","colors":[{"id":7,"title_ar":"cc","title_en":"cc"}],"image":"http://kharda.linekw.com/images/products/APoM2MFoQnGKZcG0WxH3qpQ4b8y2XUt0TqRdZ8gv.jpeg","isdelete":"0","created_at":"2017-08-19 03:42:47","updated_at":"2017-08-19 08:34:34","deleted_at":null},"country":{"id":1,"title_ar":"مدينة 1","title_en":"country 1","created_at":"2017-08-14 16:20:38","updated_at":"-0001-11-30 00:00:00","deleted_at":null}},{"id":13,"fullname":"apple","mobile":"889977445566","email":"aa@aa.com","user_id":"58","product_id":"3","quantity":"6","country_id":"1","address1":"address 1","status":"0","created_at":"2017-08-18 16:39:04","updated_at":"2017-08-18 16:39:04","deleted_at":null,"product":{"id":3,"name":"منتج","nameE":"product","description":"","descriptionE":"","category_id":{"id":3,"title_ar":"علوم","title_en":"Science"},"price":"2500","likes":"2","views":"0","colors":[{"id":7,"title_ar":"cc","title_en":"cc"}],"image":"http://kharda.linekw.com/images/products/APoM2MFoQnGKZcG0WxH3qpQ4b8y2XUt0TqRdZ8gv.jpeg","isdelete":"0","created_at":"2017-08-19 03:42:47","updated_at":"2017-08-19 08:34:34","deleted_at":null},"country":{"id":1,"title_ar":"مدينة 1","title_en":"country 1","created_at":"2017-08-14 16:20:38","updated_at":"-0001-11-30 00:00:00","deleted_at":null}}],"from":1,"last_page":1,"next_page_url":null,"path":"http://kharda.linekw.com/api/myPurchases","per_page":"10","prev_page_url":null,"to":8,"total":8}
     */

    private boolean status;
    private String messageA;
    private String messageE;
    private PurchasesBean purchases;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessageA() {
        return messageA;
    }

    public void setMessageA(String messageA) {
        this.messageA = messageA;
    }

    public String getMessageE() {
        return messageE;
    }

    public void setMessageE(String messageE) {
        this.messageE = messageE;
    }

    public PurchasesBean getPurchases() {
        return purchases;
    }

    public void setPurchases(PurchasesBean purchases) {
        this.purchases = purchases;
    }

    public static class PurchasesBean {
        /**
         * current_page : 1
         * data : [{"id":6,"fullname":"apple","mobile":"","email":"aa@aa.com","user_id":"58","product_id":"3","quantity":"6","country_id":"1","address1":"address 1","status":"0","created_at":"2017-08-07 04:27:43","updated_at":"2017-08-08 01:06:24","deleted_at":null,"product":{"id":3,"name":"منتج","nameE":"product","description":"","descriptionE":"","category_id":{"id":3,"title_ar":"علوم","title_en":"Science"},"price":"2500","likes":"2","views":"0","colors":[{"id":7,"title_ar":"cc","title_en":"cc"}],"image":"http://kharda.linekw.com/images/products/APoM2MFoQnGKZcG0WxH3qpQ4b8y2XUt0TqRdZ8gv.jpeg","isdelete":"0","created_at":"2017-08-19 03:42:47","updated_at":"2017-08-19 08:34:34","deleted_at":null},"country":{"id":1,"title_ar":"مدينة 1","title_en":"country 1","created_at":"2017-08-14 16:20:38","updated_at":"-0001-11-30 00:00:00","deleted_at":null}},{"id":7,"fullname":"apple","mobile":"appleMark","email":"aa@aa.com","user_id":"58","product_id":"3","quantity":"6","country_id":"1","address1":"address 1","status":"1","created_at":"2017-08-14 13:41:30","updated_at":"2017-08-17 09:31:18","deleted_at":null,"product":{"id":3,"name":"منتج","nameE":"product","description":"","descriptionE":"","category_id":{"id":3,"title_ar":"علوم","title_en":"Science"},"price":"2500","likes":"2","views":"0","colors":[{"id":7,"title_ar":"cc","title_en":"cc"}],"image":"http://kharda.linekw.com/images/products/APoM2MFoQnGKZcG0WxH3qpQ4b8y2XUt0TqRdZ8gv.jpeg","isdelete":"0","created_at":"2017-08-19 03:42:47","updated_at":"2017-08-19 08:34:34","deleted_at":null},"country":{"id":1,"title_ar":"مدينة 1","title_en":"country 1","created_at":"2017-08-14 16:20:38","updated_at":"-0001-11-30 00:00:00","deleted_at":null}},{"id":8,"fullname":"apple","mobile":"98554774","email":"aa@aa.com","user_id":"58","product_id":"3","quantity":"6","country_id":"1","address1":"address 1","status":"0","created_at":"2017-08-14 13:41:59","updated_at":"2017-08-14 13:41:59","deleted_at":null,"product":{"id":3,"name":"منتج","nameE":"product","description":"","descriptionE":"","category_id":{"id":3,"title_ar":"علوم","title_en":"Science"},"price":"2500","likes":"2","views":"0","colors":[{"id":7,"title_ar":"cc","title_en":"cc"}],"image":"http://kharda.linekw.com/images/products/APoM2MFoQnGKZcG0WxH3qpQ4b8y2XUt0TqRdZ8gv.jpeg","isdelete":"0","created_at":"2017-08-19 03:42:47","updated_at":"2017-08-19 08:34:34","deleted_at":null},"country":{"id":1,"title_ar":"مدينة 1","title_en":"country 1","created_at":"2017-08-14 16:20:38","updated_at":"-0001-11-30 00:00:00","deleted_at":null}},{"id":9,"fullname":"apple","mobile":"98554774","email":"aa@aa.com","user_id":"58","product_id":"3","quantity":"6","country_id":"1","address1":"address 1","status":"2","created_at":"2017-08-16 03:46:23","updated_at":"2017-08-17 09:31:21","deleted_at":null,"product":{"id":3,"name":"منتج","nameE":"product","description":"","descriptionE":"","category_id":{"id":3,"title_ar":"علوم","title_en":"Science"},"price":"2500","likes":"2","views":"0","colors":[{"id":7,"title_ar":"cc","title_en":"cc"}],"image":"http://kharda.linekw.com/images/products/APoM2MFoQnGKZcG0WxH3qpQ4b8y2XUt0TqRdZ8gv.jpeg","isdelete":"0","created_at":"2017-08-19 03:42:47","updated_at":"2017-08-19 08:34:34","deleted_at":null},"country":{"id":1,"title_ar":"مدينة 1","title_en":"country 1","created_at":"2017-08-14 16:20:38","updated_at":"-0001-11-30 00:00:00","deleted_at":null}},{"id":10,"fullname":"apple","mobile":"985547741111","email":"aa@aa.com","user_id":"58","product_id":"3","quantity":"6","country_id":"1","address1":"address 1","status":"0","created_at":"2017-08-16 13:50:18","updated_at":"2017-08-16 13:50:18","deleted_at":null,"product":{"id":3,"name":"منتج","nameE":"product","description":"","descriptionE":"","category_id":{"id":3,"title_ar":"علوم","title_en":"Science"},"price":"2500","likes":"2","views":"0","colors":[{"id":7,"title_ar":"cc","title_en":"cc"}],"image":"http://kharda.linekw.com/images/products/APoM2MFoQnGKZcG0WxH3qpQ4b8y2XUt0TqRdZ8gv.jpeg","isdelete":"0","created_at":"2017-08-19 03:42:47","updated_at":"2017-08-19 08:34:34","deleted_at":null},"country":{"id":1,"title_ar":"مدينة 1","title_en":"country 1","created_at":"2017-08-14 16:20:38","updated_at":"-0001-11-30 00:00:00","deleted_at":null}},{"id":11,"fullname":"apple","mobile":"889977445566","email":"aa@aa.com","user_id":"58","product_id":"3","quantity":"6","country_id":"1","address1":"address 1","status":"0","created_at":"2017-08-16 13:50:31","updated_at":"2017-08-16 13:50:31","deleted_at":null,"product":{"id":3,"name":"منتج","nameE":"product","description":"","descriptionE":"","category_id":{"id":3,"title_ar":"علوم","title_en":"Science"},"price":"2500","likes":"2","views":"0","colors":[{"id":7,"title_ar":"cc","title_en":"cc"}],"image":"http://kharda.linekw.com/images/products/APoM2MFoQnGKZcG0WxH3qpQ4b8y2XUt0TqRdZ8gv.jpeg","isdelete":"0","created_at":"2017-08-19 03:42:47","updated_at":"2017-08-19 08:34:34","deleted_at":null},"country":{"id":1,"title_ar":"مدينة 1","title_en":"country 1","created_at":"2017-08-14 16:20:38","updated_at":"-0001-11-30 00:00:00","deleted_at":null}},{"id":12,"fullname":"apple","mobile":"889977445566","email":"aa@aa.com","user_id":"58","product_id":"3","quantity":"6","country_id":"1","address1":"address 1","status":"0","created_at":"2017-08-16 18:44:59","updated_at":"2017-08-16 18:44:59","deleted_at":null,"product":{"id":3,"name":"منتج","nameE":"product","description":"","descriptionE":"","category_id":{"id":3,"title_ar":"علوم","title_en":"Science"},"price":"2500","likes":"2","views":"0","colors":[{"id":7,"title_ar":"cc","title_en":"cc"}],"image":"http://kharda.linekw.com/images/products/APoM2MFoQnGKZcG0WxH3qpQ4b8y2XUt0TqRdZ8gv.jpeg","isdelete":"0","created_at":"2017-08-19 03:42:47","updated_at":"2017-08-19 08:34:34","deleted_at":null},"country":{"id":1,"title_ar":"مدينة 1","title_en":"country 1","created_at":"2017-08-14 16:20:38","updated_at":"-0001-11-30 00:00:00","deleted_at":null}},{"id":13,"fullname":"apple","mobile":"889977445566","email":"aa@aa.com","user_id":"58","product_id":"3","quantity":"6","country_id":"1","address1":"address 1","status":"0","created_at":"2017-08-18 16:39:04","updated_at":"2017-08-18 16:39:04","deleted_at":null,"product":{"id":3,"name":"منتج","nameE":"product","description":"","descriptionE":"","category_id":{"id":3,"title_ar":"علوم","title_en":"Science"},"price":"2500","likes":"2","views":"0","colors":[{"id":7,"title_ar":"cc","title_en":"cc"}],"image":"http://kharda.linekw.com/images/products/APoM2MFoQnGKZcG0WxH3qpQ4b8y2XUt0TqRdZ8gv.jpeg","isdelete":"0","created_at":"2017-08-19 03:42:47","updated_at":"2017-08-19 08:34:34","deleted_at":null},"country":{"id":1,"title_ar":"مدينة 1","title_en":"country 1","created_at":"2017-08-14 16:20:38","updated_at":"-0001-11-30 00:00:00","deleted_at":null}}]
         * from : 1
         * last_page : 1
         * next_page_url : null
         * path : http://kharda.linekw.com/api/myPurchases
         * per_page : 10
         * prev_page_url : null
         * to : 8
         * total : 8
         */

        private int current_page;
        private int from;
        private int last_page;
        private Object next_page_url;
        private String path;
        private String per_page;
        private Object prev_page_url;
        private int to;
        private int total;
        private List<DataBean> data;

        public int getCurrent_page() {
            return current_page;
        }

        public void setCurrent_page(int current_page) {
            this.current_page = current_page;
        }

        public int getFrom() {
            return from;
        }

        public void setFrom(int from) {
            this.from = from;
        }

        public int getLast_page() {
            return last_page;
        }

        public void setLast_page(int last_page) {
            this.last_page = last_page;
        }

        public Object getNext_page_url() {
            return next_page_url;
        }

        public void setNext_page_url(Object next_page_url) {
            this.next_page_url = next_page_url;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public String getPer_page() {
            return per_page;
        }

        public void setPer_page(String per_page) {
            this.per_page = per_page;
        }

        public Object getPrev_page_url() {
            return prev_page_url;
        }

        public void setPrev_page_url(Object prev_page_url) {
            this.prev_page_url = prev_page_url;
        }

        public int getTo() {
            return to;
        }

        public void setTo(int to) {
            this.to = to;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public List<DataBean> getData() {

            if(data == null){
                data = new ArrayList<>();
            }
            return data;
        }

        public void setData(List<DataBean> data) {
            this.data = data;
        }

        public static class DataBean {
            /**
             * id : 6
             * fullname : apple
             * mobile :
             * email : aa@aa.com
             * user_id : 58
             * product_id : 3
             * quantity : 6
             * country_id : 1
             * address1 : address 1
             * status : 0
             * created_at : 2017-08-07 04:27:43
             * updated_at : 2017-08-08 01:06:24
             * deleted_at : null
             * product : {"id":3,"name":"منتج","nameE":"product","description":"","descriptionE":"","category_id":{"id":3,"title_ar":"علوم","title_en":"Science"},"price":"2500","likes":"2","views":"0","colors":[{"id":7,"title_ar":"cc","title_en":"cc"}],"image":"http://kharda.linekw.com/images/products/APoM2MFoQnGKZcG0WxH3qpQ4b8y2XUt0TqRdZ8gv.jpeg","isdelete":"0","created_at":"2017-08-19 03:42:47","updated_at":"2017-08-19 08:34:34","deleted_at":null}
             * country : {"id":1,"title_ar":"مدينة 1","title_en":"country 1","created_at":"2017-08-14 16:20:38","updated_at":"-0001-11-30 00:00:00","deleted_at":null}
             */

            private int id;
            private String fullname;
            private String mobile;
            private String email;
            private String user_id;
            private String product_id;
            private String quantity;
            private String country_id;
            private String address1;
            private String status;
            private String created_at;
            private String updated_at;
            private Object deleted_at;
            private ProductBean product;
            private CountryBean country;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getFullname() {
                return fullname;
            }

            public void setFullname(String fullname) {
                this.fullname = fullname;
            }

            public String getMobile() {
                return mobile;
            }

            public void setMobile(String mobile) {
                this.mobile = mobile;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getUser_id() {
                return user_id;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }

            public String getProduct_id() {
                return product_id;
            }

            public void setProduct_id(String product_id) {
                this.product_id = product_id;
            }

            public String getQuantity() {
                return quantity;
            }

            public void setQuantity(String quantity) {
                this.quantity = quantity;
            }

            public String getCountry_id() {
                return country_id;
            }

            public void setCountry_id(String country_id) {
                this.country_id = country_id;
            }

            public String getAddress1() {
                return address1;
            }

            public void setAddress1(String address1) {
                this.address1 = address1;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }

            public Object getDeleted_at() {
                return deleted_at;
            }

            public void setDeleted_at(Object deleted_at) {
                this.deleted_at = deleted_at;
            }

            public ProductBean getProduct() {
                return product;
            }

            public void setProduct(ProductBean product) {
                this.product = product;
            }

            public CountryBean getCountry() {
                return country;
            }

            public void setCountry(CountryBean country) {
                this.country = country;
            }

            public static class ProductBean {
                /**
                 * id : 3
                 * name : منتج
                 * nameE : product
                 * description :
                 * descriptionE :
                 * category_id : {"id":3,"title_ar":"علوم","title_en":"Science"}
                 * price : 2500
                 * likes : 2
                 * views : 0
                 * colors : [{"id":7,"title_ar":"cc","title_en":"cc"}]
                 * image : http://kharda.linekw.com/images/products/APoM2MFoQnGKZcG0WxH3qpQ4b8y2XUt0TqRdZ8gv.jpeg
                 * isdelete : 0
                 * created_at : 2017-08-19 03:42:47
                 * updated_at : 2017-08-19 08:34:34
                 * deleted_at : null
                 */

                private int id;
                private String name;
                private String nameE;
                private String display_name ;
                private String description;
                private String descriptionE;
                private CategoryIdBean category_id;
                private float price;
                private int quantity ;


                public int getQuantity() {
                    return quantity;
                }

                public void setQuantity(int quantity) {
                    this.quantity = quantity;
                }

                public String getDisplay_name() {

                    if (ToolsUtils.getDeviceLang().equalsIgnoreCase(QuickstartPreferences.AR_LAN)) {
                        display_name =  this.getName();
                    } else {
                        display_name =  this.getNameE();
                    }
                    return display_name.trim();


                }

                public void setDisplay_name(String display_name) {
                    this.display_name = display_name;
                }

                private String likes;
                private String views;
                private String image;
                private String isdelete;
                private String created_at;
                private String updated_at;
                private Object deleted_at;
                private ColorsBean colors;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public String getNameE() {
                    return nameE;
                }

                public void setNameE(String nameE) {
                    this.nameE = nameE;
                }

                public String getDescription() {
                    return description;
                }

                public void setDescription(String description) {
                    this.description = description;
                }

                public String getDescriptionE() {
                    return descriptionE;
                }

                public void setDescriptionE(String descriptionE) {
                    this.descriptionE = descriptionE;
                }

                public CategoryIdBean getCategory_id() {
                    return category_id;
                }

                public void setCategory_id(CategoryIdBean category_id) {
                    this.category_id = category_id;
                }

                public float getPrice() {
                    return price;
                }

                public void setPrice(float price) {
                    this.price = price;
                }

                public String getLikes() {
                    return likes;
                }

                public void setLikes(String likes) {
                    this.likes = likes;
                }

                public String getViews() {
                    return views;
                }

                public void setViews(String views) {
                    this.views = views;
                }

                public String getImage() {
                    return image;
                }

                public void setImage(String image) {
                    this.image = image;
                }

                public String getIsdelete() {
                    return isdelete;
                }

                public void setIsdelete(String isdelete) {
                    this.isdelete = isdelete;
                }

                public String getCreated_at() {
                    return created_at;
                }

                public void setCreated_at(String created_at) {
                    this.created_at = created_at;
                }

                public String getUpdated_at() {
                    return updated_at;
                }

                public void setUpdated_at(String updated_at) {
                    this.updated_at = updated_at;
                }

                public Object getDeleted_at() {
                    return deleted_at;
                }

                public void setDeleted_at(Object deleted_at) {
                    this.deleted_at = deleted_at;
                }

                public ColorsBean getColors() {
                    return colors;
                }

                public void setColors(ColorsBean colors) {
                    this.colors = colors;
                }

                public static class CategoryIdBean {
                    /**
                     * id : 3
                     * title_ar : علوم
                     * title_en : Science
                     */

                    private int id;
                    private String title_ar;
                    private String title_en;
                    private String display_title ;


                    public int getId() {
                        return id;
                    }

                    public void setId(int id) {
                        this.id = id;
                    }

                    public String getTitle_ar() {
                        return title_ar;
                    }

                    public void setTitle_ar(String title_ar) {
                        this.title_ar = title_ar;
                    }

                    public String getTitle_en() {
                        return title_en;
                    }

                    public void setTitle_en(String title_en) {
                        this.title_en = title_en;
                    }

                    public String getDisplay_title() {
                        if (ToolsUtils.getDeviceLang().equalsIgnoreCase(QuickstartPreferences.AR_LAN)) {
                            display_title =  this.getTitle_ar();
                        } else {
                            display_title =  this.getTitle_en();
                        }
                        return display_title.trim();
                    }

                    public void setDisplay_title(String display_title) {
                        this.display_title = display_title;
                    }
                }

                public static class ColorsBean {
                    /**
                     * id : 7
                     * title_ar : cc
                     * title_en : cc
                     */

                    private int id;
                    private String title_ar;
                    private String title_en;

                    public int getId() {
                        return id;
                    }

                    public void setId(int id) {
                        this.id = id;
                    }

                    public String getTitle_ar() {
                        return title_ar;
                    }

                    public void setTitle_ar(String title_ar) {
                        this.title_ar = title_ar;
                    }

                    public String getTitle_en() {
                        return title_en;
                    }

                    public void setTitle_en(String title_en) {
                        this.title_en = title_en;
                    }
                }
            }

            public static class CountryBean {
                /**
                 * id : 1
                 * title_ar : مدينة 1
                 * title_en : country 1
                 * created_at : 2017-08-14 16:20:38
                 * updated_at : -0001-11-30 00:00:00
                 * deleted_at : null
                 */

                private int id;
                private String title_ar;
                private String title_en;
                private String created_at;
                private String updated_at;
                private Object deleted_at;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getTitle_ar() {
                    return title_ar;
                }

                public void setTitle_ar(String title_ar) {
                    this.title_ar = title_ar;
                }

                public String getTitle_en() {
                    return title_en;
                }

                public void setTitle_en(String title_en) {
                    this.title_en = title_en;
                }

                public String getCreated_at() {
                    return created_at;
                }

                public void setCreated_at(String created_at) {
                    this.created_at = created_at;
                }

                public String getUpdated_at() {
                    return updated_at;
                }

                public void setUpdated_at(String updated_at) {
                    this.updated_at = updated_at;
                }

                public Object getDeleted_at() {
                    return deleted_at;
                }

                public void setDeleted_at(Object deleted_at) {
                    this.deleted_at = deleted_at;
                }
            }
        }
    }
}
