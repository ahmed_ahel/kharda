package com.khorda.Models;

import com.khorda.Utils.QuickstartPreferences;
import com.khorda.Utils.ToolsUtils;

import java.util.List;

/**
 * Created by ez on 8/17/2017.
 */

public class WalkThrough {


    /**
     * status : true
     * messageA :
     * messageE :
     * splash : [{"id":1,"title_ar":"دليل المستخدم 1","title_en":"Splash 1","image":"http://kharda.linekw.com/images/splashes/Ee0GSAmLYDCvVHdRTwLHA9A4yQPsQW8qZvebci5C.jpeg","created_at":"2017-08-17 09:40:39","updated_at":"2017-08-17 09:40:39","deleted_at":null},{"id":2,"title_ar":"دليل المستخدم 2","title_en":"Splash 2","image":"http://kharda.linekw.com/images/splashes/h9WtDdIAAtOsTcvBTqwP8oKZzn8FiibYuuXeZU1y.jpeg","created_at":"2017-08-17 09:41:26","updated_at":"2017-08-17 09:41:26","deleted_at":null},{"id":3,"title_ar":"دليل المستخدم 3","title_en":"Splash 3","image":"http://kharda.linekw.com/images/splashes/HkOZnsyu34LrB4kLAhMllJW5fx1QUegt9iOkp7cY.jpeg","created_at":"2017-08-17 09:41:51","updated_at":"2017-08-17 09:41:51","deleted_at":null}]
     */

    private boolean status;
    private String messageA;
    private String messageE;
    private List<SplashBean> splash;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessageA() {
        return messageA;
    }

    public void setMessageA(String messageA) {
        this.messageA = messageA;
    }

    public String getMessageE() {
        return messageE;
    }

    public void setMessageE(String messageE) {
        this.messageE = messageE;
    }

    public List<SplashBean> getSplash() {
        return splash;
    }

    public void setSplash(List<SplashBean> splash) {
        this.splash = splash;
    }

    public static class SplashBean {
        /**
         * id : 1
         * title_ar : دليل المستخدم 1
         * title_en : Splash 1
         * image : http://kharda.linekw.com/images/splashes/Ee0GSAmLYDCvVHdRTwLHA9A4yQPsQW8qZvebci5C.jpeg
         * created_at : 2017-08-17 09:40:39
         * updated_at : 2017-08-17 09:40:39
         * deleted_at : null
         */

        private int id;
        private String title_ar;
        private String title_en;
        private String image;
        private String created_at;
        private String updated_at;
        private Object deleted_at;
        private String display_title;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTitle_ar() {
            return title_ar;
        }

        public void setTitle_ar(String title_ar) {
            this.title_ar = title_ar;
        }

        public String getTitle_en() {
            return title_en;
        }

        public void setTitle_en(String title_en) {
            this.title_en = title_en;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public Object getDeleted_at() {
            return deleted_at;
        }

        public void setDeleted_at(Object deleted_at) {
            this.deleted_at = deleted_at;
        }


        public String getDisplay_title() {

            if (ToolsUtils.getDeviceLang().equalsIgnoreCase(QuickstartPreferences.AR_LAN)) {
                display_title =  this.getTitle_ar();
            } else {
                display_title =  this.getTitle_en();
            }
            return display_title;
        }

        public void setDisplay_title(String display_title) {
            this.display_title = display_title;
        }
    }
}
