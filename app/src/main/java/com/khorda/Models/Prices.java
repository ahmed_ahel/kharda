package com.khorda.Models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ahmed on 8/15/17.
 */

public class Prices {


    /**
     * status : true
     * messageA :
     * messageE :
     * Prices : [{"id":1,"priceFrom":"100","priceTo":"200","created_at":"2017-08-06 06:33:13","updated_at":"-0001-11-30 00:00:00"},{"id":2,"priceFrom":"500","priceTo":"15000","created_at":"2017-08-06 03:41:48","updated_at":"2017-08-06 03:57:09"}]
     */

    private boolean status;
    private String messageA;
    private String messageE;
    private List<PricesBean> prices;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessageA() {
        return messageA;
    }

    public void setMessageA(String messageA) {
        this.messageA = messageA;
    }

    public String getMessageE() {
        return messageE;
    }

    public void setMessageE(String messageE) {
        this.messageE = messageE;
    }

    public List<PricesBean> getPrices() {

        if(prices == null){
            prices = new ArrayList<>();
        }
        return prices;
    }

    public void setPrices(List<PricesBean> prices) {
        this.prices = prices;
    }

    public static class PricesBean {
        /**
         * id : 1
         * priceFrom : 100
         * priceTo : 200
         * created_at : 2017-08-06 06:33:13
         * updated_at : -0001-11-30 00:00:00
         */

        private int id;
        private String priceFrom;
        private String priceTo;
        private String created_at;
        private String updated_at;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getPriceFrom() {
            return priceFrom;
        }

        public void setPriceFrom(String priceFrom) {
            this.priceFrom = priceFrom;
        }

        public String getPriceTo() {
            return priceTo;
        }

        public void setPriceTo(String priceTo) {
            this.priceTo = priceTo;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }
    }
}
