package com.khorda.Models;

/**
 * Created by ahmed on 8/15/17.
 */

public class PrivacyPolicy {


    /**
     * status : true
     * messageA :
     * messageE :
     * privacy_policy : <p>Done Privay Policy Done Privay Policy Done Privay Policy Done Privay Policy Done Privay Policy Done Privay Policy Done Privay Policy Done Privay Policy Done Privay Policy Done Privay Policy Done Privay Policy Done Privay Policy Done</p>
     */

    private boolean status;
    private String messageA;
    private String messageE;
    private String privacy_policy;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessageA() {
        return messageA;
    }

    public void setMessageA(String messageA) {
        this.messageA = messageA;
    }

    public String getMessageE() {
        return messageE;
    }

    public void setMessageE(String messageE) {
        this.messageE = messageE;
    }

    public String getPrivacy_policy() {
        return privacy_policy;
    }

    public void setPrivacy_policy(String privacy_policy) {
        this.privacy_policy = privacy_policy;
    }
}
