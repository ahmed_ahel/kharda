package com.khorda.Models;

/**
 * Created by ez on 8/18/2017.
 */

public class TermsConditions {


    /**
     * status : true
     * messageA :
     * messageE :
     * terms_conditions : <p>terms_conditions terms_conditions terms_conditions terms_conditions terms_conditions terms_conditions terms_conditions terms_conditions terms_conditions terms_conditions <span class="marker">terms_conditions </span>terms_conditions <strong>terms_conditions terms_conditions <span class="marker">terms_conditions </span>terms_conditions terms_conditions </strong>terms_conditions terms_conditions terms_conditions terms_conditions terms_conditions terms_conditions terms_conditions terms_conditions terms_conditions terms_conditions <big>terms_conditions </big>terms_conditions</p>
     */

    private boolean status;
    private String messageA;
    private String messageE;
    private String terms_conditions;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessageA() {
        return messageA;
    }

    public void setMessageA(String messageA) {
        this.messageA = messageA;
    }

    public String getMessageE() {
        return messageE;
    }

    public void setMessageE(String messageE) {
        this.messageE = messageE;
    }

    public String getTerms_conditions() {
        return terms_conditions;
    }

    public void setTerms_conditions(String terms_conditions) {
        this.terms_conditions = terms_conditions;
    }
}
