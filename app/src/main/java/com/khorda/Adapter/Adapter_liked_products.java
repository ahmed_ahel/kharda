package com.khorda.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.khorda.CustomViews.TextView;
import com.khorda.Models.Products;
import com.khorda.R;
import com.khorda.interfaces.OnItemClickListener;
import com.squareup.picasso.Picasso;

import java.util.List;


public class Adapter_liked_products extends RecyclerView.Adapter<Adapter_liked_products.ViewHolder> {

    private List<Products.ProductsBean.DataBean> mlist;
    private Context mContext;
    private OnItemClickListener listener;


    public Adapter_liked_products(Context mContext, List<Products.ProductsBean.DataBean> mlist) {
        this.mlist = mlist;
        this.mContext = mContext;

    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView product_image,iv_unlike;
        TextView product_name, product_type, product_price;


        public ViewHolder(View v) {
            super(v);
            product_image = (ImageView) v.findViewById(R.id.product_image);
            iv_unlike = (ImageView) v.findViewById(R.id.iv_unlike);
            product_name = (TextView) v.findViewById(R.id.product_name);
            product_type = (TextView) v.findViewById(R.id.product_type);
            product_price = (TextView) v.findViewById(R.id.product_price);

        }

    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_product_liked, parent, false);


        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        try {

            ViewHolder viewHolder = (ViewHolder) holder;
            Products.ProductsBean.DataBean item = mlist.get(position);


            if (item.getImage() != null && !item.getImage().isEmpty()) {
                Picasso.with(mContext).load((String) item.getImage()).resize(800, 300).centerCrop().placeholder(R.drawable.ic_place_holder).into(viewHolder.product_image);
            }

            viewHolder.product_name.setText("" + item.getDisplay_name());
            viewHolder.product_price.setText("" + item.getPrice());
            try {
                Products.ProductsBean.DataBean.CategoryIdBean categoryIdBean = new Gson().fromJson(item.getCat_Json(), Products.ProductsBean.DataBean.CategoryIdBean.class);
                System.out.println("getCat_Json " + item.getCat_Json());
                viewHolder.product_type.setText("" + categoryIdBean.getDisplay_title());

            } catch (Exception ee) {
                viewHolder.product_type.setText("" + mContext.getString(R.string.unknown));

            }

            viewHolder.iv_unlike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (listener != null) {
                        listener.onItemClick(v, position);
                    }
                }
            });
            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (listener != null) {
                        listener.onItemClick(v, position);
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return mlist.size();
    }


}



