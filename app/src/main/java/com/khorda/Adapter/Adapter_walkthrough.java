package com.khorda.Adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.khorda.CustomViews.CircleImageView;
import com.khorda.CustomViews.TextView;
import com.khorda.Models.WalkThrough;
import com.khorda.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by ahmed on 8/11/17.
 */

public class Adapter_walkthrough extends PagerAdapter {


    private LayoutInflater layoutInflater;
    private Context context;
    private List<WalkThrough.SplashBean> WalkThroughList;

    public Adapter_walkthrough(Context context, List<WalkThrough.SplashBean> WalkThroughList) {
        this.context = context;
        this.WalkThroughList = WalkThroughList;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.row_walkthrough_pager, container, false);
        CircleImageView slide_img = (CircleImageView) view.findViewById(R.id.slide_img);
        TextView slide_desc = (TextView) view.findViewById(R.id.slide_desc);
        Picasso.with(context).load(WalkThroughList.get(position).getImage()).resize(800, 300).centerCrop().placeholder(R.drawable.ic_place_holder).into(slide_img);
        slide_desc.setText(WalkThroughList.get(position).getDisplay_title());
       // slide_desc.setText(R.string.sample_txt);
        container.addView(view);

        return view;
    }

    @Override
    public int getCount() {
        return WalkThroughList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        container.removeView(view);
    }


}