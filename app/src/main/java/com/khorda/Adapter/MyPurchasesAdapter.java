package com.khorda.Adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.khorda.CustomViews.TextView;
import com.khorda.Models.MyPurchases;
import com.khorda.R;
import com.khorda.interfaces.OnItemClickListener;
import com.khorda.interfaces.OnLoadMoreListener;
import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.List;


public class MyPurchasesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<MyPurchases.PurchasesBean.DataBean> mlist;
    private Context mContext;
    private OnItemClickListener listener;
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;
    private int visibleThreshold = 2;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;


    public MyPurchasesAdapter(Context mContext, List<MyPurchases.PurchasesBean.DataBean> mlist, RecyclerView recyclerView) {
        this.mlist = mlist;
        this.mContext = mContext;


        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView,
                                       int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager
                            .findLastVisibleItemPosition();
                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });
        }
    }


    public class MyPurchasesHolder extends RecyclerView.ViewHolder {

        ImageView product_image;
        TextView product_name, product_type, product_price, product_status;


        public MyPurchasesHolder(View v) {
            super(v);
            product_image = (ImageView)v.findViewById(R.id.product_image);
            product_name = (TextView)v.findViewById(R.id.product_name);
            product_type = (TextView)v.findViewById(R.id.product_type);
            product_price = (TextView)v.findViewById(R.id.product_price);
            product_status = (TextView)v.findViewById(R.id.product_status);

        }

    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh = null;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.item_purchasese, parent, false);
            vh = new MyPurchasesHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.progressbar_item, parent, false);
            vh = new ProgressViewHolder(v);
        }

        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {




            if (holder instanceof MyPurchasesHolder) {
                try {

                MyPurchasesHolder myPurchasesHolder = (MyPurchasesHolder) holder;
                MyPurchases.PurchasesBean.DataBean item = mlist.get(position);

                    if(item.getProduct() != null) {
                        if (item.getProduct().getImage() != null && !item.getProduct().getImage().isEmpty()) {
                            Picasso.with(mContext).load((String) item.getProduct().getImage()).resize(800,300).centerCrop().placeholder(R.drawable.ic_place_holder).into(myPurchasesHolder.product_image);
                        }

                        myPurchasesHolder.product_name.setText("" + item.getProduct().getDisplay_name());
                        myPurchasesHolder.product_price.setText("" + item.getProduct().getPrice());
                        myPurchasesHolder.product_type.setText("" + item.getProduct().getCategory_id().getDisplay_title());

                    }else{
                        myPurchasesHolder.product_image.setImageResource(R.drawable.ic_place_holder);
                        myPurchasesHolder.product_name.setText(mContext.getString(R.string.unknown));
                        myPurchasesHolder.product_price.setText(mContext.getString(R.string.unknown));
                        myPurchasesHolder.product_type.setText(mContext.getString(R.string.unknown));

                    }

                    myPurchasesHolder.product_status.setText("" + item.getStatus());

                if (item.getStatus().equalsIgnoreCase("1")) {
                    myPurchasesHolder.product_status.setText(R.string.pending);
                    myPurchasesHolder.product_status.setBackground(mContext.getResources().getDrawable(R.drawable.bg_pending_status));

                } else {
                    myPurchasesHolder.product_status.setText(R.string.delivered);
                    myPurchasesHolder.product_status.setBackground(mContext.getResources().getDrawable(R.drawable.bg_delivered_status));
                }

                myPurchasesHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (listener != null) {
                            listener.onItemClick(v, position);
                        }
                    }
                });

        }catch (Exception e){
            e.printStackTrace();
        }

        } else {
            ProgressViewHolder Holder = (ProgressViewHolder) holder;
            Holder.progressBar.show();
        }
    }


    @Override
    public int getItemViewType(int position) {
        return mlist.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }


    @Override
    public int getItemCount() {
        return mlist.size();
    }


    public class ProgressViewHolder extends RecyclerView.ViewHolder {
        public AVLoadingIndicatorView progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (AVLoadingIndicatorView) v.findViewById(R.id.progressBar1);
        }
    }


    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public void setLoaded() {
        loading = false;
    }


    private void applyAndAnimateRemovals(List<MyPurchases.PurchasesBean.DataBean> newModels) {
        for (int i = newModels.size() - 1; i >= 0; i--) {
            final MyPurchases.PurchasesBean.DataBean model = newModels.get(i);
            if (!mlist.contains(model)) {
                removeItem(i);
            }
        }
    }

    private void applyAndAnimateAdditions(List<MyPurchases.PurchasesBean.DataBean> newModels) {
        for (int i = 0, count = newModels.size(); i < count; i++) {
            final MyPurchases.PurchasesBean.DataBean model = newModels.get(i);
            if (!mlist.contains(model)) {
                addItem(i, model);
            }
        }
    }

    private void applyAndAnimateMovedItems(List<MyPurchases.PurchasesBean.DataBean> newModels) {
        for (int toPosition = newModels.size() - 1; toPosition >= 0; toPosition--) {
            final MyPurchases.PurchasesBean.DataBean model = newModels.get(toPosition);
            final int fromPosition = mlist.indexOf(model);
            if (fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition);
            }
        }
    }

    public void animateTo(List<MyPurchases.PurchasesBean.DataBean> models) {
        applyAndAnimateRemovals(models);
        applyAndAnimateAdditions(models);
        applyAndAnimateMovedItems(models);
    }

    public MyPurchases.PurchasesBean.DataBean removeItem(int position) {
        final MyPurchases.PurchasesBean.DataBean model = mlist.remove(position);
        notifyItemRemoved(position);
        return model;
    }

    public void addItem(int position, MyPurchases.PurchasesBean.DataBean model) {
        mlist.add(position, model);
        notifyItemInserted(position);
    }

    public void moveItem(int fromPosition, int toPosition) {
        final MyPurchases.PurchasesBean.DataBean model = mlist.remove(fromPosition);
        mlist.add(toPosition, model);
        notifyItemMoved(fromPosition, toPosition);
    }


}



