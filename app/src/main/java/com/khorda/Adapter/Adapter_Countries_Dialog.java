package com.khorda.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.khorda.CustomViews.TextView;
import com.khorda.Models.Countries;
import com.khorda.Models.CountriesWithCities;
import com.khorda.R;
import com.khorda.interfaces.OnItemClickListener;

import java.util.List;

/**
 * Created by ez on 8/17/2017.
 */

public class Adapter_Countries_Dialog extends RecyclerView.Adapter<Adapter_Countries_Dialog.MyViewHolder> {

    private Context context;
    public List<CountriesWithCities.CountriesBean> DataList;
    private OnItemClickListener onItemClickListener;

    public Adapter_Countries_Dialog(Context context, List<CountriesWithCities.CountriesBean> DataList) {
        this.context = context;
        this.DataList = DataList;

    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.row_cities_list_layout, parent, false);


        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.countryTitle.setText(DataList.get(position).getDisplay_title());
        holder.countryTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(view, position);
                }
            }
        });



        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(view, position);
                }
            }
        });


    }


    @Override
    public int getItemCount() {
        return DataList.size();

    }


    class MyViewHolder extends RecyclerView.ViewHolder {


        private TextView countryTitle;

        public MyViewHolder(View itemView) {
            super(itemView);
            countryTitle = (TextView) itemView.findViewById(R.id.city_title);
        }
    }


 }