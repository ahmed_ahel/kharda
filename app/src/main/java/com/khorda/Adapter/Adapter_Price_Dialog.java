package com.khorda.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.khorda.CustomViews.TextView;
import com.khorda.Models.Categories;
import com.khorda.Models.Prices;
import com.khorda.R;
import com.khorda.interfaces.OnItemClickListener;

import java.util.List;

/**
 * Created by ez on 8/17/2017.
 */

public class Adapter_Price_Dialog extends RecyclerView.Adapter<Adapter_Price_Dialog.MyViewHolder> {

    private Context context;
    public List<Prices.PricesBean> DataList;
    private OnItemClickListener onItemClickListener;
    private int lastPosition = -1;

    public Adapter_Price_Dialog(Context context, List<Prices.PricesBean> DataList) {
        this.context = context;
        this.DataList = DataList;

    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.row_price_list_layout, parent, false);


        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.price_title.setText(DataList.get(position).getPriceFrom() + " - " + DataList.get(position).getPriceTo());
        holder.price_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(view, position);
                }
            }
        });


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(view, position);
                }
            }
        });


    }


    @Override
    public int getItemCount() {
        return DataList.size();

    }


    class MyViewHolder extends RecyclerView.ViewHolder {


        private TextView price_title;

        public MyViewHolder(View itemView) {
            super(itemView);
            price_title = (TextView) itemView.findViewById(R.id.price_title);
        }
    }



    private void applyAndAnimateRemovals(List<Prices.PricesBean> newModels) {
        for (int i = DataList.size() - 1; i >= 0; i--) {
            final Prices.PricesBean model = DataList.get(i);
            if (!DataList.contains(model)) {
                removeItem(i);
            }
        }
    }

    private void applyAndAnimateAdditions(List<Prices.PricesBean> newModels) {
        for (int i = 0, count = newModels.size(); i < count; i++) {
            final Prices.PricesBean model = newModels.get(i);
            if (!DataList.contains(model)) {
                addItem(i, model);
            }
        }
    }

    private void applyAndAnimateMovedItems(List<Prices.PricesBean> newModels) {
        for (int toPosition = newModels.size() - 1; toPosition >= 0; toPosition--) {
            final Prices.PricesBean model = newModels.get(toPosition);
            final int fromPosition = DataList.indexOf(model);
            if (fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition);
            }
        }
    }

    public void animateTo(List<Prices.PricesBean> models) {
        applyAndAnimateRemovals(models);
        applyAndAnimateAdditions(models);
        applyAndAnimateMovedItems(models);
    }

    public Prices.PricesBean removeItem(int position) {
        final Prices.PricesBean model = DataList.remove(position);
        notifyItemRemoved(position);
        return model;
    }

    public void addItem(int position, Prices.PricesBean model) {
        DataList.add(position, model);
        notifyItemInserted(position);
    }

    public void moveItem(int fromPosition, int toPosition) {
        final Prices.PricesBean model = DataList.remove(fromPosition);
        DataList.add(toPosition, model);
        notifyItemMoved(fromPosition, toPosition);
    }
}