package com.khorda.Adapter;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.khorda.CustomViews.TextView;
import com.khorda.Models.Products;
import com.khorda.R;
import com.khorda.Utils.ToolsUtils;
import com.khorda.interfaces.OnItemClickListener;
import com.khorda.interfaces.OnLoadMoreListener;
import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.List;


/**
 * Created by AHMED AHEL on 2/18/2016.
 */
public class Adapter_products extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Products.ProductsBean.DataBean> mlist;
    private List<Products.AdsBean> adsBeanList;
    private Context mContext;
    private OnItemClickListener listener;
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;
    private final int VIEW_AD = 2;
    private int visibleThreshold = 2;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;


    public Adapter_products(Context mContext, List<Products.ProductsBean.DataBean> mlist, List<Products.AdsBean> adsBeanList, RecyclerView recyclerView) {
        this.mlist = mlist;
        this.mContext = mContext;
        this.adsBeanList = adsBeanList;
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView,
                                       int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });
        }
    }


    public class ProductsHolder extends RecyclerView.ViewHolder {

        private AppCompatImageView product_img, like_product_image, iv_ads;
        private TextView product_name, product_category, product_color, product_price, product_likes, iv_add_to_cart, like_product;
        private LinearLayout ll_add_to_like;

        public ProductsHolder(View v) {
            super(v);
            ll_add_to_like = (LinearLayout) v.findViewById(R.id.ll_add_to_like);
            like_product_image = (AppCompatImageView) v.findViewById(R.id.like_product_image);
            iv_ads = (AppCompatImageView) v.findViewById(R.id.iv_ads);
            product_img = (AppCompatImageView) v.findViewById(R.id.product_img);
            like_product = (TextView) v.findViewById(R.id.like_product);
            product_name = (TextView) v.findViewById(R.id.product_name);
            product_category = (TextView) v.findViewById(R.id.product_category);
            product_color = (TextView) v.findViewById(R.id.product_color);
            product_price = (TextView) v.findViewById(R.id.product_price);
            product_likes = (TextView) v.findViewById(R.id.product_likes);
            iv_add_to_cart = (TextView) v.findViewById(R.id.iv_add_to_cart);


        }

    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh = null;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.row_product_layout, parent, false);
            vh = new ProductsHolder(v);
        } else if (viewType == VIEW_PROG) {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.progressbar_item, parent, false);
            vh = new ProgressViewHolder(v);
        } else if (viewType == VIEW_AD) {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.row_ad_view, parent, false);
            vh = new AdViewHolder(v);
        }

        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof ProductsHolder) {
            ProductsHolder productsHolder = (ProductsHolder) holder;
            Products.ProductsBean.DataBean item = mlist.get(position);
            try {

                System.out.println("getImage " + item.getImage());
                if (item.getImage() != null && !item.getImage().isEmpty()) {
                    Picasso.with(mContext).load((String) item.getImage()).resize(800, 300).centerCrop().placeholder(R.drawable.ic_place_holder).into(productsHolder.product_img);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            productsHolder.product_name.setText("" + item.getDisplay_name());


            try {
                productsHolder.product_color.setText("" + item.getColors().getDisplay_title());
            } catch (Exception e) {
                productsHolder.product_color.setText(mContext.getString(R.string.unknown));
            }

            try {
                productsHolder.product_category.setText("" + item.getCategory_id().getDisplay_title());
            } catch (Exception e) {
                productsHolder.product_category.setText(mContext.getString(R.string.unknown));
            }


            try {
                productsHolder.product_price.setText("" + item.getPrice());
            } catch (Exception e) {
                productsHolder.product_price.setText(mContext.getString(R.string.unknown));
            }


            try {
                productsHolder.product_likes.setText("" + item.getLikes());
            } catch (Exception e) {
                productsHolder.product_likes.setText(mContext.getString(R.string.unknown));
            }

            if (ToolsUtils.checkProductLiked(item)) {
                productsHolder.like_product_image.setImageResource(R.drawable.ic_liked);
                productsHolder.like_product.setText(mContext.getString(R.string.remove_from_like));

            } else {
                productsHolder.like_product_image.setImageResource(R.drawable.ic_unliked);
                productsHolder.like_product.setText(mContext.getString(R.string.add_to_like));

            }

            productsHolder.product_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onItemClick(v, position);
                    }
                }
            });

            productsHolder.product_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onItemClick(v, position);
                    }
                }
            });

            productsHolder.product_color.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onItemClick(v, position);
                    }
                }
            });

            productsHolder.product_category.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onItemClick(v, position);
                    }
                }
            });


            productsHolder.product_price.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onItemClick(v, position);
                    }
                }
            });


            productsHolder.iv_add_to_cart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onItemClick(v, position);
                    }
                }
            });

            productsHolder.ll_add_to_like.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onItemClick(v, position);
                    }
                }
            });


        } else if (holder instanceof AdViewHolder) {

            AdViewHolder Holder = (AdViewHolder) holder;

            int index = (position + 1) / 5;

            if (adsBeanList.get(index).getImage() != null && !adsBeanList.get(index).getImage().isEmpty()) {
                Picasso.with(mContext).load((String) adsBeanList.get(index).getImage()).placeholder(R.drawable.ic_place_holder).into(Holder.iv_ad);
            }

        } else if (holder instanceof ProgressViewHolder) {

            ProgressViewHolder Holder = (ProgressViewHolder) holder;
            Holder.progressBar.show();
        }
    }


    @Override
    public int getItemViewType(int position) {

        int VIEW = VIEW_ITEM;

        if (mlist.get(position) == null) {
            VIEW = VIEW_PROG;
        } else {

            int index = (position + 1) / 5;

            if (index < adsBeanList.size()) {
                if ((position + 1) % 5 == 0) {
                    VIEW = VIEW_AD;
                } else {
                    VIEW = VIEW_ITEM;

                }
            }

        }


        return VIEW;

        //   return (mlist.get(position) != null) ? VIEW_ITEM: VIEW_PROG;
    }


    @Override
    public int getItemCount() {
        return mlist.size();
    }


    public class ProgressViewHolder extends RecyclerView.ViewHolder {
        public AVLoadingIndicatorView progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (AVLoadingIndicatorView) v.findViewById(R.id.progressBar1);
        }
    }


    public class AdViewHolder extends RecyclerView.ViewHolder {
        public AppCompatImageView iv_ad;

        public AdViewHolder(View v) {
            super(v);
            iv_ad = (AppCompatImageView) v.findViewById(R.id.iv_ads);
        }
    }


    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public void setLoaded() {
        loading = false;
    }


    private void applyAndAnimateRemovals(List<Products.ProductsBean.DataBean> newModels) {
        for (int i = newModels.size() - 1; i >= 0; i--) {
            final Products.ProductsBean.DataBean model = newModels.get(i);
            if (!mlist.contains(model)) {
                removeItem(i);
            }
        }
    }

    private void applyAndAnimateAdditions(List<Products.ProductsBean.DataBean> newModels) {
        for (int i = 0, count = newModels.size(); i < count; i++) {
            final Products.ProductsBean.DataBean model = newModels.get(i);
            if (!mlist.contains(model)) {
                addItem(i, model);
            }
        }
    }

    private void applyAndAnimateMovedItems(List<Products.ProductsBean.DataBean> newModels) {
        for (int toPosition = newModels.size() - 1; toPosition >= 0; toPosition--) {
            final Products.ProductsBean.DataBean model = newModels.get(toPosition);
            final int fromPosition = mlist.indexOf(model);
            if (fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition);
            }
        }
    }

    public void animateTo(List<Products.ProductsBean.DataBean> models) {
        applyAndAnimateRemovals(models);
        applyAndAnimateAdditions(models);
        applyAndAnimateMovedItems(models);
    }

    public Products.ProductsBean.DataBean removeItem(int position) {
        final Products.ProductsBean.DataBean model = mlist.remove(position);
        notifyItemRemoved(position);
        return model;
    }

    public void addItem(int position, Products.ProductsBean.DataBean model) {
        mlist.add(position, model);
        notifyItemInserted(position);
    }

    public void moveItem(int fromPosition, int toPosition) {
        final Products.ProductsBean.DataBean model = mlist.remove(fromPosition);
        mlist.add(toPosition, model);
        notifyItemMoved(fromPosition, toPosition);
    }


}
