package com.khorda.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.khorda.CustomViews.TextView;
import com.khorda.Models.Countries;
import com.khorda.R;
import com.khorda.interfaces.OnItemClickListener;

import java.util.List;

/**
 * Created by ez on 8/17/2017.
 */

public class Adapter_Cities_Dialog extends RecyclerView.Adapter<Adapter_Cities_Dialog.MyViewHolder> {

    private Context context;
    public List<Countries.CountriesBean> DataList;
    private OnItemClickListener onItemClickListener;
    private int lastPosition = -1;

    public Adapter_Cities_Dialog(Context context, List<Countries.CountriesBean> DataList) {
        this.context = context;
        this.DataList = DataList;

    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.row_cities_list_layout, parent, false);


        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.cityTitle.setText(DataList.get(position).getDisplay_title());
        holder.cityTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(view, position);
                }
            }
        });



        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(view, position);
                }
            }
        });


    }


    @Override
    public int getItemCount() {
        return DataList.size();

    }


    class MyViewHolder extends RecyclerView.ViewHolder {


        private TextView cityTitle;

        public MyViewHolder(View itemView) {
            super(itemView);
            cityTitle = (TextView) itemView.findViewById(R.id.city_title);
        }
    }



    private void applyAndAnimateRemovals(List<Countries.CountriesBean> newModels) {
        for (int i = DataList.size() - 1; i >= 0; i--) {
            final Countries.CountriesBean model = DataList.get(i);
            if (!DataList.contains(model)) {
                removeItem(i);
            }
        }
    }

    private void applyAndAnimateAdditions(List<Countries.CountriesBean> newModels) {
        for (int i = 0, count = newModels.size(); i < count; i++) {
            final Countries.CountriesBean model = newModels.get(i);
            if (!DataList.contains(model)) {
                addItem(i, model);
            }
        }
    }

    private void applyAndAnimateMovedItems(List<Countries.CountriesBean> newModels) {
        for (int toPosition = newModels.size() - 1; toPosition >= 0; toPosition--) {
            final Countries.CountriesBean model = newModels.get(toPosition);
            final int fromPosition = DataList.indexOf(model);
            if (fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition);
            }
        }
    }

    public void animateTo(List<Countries.CountriesBean> models) {
        applyAndAnimateRemovals(models);
        applyAndAnimateAdditions(models);
        applyAndAnimateMovedItems(models);
    }

    public Countries.CountriesBean removeItem(int position) {
        final Countries.CountriesBean model = DataList.remove(position);
        notifyItemRemoved(position);
        return model;
    }

    public void addItem(int position, Countries.CountriesBean model) {
        DataList.add(position, model);
        notifyItemInserted(position);
    }

    public void moveItem(int fromPosition, int toPosition) {
        final Countries.CountriesBean model = DataList.remove(fromPosition);
        DataList.add(toPosition, model);
        notifyItemMoved(fromPosition, toPosition);
    }
}