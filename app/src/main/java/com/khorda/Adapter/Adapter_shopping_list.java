package com.khorda.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.google.gson.Gson;
import com.khorda.CustomViews.TextView;
import com.khorda.Models.Products;
import com.khorda.R;
import com.khorda.interfaces.OnItemClickListener;
import com.squareup.picasso.Picasso;

import java.util.List;


/**
 * Created by AHMED AHEL on 2/18/2016.
 */
public class Adapter_shopping_list extends RecyclerSwipeAdapter<Adapter_shopping_list.ShoppingItemHolder> {

    private List<Products.ProductsBean.DataBean> mlist;
    private Context mContext;
    private OnItemClickListener listener;

    public Adapter_shopping_list(Context mContext, List<Products.ProductsBean.DataBean> mlist) {
        this.mlist = mlist;
        this.mContext = mContext;
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }


    public class ShoppingItemHolder extends RecyclerView.ViewHolder {

        private ImageView trash;
        private ImageView productImage;
        private TextView productName;
        private TextView productType;
        private TextView productPrice;
        private ImageView increaseProduct;
        private TextView productQuantity;
        private ImageView decreaseProduct;

        public ShoppingItemHolder(View view) {
            super(view);

            trash = (ImageView) view.findViewById(R.id.trash);
            productImage = (ImageView) view.findViewById(R.id.product_image);
            productName = (TextView) view.findViewById(R.id.product_name);
            productType = (TextView) view.findViewById(R.id.product_type);
            productPrice = (TextView) view.findViewById(R.id.product_price);
            increaseProduct = (ImageView) view.findViewById(R.id.increase_product);
            productQuantity = (TextView) view.findViewById(R.id.product_quantity);
            decreaseProduct = (ImageView) view.findViewById(R.id.decrease_product);

        }

    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }


    @Override
    public ShoppingItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.row_shopping_list_item, parent, false);
        return new ShoppingItemHolder(v);
    }

    @Override
    public void onBindViewHolder(ShoppingItemHolder viewHolder, final int position) {

        try {

            Products.ProductsBean.DataBean item = mlist.get(position);

            System.out.println("image" + item.getImage());
            if (item.getImage() != null && !item.getImage().isEmpty()) {
                Picasso.with(mContext).load((String) item.getImage()).resize(800, 300).centerCrop().placeholder(R.drawable.ic_place_holder).into(viewHolder.productImage);
            }


            viewHolder.productName.setText("" + item.getName());
            viewHolder.productPrice.setText("" + item.getPrice());

            try {
                Products.ProductsBean.DataBean.CategoryIdBean categoryIdBean = new Gson().fromJson(item.getCat_Json(), Products.ProductsBean.DataBean.CategoryIdBean.class);

                System.out.println("getCat_Json " + item.getCat_Json());
                viewHolder.productType.setText("" + categoryIdBean.getDisplay_title());

            } catch (Exception ee) {
                viewHolder.productType.setText("" + mContext.getString(R.string.unknown));

            }
            viewHolder.productQuantity.setText("" + item.getSelectedQuantity());
            viewHolder.decreaseProduct.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onItemClick(view, position);
                    }
                }
            });

            viewHolder.increaseProduct.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onItemClick(view, position);
                    }
                }
            });


            viewHolder.trash.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onItemClick(view, position);
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mlist.size();
    }


    private void applyAndAnimateRemovals(List<Products.ProductsBean.DataBean> newModels) {
        for (int i = newModels.size() - 1; i >= 0; i--) {
            final Products.ProductsBean.DataBean model = newModels.get(i);
            if (!mlist.contains(model)) {
                removeItem(i);
            }
        }
    }

    private void applyAndAnimateAdditions(List<Products.ProductsBean.DataBean> newModels) {
        for (int i = 0, count = newModels.size(); i < count; i++) {
            final Products.ProductsBean.DataBean model = newModels.get(i);
            if (!mlist.contains(model)) {
                addItem(i, model);
            }
        }
    }

    private void applyAndAnimateMovedItems(List<Products.ProductsBean.DataBean> newModels) {
        for (int toPosition = newModels.size() - 1; toPosition >= 0; toPosition--) {
            final Products.ProductsBean.DataBean model = newModels.get(toPosition);
            final int fromPosition = mlist.indexOf(model);
            if (fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition);
            }
        }
    }

    public void animateTo(List<Products.ProductsBean.DataBean> models) {
        applyAndAnimateRemovals(models);
        applyAndAnimateAdditions(models);
        applyAndAnimateMovedItems(models);
    }

    public Products.ProductsBean.DataBean removeItem(int position) {
        final Products.ProductsBean.DataBean model = mlist.remove(position);
        notifyItemRemoved(position);
        return model;
    }

    public void addItem(int position, Products.ProductsBean.DataBean model) {
        mlist.add(position, model);
        notifyItemInserted(position);
    }

    public void moveItem(int fromPosition, int toPosition) {
        final Products.ProductsBean.DataBean model = mlist.remove(fromPosition);
        mlist.add(toPosition, model);
        notifyItemMoved(fromPosition, toPosition);
    }


}
