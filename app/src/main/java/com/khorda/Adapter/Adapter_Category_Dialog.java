package com.khorda.Adapter;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.khorda.CustomViews.TextView;
import com.khorda.Models.Categories;
import com.khorda.Models.Countries;
import com.khorda.R;
import com.khorda.interfaces.OnItemClickListener;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by ez on 8/17/2017.
 */

public class Adapter_Category_Dialog extends RecyclerView.Adapter<Adapter_Category_Dialog.MyViewHolder> {

    private Context context;
    public List<Categories.CategoriesBean> DataList;
    private OnItemClickListener onItemClickListener;

    public Adapter_Category_Dialog(Context context, List<Categories.CategoriesBean> DataList) {
        this.context = context;
        this.DataList = DataList;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.row_category_list_layout, parent, false);

        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.category_title.setText(DataList.get(position).getDisplay_title());
        if (DataList.get(position).getImage() != null && !DataList.get(position).getImage().isEmpty()) {
            Picasso.with(context).load((String) DataList.get(position).getImage()).placeholder(R.drawable.ic_place_holder).into(holder.category_img);
        }
        holder.category_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(view, position);
                }
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(view, position);
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return DataList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {


        private TextView category_title;
        private AppCompatImageView category_img;

        public MyViewHolder(View itemView) {
            super(itemView);
            category_title = (TextView) itemView.findViewById(R.id.category_title);
            category_img = (AppCompatImageView) itemView.findViewById(R.id.category_img);
        }
    }

}