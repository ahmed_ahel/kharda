package com.khorda.Adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.AppCompatImageView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.khorda.R;
import com.khorda.interfaces.OnItemClickListener;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by macbook on 9/15/17.
 */

public class Adapter_Product_Images extends PagerAdapter {


    private LayoutInflater layoutInflater;
    private Context context;
    private List<String> ImagesList;
    private OnItemClickListener listener;

    public Adapter_Product_Images(Context context, List<String> ImagesList) {
        this.context = context;
        this.ImagesList = ImagesList;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.row_product_img_pager, container, false);

        AppCompatImageView product_img = (AppCompatImageView) view.findViewById(R.id.product_img);

        if (ImagesList.get(position) != null && !ImagesList.get(position).isEmpty()) {
            Picasso.with(context).load((String) ImagesList.get(position)).placeholder(R.drawable.ic_place_holder).into(product_img);
        } else {
            Picasso.with(context).load(R.drawable.ic_place_holder).into(product_img);
        }
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onItemClick(view, position);
                }
            }
        });


        container.addView(view);

        return view;
    }


    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return ImagesList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        container.removeView(view);
    }


}