package com.khorda;

import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.khorda.Adapter.Adapter_Cities_Dialog;
import com.khorda.Adapter.Adapter_Cities_Dialog2;
import com.khorda.Models.Countries;
import com.khorda.Models.CountriesWithCities;
import com.khorda.interfaces.OnItemClickListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ez on 8/17/2017.
 */

public class CitiesListDialog2 {
    private SelectedCity citySelect;
    private Dialog dialog;
    private AppCompatActivity activity;
    private Adapter_Cities_Dialog2 adapterFilter;
    private List<CountriesWithCities.CountriesBean.CitiesBean> filterList = new ArrayList<>();
    private RecyclerView recyclerViewError;

    public CitiesListDialog2(final AppCompatActivity activity) {
        this.activity = activity;
        dialog = new Dialog(activity);

    }

    public void setCites(List<CountriesWithCities.CountriesBean.CitiesBean> cites) {
       // filterList.clear();
        this.filterList.addAll(cites);
    }

    public void setSelectCity(SelectedCity citySelect) {
        this.citySelect = citySelect;
    }


    public void show() {


        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.select_city_dialog_layout);

        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);


        recyclerViewError = (RecyclerView) dialog.findViewById(R.id.cities_list);
        recyclerViewError.setLayoutManager(new LinearLayoutManager(activity));
        adapterFilter = new Adapter_Cities_Dialog2(activity, filterList);
        recyclerViewError.setAdapter(adapterFilter);

        adapterFilter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int position) {
                try {
                    citySelect.getSelectedCity(adapterFilter.DataList.get(position));
                    dialog.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

         dialog.show();



    }

    public interface SelectedCity {
        public void getSelectedCity(CountriesWithCities.CountriesBean.CitiesBean SelectedCity);
    }
}