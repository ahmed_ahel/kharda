package com.khorda;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.khorda.CustomViews.AVLoadingIndicatorDialog;
import com.khorda.CustomViews.Button;
import com.khorda.CustomViews.EditText;
import com.khorda.CustomViews.TextView;
import com.khorda.Utils.MyContextWrapper;
import com.khorda.Utils.PreferenceEditor;
import com.khorda.Utils.QuickstartPreferences;
import com.khorda.interfaces.ApiClient;
import com.khorda.interfaces.ApiInterface;
import com.khorda.Models.Countries;
import com.khorda.Models.UserInfo;
import com.khorda.Utils.ToolsUtils;

import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private Button signup_btn;
    private EditText fullName,
            email,
            mobile,
            password;
    private TextView city, tv_terms_conditions;
    private int CityId = -1;
    private SwipeRefreshLayout swipe;
    private Countries cities;


    @Override
    protected void attachBaseContext(Context newBase) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String lang = PreferenceEditor.getInstance().getStringPreference(QuickstartPreferences.CURRENT_LANGUAGE);
            super.attachBaseContext(MyContextWrapper.wrap(newBase, lang));
        }else {
            super.attachBaseContext(newBase);
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        tv_terms_conditions = (TextView) findViewById(R.id.tv_terms_conditions);
        city = (TextView) findViewById(R.id.city);
        fullName = (EditText) findViewById(R.id.fullName);
        email = (EditText) findViewById(R.id.email);
        mobile = (EditText) findViewById(R.id.mobile);
        password = (EditText) findViewById(R.id.password);
        signup_btn = (Button) findViewById(R.id.signup_btn);
        swipe = (SwipeRefreshLayout) findViewById(R.id.swipe);
        swipe.setOnRefreshListener(this);
        tv_terms_conditions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(getApplicationContext(), TermAndConditionActivity.class));

            }
        });
        if (getIntent().getExtras() != null) {
            fullName.setText((String) getIntent().getExtras().get("fullName"));
            email.setText((String) getIntent().getExtras().get("email"));
        }


        changeStatusBarColor();


        city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (cities != null) {
                    CitiesListDialog citiesListDialog = new CitiesListDialog(SignUpActivity.this);
                    citiesListDialog.setCountries(cities);
                    citiesListDialog.setSelectCity(new CitiesListDialog.SelectedCity() {
                        @Override
                        public void getSelectedCity(Countries.CountriesBean SelectedCity) {
                            city.setText(SelectedCity.getDisplay_title());
                            CityId = SelectedCity.getId();
                            System.out.println("Display_title" + SelectedCity.getDisplay_title());
                            System.out.println("Display id" + CityId);


                        }
                    });
                    citiesListDialog.show();

                } else {

                    Toast.makeText(getApplicationContext(), R.string.swipe_to_refresh, Toast.LENGTH_LONG).show();

                }


            }


        });


        signup_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (fullName.getText().toString().isEmpty()) {
                    Toast.makeText(getApplicationContext(), R.string.err_msg_full_name, Toast.LENGTH_SHORT).show();
                    return;
                }


                if (email.getText().toString().isEmpty()) {
                    Toast.makeText(getApplicationContext(), R.string.err_msg_email, Toast.LENGTH_SHORT).show();
                    return;
                }

                if (!ToolsUtils.isValidEmail(email.getText().toString())) {
                    Toast.makeText(getApplicationContext(), R.string.err_msg_valid_email, Toast.LENGTH_SHORT).show();
                    return;
                }

                if (city.getText().toString().isEmpty()) {
                    Toast.makeText(getApplicationContext(), R.string.err_msg_city, Toast.LENGTH_SHORT).show();
                    return;
                }

                if (mobile.getText().toString().isEmpty()) {
                    Toast.makeText(getApplicationContext(), R.string.err_msg_mobile, Toast.LENGTH_SHORT).show();
                    return;
                }

                if (mobile.getText().toString().length() < 11) {
                    Toast.makeText(getApplicationContext(), R.string.err_msg_mobile_length, Toast.LENGTH_SHORT).show();
                    return;
                }


                if (password.getText().toString().isEmpty()) {
                    Toast.makeText(getApplicationContext(), R.string.err_msg_password, Toast.LENGTH_SHORT).show();
                    return;
                }

                RequestSignUp();


            }
        });

        requestGetCities();
    }

    private void requestGetCities() {
        if (ToolsUtils.isNetworkAvailable()) {

            swipe.setRefreshing(true);

            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<Countries> call = apiService.getCountries();

            call.enqueue(new Callback<Countries>() {
                @Override
                public void onResponse(Call<Countries> call, Response<Countries> response) {

                    System.out.println("Code" + response.code());
                    System.out.println("raw" + response.raw());
                    System.out.println("Code" + response.message());
                    swipe.setRefreshing(false);

                    if (response.code() == 200 && response.body().isStatus()) {

                        cities = response.body();
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<Countries> call, Throwable t) {
                    swipe.setRefreshing(false);
                    Toast.makeText(getApplicationContext(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            swipe.setRefreshing(false);
            Toast.makeText(getApplicationContext(), R.string.err_msg_no_internet_connection, Toast.LENGTH_SHORT).show();
        }
    }
    private void RequestSignUp() {

        if (ToolsUtils.isNetworkAvailable()) {

            final AVLoadingIndicatorDialog dialog = ToolsUtils.CreateDialog(this, null);
            dialog.show();
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            RequestBody full_name = RequestBody.create(MediaType.parse("text/plain"), fullName.getText().toString().trim());
            RequestBody emailAddress = RequestBody.create(MediaType.parse("text/plain"), email.getText().toString().trim());
            RequestBody mobileNum = RequestBody.create(MediaType.parse("text/plain"), mobile.getText().toString().trim());
            RequestBody pass = RequestBody.create(MediaType.parse("text/plain"), password.getText().toString().trim());
            RequestBody fcm = RequestBody.create(MediaType.parse("text/plain"), FirebaseInstanceId.getInstance().getToken());
            RequestBody city_id = RequestBody.create(MediaType.parse("text/plain"), "" + CityId);


            Map<String, RequestBody> map = new HashMap<>();
            map.put("fullname", full_name);
            map.put("email", emailAddress);
            map.put("mobile", mobileNum);
            map.put("password", pass);
            map.put("FCM_token", fcm);
            map.put("country_id", city_id);

            System.out.println("request" + map.toString());

            Call<UserInfo> call = apiService.signup(map);
            call.enqueue(new retrofit2.Callback<UserInfo>() {
                @Override
                public void onResponse(Call<UserInfo> call, Response<UserInfo> response) {

                    System.out.println("Code" + response.code());
                    System.out.println("raw" + response.raw());
                    System.out.println("Code" + response.message());
                    dialog.hide();

                    if (response.code() == 200) {


                        if (response.body().isStatus()) {
                            //   Toast.makeText(SignUpActivity.this, "Sucess", Toast.LENGTH_SHORT).show();

                            Intent i = new Intent(getApplicationContext(), VerificationActivity.class);
                            i.putExtra("mobile", mobile.getText().toString().trim());
                            startActivity(i);


                        } else {
                            Toast.makeText(SignUpActivity.this, response.body().getDisplay_message(), Toast.LENGTH_SHORT).show();
                            Log.d("tag", "res" + response.body().getDisplay_message());

                        }
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();

                    }

                }

                @Override
                public void onFailure(Call<UserInfo> call, Throwable t) {
                    t.printStackTrace();
                    dialog.hide();
                    System.out.println("onFailure");
                    Toast.makeText(getApplicationContext(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();

                }
            });

        } else {
            Toast.makeText(getApplicationContext(), R.string.err_msg_no_internet_connection, Toast.LENGTH_SHORT).show();
        }
    }
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.WHITE);
        }
    }
    public void BackClicked(View v) {
        finish();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
    @Override
    public void onRefresh() {
        CityId = -1 ;
        city.setText("");
        requestGetCities();
    }
}
