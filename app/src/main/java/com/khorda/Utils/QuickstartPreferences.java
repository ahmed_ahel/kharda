package com.khorda.Utils;

/**
 * Created by AHMED AHEL on 27-Apr-16.
 */
public class QuickstartPreferences {

    public static final String  IS_NOT_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";
    public static final String CURRENT_LANGUAGE = "en";
    public static final String AR_LAN = "ar";
    public static final String EN_LAN = "en";
    public static final String USER_INFO = "user_info";
    public static final String ITEM_CHANGED_FILTER = "item_changed";
}
