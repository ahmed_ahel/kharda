package com.khorda.Utils;

import android.content.SharedPreferences;

import com.khorda.MyApplication;


public class PreferenceEditor {

    private static PreferenceEditor instance;
    private static SharedPreferences mSharedPreferences;

    private PreferenceEditor(){
        mSharedPreferences = MyApplication.getInstance().getSharedPreferences("MyPref", 0);
    }

    public static PreferenceEditor getInstance(){
        if(instance == null){
            instance = new PreferenceEditor();
        }
        return instance;
    }



    public void setStringPreference(String preference, String value){
        mSharedPreferences.edit().putString(preference, value).commit();
    }

    public String getStringPreference(String preference) {
        return mSharedPreferences.getString(preference, null);
    }

    public void setBooleanPreference(String preference, boolean value){
        mSharedPreferences.edit().putBoolean(preference, value).commit();
    }

    public boolean getBooleanPreference(String preference) {
        return mSharedPreferences.getBoolean(preference, false);
    }

    public void setIntegerPreference(String preference, int value){
        mSharedPreferences.edit().putInt(preference, value).commit();
    }

    public int getIntegerPreference(String preference) {
        return mSharedPreferences.getInt(preference, 0);
    }



    public void removePreference(String preference) {
        SharedPreferences.Editor e = mSharedPreferences.edit();
        e.remove(preference);
        e.commit();
    }




}
