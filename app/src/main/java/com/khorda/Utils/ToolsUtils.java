package com.khorda.Utils;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.khorda.CustomViews.AVLoadingIndicatorDialog;
import com.khorda.Models.Products;
import com.khorda.MyApplication;
import com.khorda.NoInternetConnectionActivity;
import com.khorda.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by ahmed on 8/11/17.
 */

public class ToolsUtils {


    public static void setupWebView(WebView webView) {
        WebSettings settings = webView.getSettings();
        // Enable Javascript
        settings.setJavaScriptEnabled(true);
        // Use WideViewport and Zoom out if there is no viewport defined
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);

        // Enable pinch to zoom without the zoom buttons
        settings.setBuiltInZoomControls(true);

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.HONEYCOMB) {
            // Hide the zoom controls for HONEYCOMB+
            settings.setDisplayZoomControls(false);
        }

        // Enable remote debugging via chrome://inspect
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true);
        }


    }

    public static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean isNetworkAvailable() {
        try {
            ConnectivityManager connectivityManager
                    = (ConnectivityManager) MyApplication.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }


    }

    public static void changeAppLanguage(String localeCode) {
        PreferenceEditor.getInstance().setStringPreference(QuickstartPreferences.CURRENT_LANGUAGE, localeCode);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {

            Resources res = MyApplication.getInstance().getResources();
            // Change locale settings in the app.
            DisplayMetrics dm = res.getDisplayMetrics();
            android.content.res.Configuration conf = res.getConfiguration();
            conf.setLocale(new Locale(localeCode.toLowerCase())); // API 17+ only.
            // Use conf.locale = new Locale(...) if targeting lower versions
            res.updateConfiguration(conf, dm);
        }

    }

    public static AVLoadingIndicatorDialog CreateDialog(AppCompatActivity activity, String msg) {
        if (msg == null || msg.isEmpty()) {
            msg = activity.getResources().getString(R.string.wait_mexssage);
        }
        AVLoadingIndicatorDialog waitDialog = new AVLoadingIndicatorDialog(activity);
        waitDialog.setMessage(msg);
        waitDialog.setCancelable(false);
        return waitDialog;

    }


    public static void ShowNoInternetConnection(Context mContext) {
        mContext.startActivity(new Intent(mContext, NoInternetConnectionActivity.class));
    }


    public static String getDeviceLang() {
        String lang = PreferenceEditor.getInstance().getStringPreference(QuickstartPreferences.CURRENT_LANGUAGE);
        if (lang == null) {
            PreferenceEditor.getInstance().setStringPreference(QuickstartPreferences.CURRENT_LANGUAGE, QuickstartPreferences.EN_LAN);
            return QuickstartPreferences.EN_LAN;
        }

        return lang;
    }


    public static String setDefualtLang() {
        String lang = Locale.getDefault().getLanguage();
        System.out.println("lang" + lang);
        if (lang.equalsIgnoreCase("ar")) {
            PreferenceEditor.getInstance().setStringPreference(QuickstartPreferences.CURRENT_LANGUAGE, QuickstartPreferences.AR_LAN);
            return QuickstartPreferences.AR_LAN;
        }

        PreferenceEditor.getInstance().setStringPreference(QuickstartPreferences.CURRENT_LANGUAGE, QuickstartPreferences.EN_LAN);
        return QuickstartPreferences.EN_LAN;

    }

    public static String RemoveHtmlTags(String html) {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result.toString().trim();
    }


    public static boolean checkProductInCart(Products.ProductsBean.DataBean product) {

        List<Products.ProductsBean.DataBean> list = Products.ProductsBean.DataBean.listAll(Products.ProductsBean.DataBean.class);

        if (list == null) {
            list = new ArrayList<>();
        }

        for (int i = 0; i < list.size(); i++) {
            Products.ProductsBean.DataBean item = list.get(i);
            if (item.getProduct_id().equalsIgnoreCase(product.getProduct_id())) {
                if (item.isInCart()) {
                    return true;
                }
            }
        }
        return false;

    }


    public static boolean checkProductLiked(Products.ProductsBean.DataBean product) {

        List<Products.ProductsBean.DataBean> list = Products.ProductsBean.DataBean.listAll(Products.ProductsBean.DataBean.class);

        if (list == null) {
            list = new ArrayList<>();
        }

        for (int i = 0; i < list.size(); i++) {
            Products.ProductsBean.DataBean item = list.get(i);
            if (item.getProduct_id().equalsIgnoreCase(product.getProduct_id())) {
                if (item.getIs_liked().equals("1")) {
                    return true;
                }

            }
        }
        return false;

    }


    public static List<Products.ProductsBean.DataBean> getAllCartProducts() {

        List<Products.ProductsBean.DataBean> list = Products.ProductsBean.DataBean.listAll(Products.ProductsBean.DataBean.class);

        if (list == null) {
            list = new ArrayList<>();
        }

        List<Products.ProductsBean.DataBean> CartItems =  new ArrayList<>();
        for (Products.ProductsBean.DataBean item :list) {

            if(item.isInCart()) {
                CartItems.add(item);
            }
        }

        return CartItems;
    }



    public static List<Products.ProductsBean.DataBean> getAllLikedProducts() {

        List<Products.ProductsBean.DataBean> list = Products.ProductsBean.DataBean.listAll(Products.ProductsBean.DataBean.class);

        if (list == null) {
            list = new ArrayList<>();
        }

        List<Products.ProductsBean.DataBean> LikedItems =  new ArrayList<>();
        for (Products.ProductsBean.DataBean item :list) {
            if(item.getIs_liked().equals("1")) {
                LikedItems.add(item);
            }
        }

        System.out.println("LikedItems"+LikedItems.size());
        return LikedItems;
    }

}
