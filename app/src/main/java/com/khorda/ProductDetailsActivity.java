package com.khorda;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.khorda.Adapter.Adapter_Product_Images;
import com.khorda.CustomViews.AVLoadingIndicatorDialog;
import com.khorda.CustomViews.Button;
import com.khorda.CustomViews.TextView;
import com.khorda.CustomViews.scaleImageView.ImageSource;
import com.khorda.CustomViews.scaleImageView.SubsamplingScaleImageView;
import com.khorda.Models.Products;
import com.khorda.Models.RequestResponse;
import com.khorda.Models.UserInfo;
import com.khorda.Utils.MyContextWrapper;
import com.khorda.Utils.PreferenceEditor;
import com.khorda.Utils.QuickstartPreferences;
import com.khorda.Utils.ToolsUtils;
import com.khorda.interfaces.ApiClient;
import com.khorda.interfaces.ApiInterface;
import com.khorda.interfaces.OnItemClickListener;

import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    private Products.ProductsBean.DataBean product;
    private TextView like_product,
            productName,
            productPrice,
            productCategory,
            productColor,
            productDetails,
            product_likes,
            previews_count, product_quantity;
    private Toolbar toolbar;
    private TextView toolbar_title;
    private FrameLayout frameLayoutCart;
    private AppCompatTextView textViewCartCounter;
    private UserInfo UserData;
    private int pos = -1;
    private long ProductId = -1;
    private ViewPager viewPager;
    private CircleIndicator indicator;
    private Adapter_Product_Images adapter_product_images;
    private AppCompatRatingBar rb_product_rate;
    private LinearLayout ll_add_to_like;
    private AppCompatImageView like_product_image;


    @Override
    protected void attachBaseContext(Context newBase) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String lang = PreferenceEditor.getInstance().getStringPreference(QuickstartPreferences.CURRENT_LANGUAGE);
            System.out.println("lang" + lang);
            super.attachBaseContext(MyContextWrapper.wrap(newBase, lang));
        } else {
            super.attachBaseContext(newBase);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        rb_product_rate = (AppCompatRatingBar) findViewById(R.id.rb_product_rate);
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        indicator = (CircleIndicator) findViewById(R.id.layoutDots);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        frameLayoutCart = (FrameLayout) findViewById(R.id.frameLayoutCart);
        textViewCartCounter = (AppCompatTextView) findViewById(R.id.textViewCartCounter);
        UserData = new Gson().fromJson(PreferenceEditor.getInstance().getStringPreference(QuickstartPreferences.USER_INFO), UserInfo.class);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        frameLayoutCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(getApplicationContext(), HomeActivity.class);
                i.putExtra("nav", R.id.nav_my_cart);
                startActivity(i);
                finish();


            }
        });

        toolbar_title.setText(R.string.product_title);
        ll_add_to_like = (LinearLayout) findViewById(R.id.ll_add_to_like);
        like_product_image = (AppCompatImageView) findViewById(R.id.like_product_image);

        like_product = (TextView) findViewById(R.id.like_product);
        productName = (TextView) findViewById(R.id.product_name);
        productPrice = (TextView) findViewById(R.id.product_price);
        productCategory = (TextView) findViewById(R.id.product_category);
        productColor = (TextView) findViewById(R.id.product_color);
        productDetails = (TextView) findViewById(R.id.product_details);
        product_likes = (TextView) findViewById(R.id.product_likes);
        previews_count = (TextView) findViewById(R.id.tv_previews_count);
        product_quantity = (TextView) findViewById(R.id.product_quantity);

        product = (Products.ProductsBean.DataBean) getIntent().getExtras().get("product");
        pos = (Integer) getIntent().getExtras().get("pos");


        adapter_product_images = new Adapter_Product_Images(getApplicationContext(), product.getImages());
        if (product.getImages().size() == 0) {
            product.getImages().add(null);
        }


        viewPager.setAdapter(adapter_product_images);

        adapter_product_images.setListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int position) {
                if (product.getImages().size() > 0) {

                    BitmapDrawable drawable = (BitmapDrawable) ((AppCompatImageView) itemView).getDrawable();
                    Bitmap bitmap = drawable.getBitmap();
                    if (bitmap != null) {
                        showFullImageDialog(bitmap);
                    }
                }

            }
        });

        double index = Math.ceil(new Double(product.getImages().size() / 2));
        //viewPager.setCurrentItem((int) index, true);
        int margin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 60 * 2, getResources().getDisplayMetrics());
        indicator.setViewPager(viewPager);


        productName.setText("" + product.getDisplay_name());
        productColor.setText("" + product.getColors().getDisplay_title());
        productCategory.setText("" + product.getCategory_id().getDisplay_title());
        productPrice.setText("" + product.getPrice());

        productDetails.setText("" + ToolsUtils.RemoveHtmlTags(product.getDisplay_description()));

       /* ToolsUtils.setupWebView(productDetails);
        productDetails.loadData(product.getDisplay_description(), "text/html; charset=UTF-8", null);
*/
        product_quantity.setText("" + product.getQuantity());
        product_likes.setText("" + product.getLikes());

        rb_product_rate.setRating(product.getRate_product().getRate());
        previews_count.setText("" + product.getRate_product().getCount());


        if (ToolsUtils.checkProductLiked(product)) {
            like_product_image.setImageResource(R.drawable.ic_liked);
            like_product.setText(getString(R.string.remove_from_like));
            product.setIs_liked("1");

        } else {
            like_product_image.setImageResource(R.drawable.ic_unliked);
            like_product.setText(getString(R.string.add_to_like));
            product.setIs_liked("0");
        }


        ll_add_to_like.setOnClickListener(this);

        textViewCartCounter.setText(""+ToolsUtils.getAllCartProducts().size());

        Log.d("Err", "_ID : " + product.getProduct_id());
        Log.d("Err", "ID : " + product.getId());

    }

    private void showFullImageDialog(Bitmap bitmap) {
        final Dialog dialog = new Dialog(ProductDetailsActivity.this);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.full_image_dialog);
        dialog.setCancelable(true);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        SubsamplingScaleImageView full_image = (SubsamplingScaleImageView) dialog.findViewById(R.id.img_full_image);
        TypedValue outValue = new TypedValue();
        getResources().getValue(R.dimen.image_max_scale, outValue, true);
        full_image.setMaxScale(outValue.getFloat());

        full_image.setImage(ImageSource.bitmap(bitmap));

        dialog.show();

    }

    public void product_rate_clicked(View v) {
        UserInfo UserData = new Gson().fromJson(PreferenceEditor.getInstance().getStringPreference(QuickstartPreferences.USER_INFO), UserInfo.class);
        if (UserData != null) {

            showRateDailog();
        } else {
            Toast.makeText(getApplicationContext(), R.string.err_request_login, Toast.LENGTH_SHORT).show();
        }


    }




    public void addToCart(View v) {


        try {

            boolean result = ToolsUtils.checkProductInCart(product);

            if (result) {
                Toast.makeText(getApplicationContext(), R.string.err_msg_product_already_in_cart, Toast.LENGTH_SHORT).show();
            } else {
                product.setInCart(true);
                if (product.getCategory_id() != null) {
                    product.setCat_Json(new Gson().toJson(product.getCategory_id()));
                }
                product.save();
                textViewCartCounter.setText(""+ToolsUtils.getAllCartProducts().size());

                Toast.makeText(getApplicationContext(), R.string.item_added_to_cart, Toast.LENGTH_SHORT).show();

            }


        } catch (Exception e) {
            e.printStackTrace();
        }


   /*     try {

            if (ProductId == -1) {
                if ((product.getSelectedQuantity() + 1) <= product.getQuantity()) {
                    product.setSelectedQuantity(1);
                    if (product.getCategory_id() != null) {
                        product.setCat_Json(new Gson().toJson(product.getCategory_id()));
                    }

                    product.save();
                    getAllCartProducts();
                    Toast.makeText(ProductDetailsActivity.this, R.string.item_added_to_cart, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, R.string.err_msg_add_quantity, Toast.LENGTH_SHORT).show();
                }

            } else {

                Toast.makeText(this, R.string.err_msg_product_already_in_cart, Toast.LENGTH_SHORT).show();


            }


        } catch (Exception e) {
            e.printStackTrace();
        }
*/

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    @Override
    protected void onResume() {
        super.onResume();
        textViewCartCounter.setText(""+ToolsUtils.getAllCartProducts().size());
    }

    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.ll_add_to_like) {

            if (product.getIs_liked().equalsIgnoreCase("0")) {

                LikeProductRequest();

            } else {

                unLikeProductRequest();
            }


        }

    }

    private void showRateDailog() {
        final Dialog dialog = new Dialog(ProductDetailsActivity.this);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.rate_dailog_layout);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        final AppCompatRatingBar ratingBar = (AppCompatRatingBar) dialog.findViewById(R.id.rb_product_rate);
        Button submit = (Button) dialog.findViewById(R.id.btn_submit);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (ToolsUtils.isNetworkAvailable()) {

                    final AVLoadingIndicatorDialog loadingIndicatorDialog = ToolsUtils.CreateDialog(ProductDetailsActivity.this, null);
                    loadingIndicatorDialog.show();

                    ApiInterface apiService =
                            ApiClient.getClient().create(ApiInterface.class);
                    Call<RequestResponse> call = apiService.rateProduct(product.getProduct_id(), ratingBar.getRating());
                    call.enqueue(new Callback<RequestResponse>() {
                        @Override
                        public void onResponse(Call<RequestResponse> call, Response<RequestResponse> response) {

                            System.out.println("Code" + response.code());
                            System.out.println("raw" + response.raw());
                            System.out.println("Code" + response.message());
                            loadingIndicatorDialog.hide();

                            if (response.code() == 200) {

                                if (response.body().isStatus()) {

                                    Toast.makeText(ProductDetailsActivity.this, "" + response.body().getDisplay_message(), Toast.LENGTH_SHORT).show();

                                    dialog.hide();

                                } else {
                                    Toast.makeText(getApplicationContext(), "" + response.body().getDisplay_message(), Toast.LENGTH_SHORT).show();

                                }

                            } else {
                                Toast.makeText(getApplicationContext(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();

                            }

                        }

                        @Override
                        public void onFailure(Call<RequestResponse> call, Throwable t) {
                            loadingIndicatorDialog.hide();
                            Toast.makeText(getApplicationContext(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();

                        }
                    });

                } else {
                    Toast.makeText(getApplicationContext(), R.string.err_msg_no_internet_connection, Toast.LENGTH_SHORT).show();
                }
            }
        });
        dialog.show();

    }

    private void LikeProductRequest() {

        if (ToolsUtils.isNetworkAvailable()) {

            final AVLoadingIndicatorDialog dialog = ToolsUtils.CreateDialog(this, null);
            dialog.show();


            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<RequestResponse> call = apiService.likeProduct(product.getProduct_id());

            call.enqueue(new Callback<RequestResponse>() {
                @Override
                public void onResponse(Call<RequestResponse> call, Response<RequestResponse> response) {

                    System.out.println("Code" + response.code());
                    System.out.println("raw" + response.raw());
                    System.out.println("Code" + response.message());
                    dialog.hide();

                    if (response.code() == 200) {

                        if (response.body().isStatus()) {
                            Toast.makeText(getApplicationContext(), "" + response.body().getDisplay_message(), Toast.LENGTH_SHORT).show();

                            like_product_image.setImageResource(R.drawable.ic_liked);
                            like_product.setText(getString(R.string.remove_from_like));

                            String likes = "" + ((Integer.parseInt(product.getLikes())) + 1);
                            product_likes.setText(likes);
                            product.setIs_liked("1");
                            product.setLikes(likes);
                            if (product.getCategory_id() != null) {
                                product.setCat_Json(new Gson().toJson(product.getCategory_id()));
                            }
                            product.save();


                            Intent intent = new Intent(QuickstartPreferences.ITEM_CHANGED_FILTER);
                            intent.putExtra("product", product);
                            intent.putExtra("position", pos);
                            sendBroadcast(intent);

                        } else {
                            Toast.makeText(getApplicationContext(), "" + response.body().getDisplay_message(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Toast.makeText(getApplicationContext(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();

                    }

                }

                @Override
                public void onFailure(Call<RequestResponse> call, Throwable t) {
                    dialog.hide();
                    Toast.makeText(getApplicationContext(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();

                }
            });

        } else {
            Toast.makeText(getApplicationContext(), R.string.err_msg_no_internet_connection, Toast.LENGTH_SHORT).show();
        }

    }

    private void unLikeProductRequest() {
     if (ToolsUtils.isNetworkAvailable()) {

            final AVLoadingIndicatorDialog dialog = ToolsUtils.CreateDialog(this, null);
            dialog.show();


            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<RequestResponse> call = apiService.unLikeProduct(product.getProduct_id());

            call.enqueue(new Callback<RequestResponse>() {
                @Override
                public void onResponse(Call<RequestResponse> call, Response<RequestResponse> response) {

                    System.out.println("Code" + response.code());
                    System.out.println("raw" + response.raw());
                    System.out.println("Code" + response.message());
                    dialog.hide();

                    if (response.code() == 200) {

                        if (response.body().isStatus()) {
                            Toast.makeText(getApplicationContext(), "" + response.body().getDisplay_message(), Toast.LENGTH_SHORT).show();

                            like_product_image.setImageResource(R.drawable.ic_unliked);
                            like_product.setText(getString(R.string.add_to_like));
                            String likes = "" + ((Integer.parseInt(product.getLikes())) - 1);
                            product_likes.setText(likes);
                            product.setIs_liked("0");
                            product.setLikes(likes);
                            product.save();

                            Intent intent = new Intent(QuickstartPreferences.ITEM_CHANGED_FILTER);
                            intent.putExtra("product", product);
                            intent.putExtra("position", pos);
                            sendBroadcast(intent);

                        } else {
                            Toast.makeText(getApplicationContext(), "" + response.body().getDisplay_message(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Toast.makeText(getApplicationContext(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();

                    }

                }

                @Override
                public void onFailure(Call<RequestResponse> call, Throwable t) {
                    dialog.hide();
                    Toast.makeText(getApplicationContext(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();

                }
            });

        } else {
            Toast.makeText(getApplicationContext(), R.string.err_msg_no_internet_connection, Toast.LENGTH_SHORT).show();
        }


    }


}
