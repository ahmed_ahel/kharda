package com.khorda;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.khorda.CustomViews.AVLoadingIndicatorDialog;
import com.khorda.CustomViews.Button;
import com.khorda.CustomViews.EditText;
import com.khorda.CustomViews.TextView;
import com.khorda.Models.ForgetPassword;
import com.khorda.Models.UserInfo;
import com.khorda.Utils.MyContextWrapper;
import com.khorda.Utils.PreferenceEditor;
import com.khorda.Utils.QuickstartPreferences;
import com.khorda.Utils.ToolsUtils;
import com.khorda.interfaces.ApiClient;
import com.khorda.interfaces.ApiInterface;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Converter;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private TwitterLoginButton loginButton;
    private EditText mobile, password;
    private Button login, signup;
    private TextView forgetPassword;
    private TwitterSession session;
    private String TAG = "Login";
    private TwitterAuthClient twitterAuthClient;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mobile = (EditText) findViewById(R.id.mobile);
        password = (EditText) findViewById(R.id.password);
        forgetPassword = (TextView) findViewById(R.id.forget_password);
        login = (Button) findViewById(R.id.login_btn);
        signup = (Button) findViewById(R.id.signup_btn);
        twitterAuthClient = new TwitterAuthClient();
        changeStatusBarColor();
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mobile.getText().toString().isEmpty()) {
                    Toast.makeText(LoginActivity.this, R.string.err_msg_mobile, Toast.LENGTH_SHORT).show();
                    return;
                }
                if (mobile.getText().toString().length() < 11) {
                    Toast.makeText(LoginActivity.this, R.string.err_msg_mobile_length, Toast.LENGTH_SHORT).show();
                    return;
                }
                if (password.getText().toString().isEmpty()) {
                    Toast.makeText(LoginActivity.this, R.string.err_msg_password, Toast.LENGTH_SHORT).show();
                    return;
                }
                RequestLogin();
            }
        });
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), SignUpActivity.class));
            }
        });
        forgetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showForgetPasswordDialog();
            }
        });
        loginButton = (TwitterLoginButton) findViewById(R.id.login_button);
        loginButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                // Do something with result, which provides a TwitterSession for making API calls

                session = result.data;

                Log.d("Err", result.data.getUserName());

                final TwitterSession twitterSession = result.data;
                twitterAuthClient.requestEmail(twitterSession, new com.twitter.sdk.android.core.Callback<String>() {
                    @Override
                    public void success(Result<String> emailResult) {
                        String email = emailResult.data;

                        Log.d("Email", "" + email);
                        Log.d("session", "" + session.getAuthToken());

                        if (email != null && email.length() > 0) {
                            requestLoginByTwitter(email, session.getAuthToken());
                        } else {
                            Toast.makeText(LoginActivity.this, R.string.twitter_request_email_failed, Toast.LENGTH_SHORT).show();

                        }

                    }

                    @Override
                    public void failure(TwitterException e) {
                        e.printStackTrace();
                        Toast.makeText(LoginActivity.this, R.string.twitter_request_email_failed, Toast.LENGTH_SHORT).show();

                        // callback.onTwitterSignInFailed(e);
                    }
                });


            }

            @Override
            public void failure(TwitterException exception) {
                // Do something on failure

                Toast.makeText(LoginActivity.this, R.string.twitter_session_token_experid, Toast.LENGTH_SHORT).show();
            }
        });


    }

    private void requestLoginByTwitter(final String Twitter_email, TwitterAuthToken authToken) {


        Log.d("Err", Twitter_email);
        Log.d("Err", authToken.token);
        Log.d("Err", authToken.secret);
        System.out.println("Fcm " + FirebaseInstanceId.getInstance().getToken());


        if (ToolsUtils.isNetworkAvailable()) {
            final AVLoadingIndicatorDialog dialog = ToolsUtils.CreateDialog(this, null);
            dialog.show();
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            Map<String, RequestBody> map = new HashMap<>();
            final RequestBody email = RequestBody.create(MediaType.parse("text/plain"), Twitter_email);
            RequestBody twitter_token = RequestBody.create(MediaType.parse("text/plain"), authToken.token);
            RequestBody twitter_secret = RequestBody.create(MediaType.parse("text/plain"), authToken.secret);
            RequestBody fcm = RequestBody.create(MediaType.parse("text/plain"), FirebaseInstanceId.getInstance().getToken());
            map.put("email", email);
            map.put("token", twitter_token);
            map.put("secret_token", twitter_secret);
            map.put("FCM_token", fcm);

            Call<UserInfo> call = apiService.loginByTwitter(map);
            call.enqueue(new retrofit2.Callback<UserInfo>() {
                @Override
                public void onResponse(Call<UserInfo> call, Response<UserInfo> response) {

                    System.out.println("Code" + response.code());
                    System.out.println("raw" + response.raw());
                    System.out.println("Code" + response.message());
                    dialog.hide();

                    if (response.code() == 200) {
                        if (response.body().isStatus()) {
                            PreferenceEditor.getInstance().setStringPreference(QuickstartPreferences.USER_INFO, new Gson().toJson(response.body()));
                            startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                        } else if (response.body().getDisplay_message().equalsIgnoreCase("this email do not match our records")) {

                            Intent i = new Intent(getApplicationContext(), SignUpActivity.class);
                            i.putExtra("fullName", session.getUserName());
                            i.putExtra("email", Twitter_email);
                            startActivity(i);


                        } else if (response.body().getDisplay_message().equalsIgnoreCase("not active")) {

                            String mobile = response.body().getUser().getMobile();
                            Log.d("TAG", "mobile" + mobile);
                            Intent i = new Intent(getApplicationContext(), VerificationActivity.class);
                            i.putExtra("mobile", mobile);
                            startActivity(i);

                        } else {

                            Toast.makeText(LoginActivity.this, "" + response.body().getDisplay_message(), Toast.LENGTH_SHORT).show();
                        }
                    } else if (response.code() == 400) {
                        Toast.makeText(LoginActivity.this, R.string.twitter_session_token_experid, Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(getApplicationContext(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();

                    }

                }

                @Override
                public void onFailure(Call<UserInfo> call, Throwable t) {
                    t.printStackTrace();
                    dialog.hide();
                    System.out.println("onFailure");
                    Toast.makeText(getApplicationContext(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();

                }
            });

        } else {
            Toast.makeText(getApplicationContext(), R.string.err_msg_no_internet_connection, Toast.LENGTH_SHORT).show();
        }
    }


    private void showForgetPasswordDialog() {

        try {
            final Dialog ForgetPasswordDailog = new Dialog(this);
            ForgetPasswordDailog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            LayoutInflater mInflater = (LayoutInflater) getSystemService(Activity.LAYOUT_INFLATER_SERVICE);


            View v = mInflater.inflate(R.layout.layout_forget_password, null);

            final EditText forget_email = (EditText) v.findViewById(R.id.forget_email);
            Button continue_btn = (Button) v.findViewById(R.id.continue_btn);

            AppCompatImageButton close_btn = (AppCompatImageButton) v.findViewById(R.id.close_btn);

            close_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ForgetPasswordDailog.dismiss();
                }
            });
            continue_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (forget_email.getText().toString().isEmpty()) {
                        Toast.makeText(LoginActivity.this, R.string.err_msg_email, Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (!ToolsUtils.isValidEmail(forget_email.getText().toString())) {
                        Toast.makeText(LoginActivity.this, R.string.err_msg_valid_email, Toast.LENGTH_SHORT).show();
                        return;
                    }

                    ForgetPasswordDailog.dismiss();
                    requestForgetPassword(forget_email.getText().toString());

                }
            });


            ForgetPasswordDailog.setContentView(v);
            ForgetPasswordDailog.setCancelable(true);
            ForgetPasswordDailog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            ForgetPasswordDailog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            ForgetPasswordDailog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            ForgetPasswordDailog.show();
            ForgetPasswordDailog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    System.gc();
                }
            });
        } catch (
                Exception e)

        {
            e.printStackTrace();
        }


    }

    private void requestForgetPassword(final String email) {

        if (ToolsUtils.isNetworkAvailable()) {

            Log.d("Err", "Email :" + email);

            final AVLoadingIndicatorDialog dialog = ToolsUtils.CreateDialog(this, null);
            dialog.show();
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            Call<ForgetPassword> call = apiService.forgotPassword(email);

            Log.d("Err", "" + call.toString());

            call.enqueue(new retrofit2.Callback<ForgetPassword>() {
                @Override
                public void onResponse(Call<ForgetPassword> call, Response<ForgetPassword> response) {
                    System.out.println("Code" + response.code());
                    System.out.println("raw" + response.raw());
                    System.out.println("Code" + response.message());
                    dialog.hide();
                    if (response.code() == 200) {
                        if (response.body().isStatus()) {
                            Toast.makeText(LoginActivity.this, response.body().getDisplay_message(), Toast.LENGTH_SHORT).show();
                        }
                    } else if (response.code() == 400) {

                        Converter<ResponseBody, ForgetPassword> converter = ApiClient.getClient()
                                .responseBodyConverter(ForgetPassword.class, new Annotation[0]);
                        ForgetPassword result = null;
                        try {
                            result = converter.convert(response.errorBody());
                            Toast.makeText(LoginActivity.this, result.getDisplay_message(), Toast.LENGTH_SHORT).show();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ForgetPassword> call, Throwable t) {
                    t.printStackTrace();
                    dialog.hide();
                    System.out.println("onFailure");
                    Toast.makeText(getApplicationContext(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(getApplicationContext(), R.string.err_msg_no_internet_connection, Toast.LENGTH_SHORT).show();
        }
    }


    private void RequestLogin() {

        if (ToolsUtils.isNetworkAvailable()) {


            System.out.println("Fcm " + FirebaseInstanceId.getInstance().getToken());

            final AVLoadingIndicatorDialog dialog = ToolsUtils.CreateDialog(this, null);
            dialog.show();
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            Map<String, RequestBody> map = new HashMap<>();
            RequestBody mobileNum = RequestBody.create(MediaType.parse("text/plain"), mobile.getText().toString().trim());
            RequestBody pass = RequestBody.create(MediaType.parse("text/plain"), password.getText().toString().trim());
            RequestBody fcm = RequestBody.create(MediaType.parse("text/plain"), FirebaseInstanceId.getInstance().getToken());
            map.put("mobile", mobileNum);
            map.put("password", pass);
            map.put("FCM_token", fcm);

            Call<UserInfo> call = apiService.login(map);
            call.enqueue(new retrofit2.Callback<UserInfo>() {
                @Override
                public void onResponse(Call<UserInfo> call, Response<UserInfo> response) {

                    System.out.println("Code" + response.code());
                    System.out.println("raw" + response.raw());
                    System.out.println("Code" + response.message());
                    dialog.hide();

                    if (response.code() == 200) {
                        if (response.body().isStatus()) {
                            PreferenceEditor.getInstance().setStringPreference(QuickstartPreferences.USER_INFO, new Gson().toJson(response.body()));
                            startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                        } else if (response.body().getDisplay_message().equalsIgnoreCase("not active")) {

                            Intent i = new Intent(getApplicationContext(), VerificationActivity.class);
                            i.putExtra("mobile", mobile.getText().toString().trim());
                            startActivity(i);
                        } else {
                            Toast.makeText(getApplicationContext(), R.string.err_msg_incorrect_mobile_or_pass, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<UserInfo> call, Throwable t) {
                    t.printStackTrace();
                    dialog.hide();
                    System.out.println("onFailure");
                    Toast.makeText(getApplicationContext(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(getApplicationContext(), R.string.err_msg_no_internet_connection, Toast.LENGTH_SHORT).show();
        }
    }


    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.WHITE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Pass the activity result to the login button.
        loginButton.onActivityResult(requestCode, resultCode, data);
    }
}
