package com.khorda.interfaces;

/**
 * Created by ahmed on 10/19/16.
 */


public interface OnLoadMoreListener {
    void onLoadMore();
}