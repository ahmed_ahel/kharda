package com.khorda.interfaces;

import android.view.View;

import com.khorda.Models.Products;

/**
 * Created by ez on 8/21/2017.
 */

public interface OnItemChangedListener {
    void onItemChanged(int position, Products.ProductsBean.DataBean p);

}
