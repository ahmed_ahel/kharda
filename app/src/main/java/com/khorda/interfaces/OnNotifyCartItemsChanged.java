package com.khorda.interfaces;

/**
 * Created by ez on 8/20/2017.
 */

public interface OnNotifyCartItemsChanged {
    public  void onCartChanged();
}
