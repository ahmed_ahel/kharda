package com.khorda.interfaces;

import android.util.Log;

import com.google.gson.Gson;
import com.khorda.Models.UserInfo;
import com.khorda.Utils.PreferenceEditor;
import com.khorda.Utils.QuickstartPreferences;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.Request;
 import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by AHMED-PC on 12/6/2016.
 */

public class ApiClient {
    public static final String BASE_URL = "http://khardakw.com/api/";
    private static Retrofit retrofit = null;



    public static OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .readTimeout(30, TimeUnit.SECONDS)
            .connectTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request.Builder builder = chain.request().newBuilder();
                    builder.addHeader("Accept", "application/json");

                    UserInfo UserData = new Gson().fromJson(PreferenceEditor.getInstance().getStringPreference(QuickstartPreferences.USER_INFO), UserInfo.class);

                    if (UserData != null) {
                        Log.d("TAG","Api_token "+UserData.getUser().getApi_token());
                         builder.addHeader("Authorization", "Bearer "+UserData.getUser().getApi_token());
                      //  builder.addHeader("Authorization", "Bearer "+token);


                    }

                    Request request = builder.build();

                    return chain.proceed(request);
                }
            })


            .build();

    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create(new Gson()))
                    .build();
        }


        return retrofit;
    }



}
