package com.khorda.interfaces;

import com.khorda.Models.Categories;
import com.khorda.Models.Colors;
import com.khorda.Models.Countries;
import com.khorda.Models.CountriesWithCities;
import com.khorda.Models.ForgetPassword;
import com.khorda.Models.MyPurchases;
import com.khorda.Models.PaymentResponse;
import com.khorda.Models.PrivacyPolicy;
import com.khorda.Models.Products;
import com.khorda.Models.RequestResponse;
import com.khorda.Models.TermsConditions;
import com.khorda.Models.UserInfo;
import com.khorda.Models.Prices;
import com.khorda.Models.WalkThrough;
import com.khorda.Models.likedProducts;

import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {


    @GET("getCountries")
    Call<Countries> getCountries();

    @GET("getCountryCity")
    Call<CountriesWithCities> getCountriesWithCities();


    @Multipart
    @POST("signup")
    Call<UserInfo> signup(@PartMap() Map<String, RequestBody> partMap);

    @Multipart
    @POST("login")
    Call<UserInfo> login(@PartMap() Map<String, RequestBody> partMap);

    @Multipart
    @POST("loginTwitter")
    Call<UserInfo> loginByTwitter(@PartMap() Map<String, RequestBody> partMap);


    @FormUrlEncoded
    @POST("password/email")
    Call<ForgetPassword> forgotPassword(@Field("email") String email);

    @FormUrlEncoded
    @POST("requestNewCode")
    Call<RequestResponse> requestNewCode(@Field("mobile") String mobile);


    @FormUrlEncoded
    @POST("checkCode")
    Call<UserInfo> checkCode(@Field("mobile") String mobile,@Field("code") String code);


    @GET("logout")
    Call<RequestResponse> logout();

    @Multipart
    @POST("editUser")
    Call<UserInfo> editUser(@PartMap() Map<String, RequestBody> partMap);

    @FormUrlEncoded
    @POST("changePassword")
    Call<RequestResponse> changePassword(@Field("oldpassword") String oldpassword,@Field("password") String password);

    @GET("categories")
    Call<Categories> getCategories();

    @GET("colors")
    Call<Colors> getColors();

    @GET("prices")
    Call<Prices> getPrices();

    @GET("likeProduct/{id}")
    Call<RequestResponse> likeProduct(@Path("id") String productId);

    @GET("unLikeProduct/{id}")
    Call<RequestResponse> unLikeProduct(@Path("id") String productId);

    @GET("likedProducts")
    Call<likedProducts> likedProducts();


    @GET("products")
    Call<Products> getProducts(@Query("page") int pageId);

    @Multipart
    @POST("sort")
    Call<Products> SortProducts(@PartMap() Map<String, RequestBody> partMap ,@Query("page") int pageId);

    @Multipart
    @POST("sendOrder")
    Call<RequestResponse> sendCashOnDeliveryOrder(@PartMap() Map<String, RequestBody> partMap);

    @Multipart
    @POST("payment")
    Call<PaymentResponse> sendPaymentOrder(@PartMap() Map<String, RequestBody> partMap);


    @GET("getWalkThrough")
    Call<WalkThrough> getWalkThrough();

    @GET("getPrivacyPolicy")
    Call<PrivacyPolicy> getPrivacyPolicy();


    @GET("getTermsConditions")
    Call<TermsConditions> getTermsConditions();


    @GET("myPurchases")
    Call<MyPurchases> getMyPurchases(@Query("page") int pageId);


    @GET("checkOrderPayment/{id}")
    Call<RequestResponse> checkOrderPayment(@Path("id") String orderId);


    @GET("rateProduct/{pid}/{rate}")
    Call<RequestResponse> rateProduct(@Path("pid") String productId,@Path("rate") float rate);


}
