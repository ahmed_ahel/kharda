package com.khorda;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.khorda.CustomViews.Button;
import com.khorda.CustomViews.TextView;
import com.khorda.Models.Payment;
import com.khorda.Models.Products;
import com.khorda.Utils.MyContextWrapper;
import com.khorda.Utils.PreferenceEditor;
import com.khorda.Utils.QuickstartPreferences;
import com.khorda.Utils.ToolsUtils;

import java.util.List;

public class PaymentActivity extends AppCompatActivity implements View.OnClickListener {

    private Payment payment;
    private AppCompatImageView cashOnDelivery, epay;
    private Button checkOutBtn;
    private int PaymentType = -1;
    private Toolbar toolbar;
    private TextView toolbar_title;
    private AppCompatTextView textViewCartCounter;
    private AppCompatImageView header_image;


    @Override
    protected void attachBaseContext(Context newBase) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String lang = PreferenceEditor.getInstance().getStringPreference(QuickstartPreferences.CURRENT_LANGUAGE);
            super.attachBaseContext(MyContextWrapper.wrap(newBase, lang));
        }else {
            super.attachBaseContext(newBase);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        header_image = (AppCompatImageView) findViewById(R.id.image);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        textViewCartCounter = (AppCompatTextView) findViewById(R.id.textViewCartCounter);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar_title.setText(R.string.payment_title);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        if (ToolsUtils.getDeviceLang().equalsIgnoreCase(QuickstartPreferences.AR_LAN)) {
            header_image.setImageResource(R.drawable.rtl_payment);
        } else {
            header_image.setImageResource(R.drawable.payment);
        }


        cashOnDelivery = (AppCompatImageView) findViewById(R.id.cashOnDelivery);
        epay = (AppCompatImageView) findViewById(R.id.epay);
        checkOutBtn = (Button) findViewById(R.id.check_out_btn);
        cashOnDelivery.setOnClickListener(this);
        epay.setOnClickListener(this);
        checkOutBtn.setOnClickListener(this);

        try {
            List<Payment> list = Payment.listAll(Payment.class);
            if (list == null) {
                finish();
            }

            payment = list.get(list.size() - 1);


            textViewCartCounter.setText(""+ToolsUtils.getAllCartProducts().size());

            Float totalPrice = CalculatePrice(ToolsUtils.getAllCartProducts());

            System.out.println("totalPrice : " + totalPrice);

            if (totalPrice > 30) {
                epay.setVisibility(View.VISIBLE);
                cashOnDelivery.setVisibility(View.VISIBLE);

            } else {
                epay.setVisibility(View.GONE);
                cashOnDelivery.setVisibility(View.VISIBLE);
                cashOnDelivery.setBackgroundResource(R.drawable.payment_gateway_selected);
                epay.setBackgroundColor(getResources().getColor(android.R.color.transparent));

                PaymentType = 0;
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private float CalculatePrice(List<Products.ProductsBean.DataBean> list) {

        float totalPrice = 0;
        for (Products.ProductsBean.DataBean item : list) {
            System.out.println("getPrice: " + item.getPrice());
            if (Float.valueOf((item.getPrice())) > 0) {
                totalPrice += (Float.valueOf(item.getPrice()) * item.getSelectedQuantity());
            }
        }

        return totalPrice;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.cashOnDelivery) {
            cashOnDelivery.setBackgroundResource(R.drawable.payment_gateway_selected);
            epay.setBackgroundColor(getResources().getColor(android.R.color.transparent));
            PaymentType = 0;
        } else if (view.getId() == R.id.epay) {
            epay.setBackgroundResource(R.drawable.payment_gateway_selected);
            cashOnDelivery.setBackgroundColor(getResources().getColor(android.R.color.transparent));
            PaymentType = 1;
        } else if (view.getId() == R.id.check_out_btn) {
            if (PaymentType == -1) {
                Toast.makeText(this, R.string.err_msg_choose_payment_type, Toast.LENGTH_SHORT).show();
                return;
            }

            payment.setType(PaymentType);
            payment.save();

            startActivity(new Intent(getApplicationContext(), FinishOrderActivity.class));

        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


}
