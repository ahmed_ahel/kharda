package com.khorda;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

import com.khorda.Utils.MyContextWrapper;
import com.khorda.Utils.PreferenceEditor;
import com.khorda.Utils.QuickstartPreferences;
import com.khorda.Utils.ToolsUtils;

public class SplashActivity extends AppCompatActivity {

    private final int TIME_DELAY = 3000;

  /*  @Override
    protected void attachBaseContext(Context newBase) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String lang = PreferenceEditor.getInstance().getStringPreference(QuickstartPreferences.CURRENT_LANGUAGE);
            if (lang == null) {
                lang =  ToolsUtils.setDefualtLang();

            }
            super.attachBaseContext(MyContextWrapper.wrap(newBase, lang));
        } else {
            super.attachBaseContext(newBase);
        }
    }*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        String lang = PreferenceEditor.getInstance().getStringPreference(QuickstartPreferences.CURRENT_LANGUAGE);
        if (lang == null) {
            ToolsUtils.setDefualtLang();
        } else {
            ToolsUtils.changeAppLanguage(lang);
        }

        changeStatusBarColor();
        new Handler().postDelayed(new Runnable() {
            public void run() {
                Intent i;
                if (PreferenceEditor.getInstance().getBooleanPreference(QuickstartPreferences.IS_NOT_FIRST_TIME_LAUNCH)) {
                    i = new Intent(getApplicationContext(), HomeActivity.class);
                } else {
                    i = new Intent(getApplicationContext(), WalkthroughActivity.class);
                }

                finish();
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }
        }, TIME_DELAY);
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.WHITE);
        }
    }
}
