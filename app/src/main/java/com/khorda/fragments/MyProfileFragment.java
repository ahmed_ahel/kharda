package com.khorda.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.khorda.CitiesListDialog;
import com.khorda.CustomViews.AVLoadingIndicatorDialog;
import com.khorda.CustomViews.Button;
import com.khorda.CustomViews.EditText;
import com.khorda.CustomViews.TextView;
import com.khorda.Models.Countries;
import com.khorda.Models.UserInfo;
import com.khorda.R;
import com.khorda.SignUpActivity;
import com.khorda.Utils.PreferenceEditor;
import com.khorda.Utils.QuickstartPreferences;
import com.khorda.Utils.ToolsUtils;
import com.khorda.interfaces.ApiClient;
import com.khorda.interfaces.ApiInterface;

import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyProfileFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {


    private Button save_btn;
    private EditText fullName,
            email,
            mobile;
    private TextView city;
    private int CityId = -1;
    private UserInfo UserData;
    private SwipeRefreshLayout swipe;
    private Countries cities;


    public MyProfileFragment() {
    }

    public static MyProfileFragment newInstance() {
        MyProfileFragment fragment = new MyProfileFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_my_profile, container, false);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        city = (TextView) getView().findViewById(R.id.city);
        fullName = (EditText) getView().findViewById(R.id.fullName);
        email = (EditText) getView().findViewById(R.id.email);
        mobile = (EditText) getView().findViewById(R.id.mobile);
         save_btn = (Button) getView().findViewById(R.id.save_btn);
        swipe = (SwipeRefreshLayout) getView().findViewById(R.id.swipe);
        swipe.setOnRefreshListener(this);
        try {
            UserData = new Gson().fromJson(PreferenceEditor.getInstance().getStringPreference(QuickstartPreferences.USER_INFO), UserInfo.class);


            if (UserData != null) {
                CityId = UserData.getUser().getCountry_id().getId();
                city.setText(UserData.getUser().getCountry_id().getDisplay_title());
                fullName.setText(UserData.getUser().getFullname());
                email.setText(UserData.getUser().getEmail());
                mobile.setText(UserData.getUser().getMobile());
            }

        }catch (Exception e){
            e.printStackTrace();

        }

        city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (cities != null) {
                    CitiesListDialog citiesListDialog = new CitiesListDialog((AppCompatActivity)getActivity());
                    citiesListDialog.setCountries(cities);
                    citiesListDialog.setSelectCity(new CitiesListDialog.SelectedCity() {
                        @Override
                        public void getSelectedCity(Countries.CountriesBean SelectedCity) {
                            city.setText(SelectedCity.getDisplay_title());
                            CityId = SelectedCity.getId();
                            System.out.println("Display_title" + SelectedCity.getDisplay_title());
                            System.out.println("Display id" + CityId);


                        }
                    });
                    citiesListDialog.show();

                } else {

                    Toast.makeText(getActivity(), R.string.swipe_to_refresh, Toast.LENGTH_LONG).show();

                }
            }
        });


        save_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(UserData ==null){
                    Toast.makeText(getActivity(), R.string.err_request_login, Toast.LENGTH_SHORT).show();
                    return;
                }

                if (fullName.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), R.string.err_msg_full_name, Toast.LENGTH_SHORT).show();
                    return;
                }

                if (email.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), R.string.err_msg_email, Toast.LENGTH_SHORT).show();
                    return;
                }

                if (!ToolsUtils.isValidEmail(email.getText().toString())) {
                    Toast.makeText(getActivity(), R.string.err_msg_valid_email, Toast.LENGTH_SHORT).show();
                    return;
                }

                if (city.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), R.string.err_msg_city, Toast.LENGTH_SHORT).show();
                    return;
                }

                if (mobile.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), R.string.err_msg_mobile, Toast.LENGTH_SHORT).show();
                    return;
                }

                if (mobile.getText().toString().length() < 11) {
                    Toast.makeText(getActivity(), R.string.err_msg_mobile_length, Toast.LENGTH_SHORT).show();
                    return;
                }

                RequestEditProfile();


            }
        });


        requestGetCities();

    }


    private void RequestEditProfile() {

        if (ToolsUtils.isNetworkAvailable()) {

            final AVLoadingIndicatorDialog dialog = ToolsUtils.CreateDialog((AppCompatActivity)getActivity(), null);
            dialog.show();
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            RequestBody full_name = RequestBody.create(MediaType.parse("text/plain"), fullName.getText().toString().trim());
            RequestBody emailAddress = RequestBody.create(MediaType.parse("text/plain"), email.getText().toString().trim());
            RequestBody mobileNum = RequestBody.create(MediaType.parse("text/plain"), mobile.getText().toString().trim());
            RequestBody fcm = RequestBody.create(MediaType.parse("text/plain"), "ddd");// FirebaseInstanceId.getInstance().getToken());
            RequestBody city_id = RequestBody.create(MediaType.parse("text/plain"), "" + CityId);


            Map<String, RequestBody> map = new HashMap<>();
            map.put("fullname", full_name);
            map.put("email", emailAddress);
            map.put("mobile", mobileNum);
          //  map.put("FCM_token", fcm);
            map.put("country_id", city_id);


            Call<UserInfo> call = apiService.editUser(map);
            call.enqueue(new retrofit2.Callback<UserInfo>() {
                @Override
                public void onResponse(Call<UserInfo> call, Response<UserInfo> response) {

                    System.out.println("Code" + response.code());
                    System.out.println("raw" + response.raw());
                    System.out.println("Code" + response.message());
                    dialog.hide();

                    if (response.code() == 200) {

                        if(response.body().isStatus()) {
                            PreferenceEditor.getInstance().setStringPreference(QuickstartPreferences.USER_INFO, new Gson().toJson(response.body()));

                        }else {
                             Log.d("tag","res"+response.body().getDisplay_message());

                        }
                    } else {
                        Toast.makeText(getActivity(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();

                    }

                }

                @Override
                public void onFailure(Call<UserInfo> call, Throwable t) {
                    t.printStackTrace();
                    dialog.hide();
                    System.out.println("onFailure");
                    Toast.makeText(getActivity(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();

                }
            });

        } else {
            Toast.makeText(getActivity(), R.string.err_msg_no_internet_connection, Toast.LENGTH_SHORT).show();
        }
    }

    private void requestGetCities() {
        if (ToolsUtils.isNetworkAvailable()) {

            swipe.setRefreshing(true);

            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<Countries> call = apiService.getCountries();

            call.enqueue(new Callback<Countries>() {
                @Override
                public void onResponse(Call<Countries> call, Response<Countries> response) {

                    System.out.println("Code" + response.code());
                    System.out.println("raw" + response.raw());
                    System.out.println("Code" + response.message());
                    swipe.setRefreshing(false);

                    if (response.code() == 200 && response.body().isStatus()) {

                        cities = response.body();
                    } else {
                        Toast.makeText(getActivity(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<Countries> call, Throwable t) {
                    swipe.setRefreshing(false);
                    Toast.makeText(getActivity(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            swipe.setRefreshing(false);
            Toast.makeText(getActivity(), R.string.err_msg_no_internet_connection, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRefresh() {
        requestGetCities();
    }
}
