package com.khorda.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.khorda.Adapter.Adapter_products;
import com.khorda.CategoriesListDialog;
import com.khorda.ColorsListDialog;
import com.khorda.CustomViews.AVLoadingIndicatorDialog;
import com.khorda.CustomViews.TextView;
import com.khorda.Models.Categories;
import com.khorda.Models.Colors;
import com.khorda.Models.Prices;
import com.khorda.Models.Products;
import com.khorda.Models.RequestResponse;
import com.khorda.PriceListDialog;
import com.khorda.ProductDetailsActivity;
import com.khorda.R;
import com.khorda.Utils.QuickstartPreferences;
import com.khorda.Utils.ToolsUtils;
import com.khorda.interfaces.ApiClient;
import com.khorda.interfaces.ApiInterface;
import com.khorda.interfaces.OnItemClickListener;
import com.khorda.interfaces.OnLoadMoreListener;
import com.khorda.interfaces.OnNotifyCartItemsChanged;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private RecyclerView rv_products_list;
    private List<Products.ProductsBean.DataBean> productsList;
    private List<Products.AdsBean> adsList;
    private Adapter_products adapter_products;
    BroadcastReceiver broad = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Products.ProductsBean.DataBean pro = (Products.ProductsBean.DataBean) intent.getExtras().get("product");
            int pos = intent.getExtras().getInt("position");
            productsList.set(pos, pro);
            adapter_products.notifyItemChanged(pos);
        }
    };
    private int pageId = 1;
    private int pageSortId = 1;
    private TextView category_filter, price_filter, color_filter;
    private int SelectCategoryId = -1;
    private int SelectedColorId = -1;
    private int SelectedPriceId = -1;
    private Products products;
    private LinearLayout category_filter_layout, price_filter_layout, color_filter_layout;
    private SwipeRefreshLayout swipe;
    private Categories categories;
    private Prices prices;
    private Colors colors;
    private OnNotifyCartItemsChanged listener;


    public ProductsFragment() {
    }

    public static ProductsFragment newInstance() {
        return new ProductsFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (OnNotifyCartItemsChanged) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_products, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        productsList = new ArrayList<>();
        adsList = new ArrayList<>();
        rv_products_list = (RecyclerView) view.findViewById(R.id.products_list);
        category_filter = (TextView) view.findViewById(R.id.category_filter);
        price_filter = (TextView) view.findViewById(R.id.price_filter);
        color_filter = (TextView) view.findViewById(R.id.color_filter);
        swipe = (SwipeRefreshLayout) view.findViewById(R.id.swipe);
        swipe.setOnRefreshListener(this);
        category_filter_layout = (LinearLayout) view.findViewById(R.id.category_filter_layout);
        price_filter_layout = (LinearLayout) view.findViewById(R.id.price_filter_layout);
        color_filter_layout = (LinearLayout) view.findViewById(R.id.color_filter_layout);

        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);
        rv_products_list.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rv_products_list.getContext(),
                layoutManager.getOrientation());
        //rv_products_list.addItemDecoration(dividerItemDecoration);
        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.spacing);
        //rv_products_list.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
        //rv_products_list.setHasFixedSize(true);

        adapter_products = new Adapter_products(getActivity(), productsList, adsList, rv_products_list);
        rv_products_list.setAdapter(adapter_products);


        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                switch (adapter_products.getItemViewType(position)) {
                    case 0:
                        return 2;
                    case 1:
                        return 1;
                    case 2:
                        return 2;

                    default:
                        return 2;
                }
            }
        });

        category_filter_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /*if (ToolsUtils.isNetworkAvailable()) {
                    CategoriesListDialog categoriesListDialog = new CategoriesListDialog((AppCompatActivity) getActivity());
                    categoriesListDialog.setSelectCategory(new CategoriesListDialog.SelectedCategory() {
                        @Override
                        public void getSelectedCategory(Categories.CategoriesBean SelectedCategory) {
                            category_filter.setText(SelectedCategory.getDisplay_title());
                            SelectCategoryId = SelectedCategory.getId();
                            Log.d("Err", "current price" + SelectedPriceId);
                            SortProducts();
                        }
                    });
                    categoriesListDialog.show();
                } else {
                    Toast.makeText(getActivity(), R.string.err_msg_no_internet_connection, Toast.LENGTH_SHORT).show();

                }*/

                if (categories != null) {
                    CategoriesListDialog categoriesListDialog = new CategoriesListDialog((AppCompatActivity) getActivity());
                    categoriesListDialog.setCategories(categories);
                    categoriesListDialog.setSelectCategory(new CategoriesListDialog.SelectedCategory() {
                        @Override
                        public void getSelectedCategory(Categories.CategoriesBean SelectedCategory) {
                            category_filter.setText(SelectedCategory.getDisplay_title());
                            SelectCategoryId = SelectedCategory.getId();
                            Log.d("Err", "current price" + SelectedPriceId);
                            SortProducts();
                        }
                    });
                    categoriesListDialog.show();

                } else {
                    if (getActivity() != null) {

                        Toast.makeText(getActivity(), R.string.swipe_to_refresh, Toast.LENGTH_LONG).show();
                    }
                }

            }
        });

        price_filter_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


               /* if (ToolsUtils.isNetworkAvailable()) {
                    PriceListDialog priceListDialog = new PriceListDialog((AppCompatActivity) getActivity());
                    priceListDialog.setSelectPrice(new PriceListDialog.SelectedPrice() {
                        @Override
                        public void getSelectedPrice(Prices.PricesBean SelectedPrice) {
                            price_filter.setText(SelectedPrice.getPriceFrom() + " - " + SelectedPrice.getPriceTo());
                            SelectedPriceId = SelectedPrice.getId();
                            SortProducts();
                        }
                    });
                    priceListDialog.show();

                } else {
                    Toast.makeText(getActivity(), R.string.err_msg_no_internet_connection, Toast.LENGTH_SHORT).show();
                }
                */


                if (prices != null) {
                    if (getActivity() == null) {
                        return;
                    }
                    PriceListDialog priceListDialog = new PriceListDialog((AppCompatActivity) getActivity());
                    priceListDialog.setPrices(prices);
                    priceListDialog.setSelectPrice(new PriceListDialog.SelectedPrice() {
                        @Override
                        public void getSelectedPrice(Prices.PricesBean SelectedPrice) {
                            price_filter.setText(SelectedPrice.getPriceFrom() + " - " + SelectedPrice.getPriceTo());
                            SelectedPriceId = SelectedPrice.getId();
                            SortProducts();
                        }
                    });
                    priceListDialog.show();

                } else {
                    if (getActivity() != null) {

                        Toast.makeText(getActivity(), R.string.swipe_to_refresh, Toast.LENGTH_LONG).show();
                    }
                }
            }
        });


        color_filter_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* if (ToolsUtils.isNetworkAvailable()) {
                    ColorsListDialog colorsListDialog = new ColorsListDialog((AppCompatActivity) getActivity());
                    colorsListDialog.setSelectColor(new ColorsListDialog.SelectedColor() {
                        @Override
                        public void getSelectedColor(Colors.ColorsBean SelectedColor) {
                            color_filter.setText(SelectedColor.getDisplay_title());
                            SelectedColorId = SelectedColor.getId();
                            SortProducts();
                        }
                    });

                    colorsListDialog.show();
                } else {
                    Toast.makeText(getActivity(), R.string.err_msg_no_internet_connection, Toast.LENGTH_SHORT).show();

                }*/


                if (colors != null) {
                    ColorsListDialog colorsListDialog = new ColorsListDialog((AppCompatActivity) getActivity());
                    colorsListDialog.setColors(colors);
                    colorsListDialog.setSelectColor(new ColorsListDialog.SelectedColor() {
                        @Override
                        public void getSelectedColor(Colors.ColorsBean SelectedColor) {
                            color_filter.setText(SelectedColor.getDisplay_title());
                            SelectedColorId = SelectedColor.getId();
                            SortProducts();
                        }
                    });

                    colorsListDialog.show();
                } else {

                    Toast.makeText(getActivity(), R.string.swipe_to_refresh, Toast.LENGTH_LONG).show();

                }
            }
        });

        adapter_products.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                System.out.println("ll_add_to_like");
                if (view.getId() == R.id.ll_add_to_like) {
                    System.out.println("ll_add_to_like2");
                    if (productsList.get(position).getIs_liked().equalsIgnoreCase("0")) {
                        LikeProductRequest(position);
                    } else {
                        unLikeProductRequest(position);
                    }

                } else if (view.getId() == R.id.iv_add_to_cart) {

                    addToCart(position);

                } else {

                    Intent i = new Intent(getActivity(), ProductDetailsActivity.class);
                    i.putExtra("product", productsList.get(position));
                    i.putExtra("pos", position);
                    startActivity(i);
                }
            }
        });


        adapter_products.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {

                System.out.println("Current Page : " + products.getProducts().getCurrent_page());
                System.out.println("Next Page : " + products.getProducts().getNext_page_url());
                if (products.getProducts().getNext_page_url() != null) {
                    if ((SelectCategoryId & SelectedColorId & SelectedPriceId) == -1) {
                        LoadMoreWithoutSort();
                    } else {
                        LoadMoreWithSort();
                    }
                }
            }
        });


        RefreshDataRequest();
        getActivity().registerReceiver(broad, new IntentFilter(QuickstartPreferences.ITEM_CHANGED_FILTER));
    }

    public void addToCart(int position) {
        Products.ProductsBean.DataBean product = productsList.get(position);

        try {

            boolean result = ToolsUtils.checkProductInCart(product);

            if (result) {
                Toast.makeText(getActivity(), R.string.err_msg_product_already_in_cart, Toast.LENGTH_SHORT).show();
            } else {
                product.setInCart(true);
                if (product.getCategory_id() != null) {
                    product.setCat_Json(new Gson().toJson(product.getCategory_id()));
                }
                product.save();
                if (listener != null) {
                    listener.onCartChanged();
                }
                Toast.makeText(getActivity(), R.string.item_added_to_cart, Toast.LENGTH_SHORT).show();

            }


        } catch (Exception e) {
            e.printStackTrace();
        }


        /*
        long ProductId = -1 ;
        try {
            List<Products.ProductsBean.DataBean> list = Products.ProductsBean.DataBean.listAll(Products.ProductsBean.DataBean.class);

            if (list == null) {
                list = new ArrayList<>();
            }
             for (Products.ProductsBean.DataBean item : list) {
                if (item.getProduct_id().equalsIgnoreCase(product.getProduct_id())) {
                    Log.d("Err", "ProductId : " + ProductId);
                    ProductId = item.getId();
                    break;
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        try {

            if (ProductId == -1) {
                if((product.getSelectedQuantity()+1) <= product.getQuantity()) {
                    product.setSelectedQuantity(1);
                    if (product.getCategory_id() != null) {
                        product.setCat_Json(new Gson().toJson(product.getCategory_id()));
                    }

                    product.save();
                    if(listener != null){
                        listener.onCartChanged();
                    }
                    Toast.makeText(getActivity(), R.string.item_added_to_cart, Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(getActivity(), R.string.err_msg_add_quantity, Toast.LENGTH_SHORT).show();
                }

            } else {

                Toast.makeText(getActivity(), R.string.err_msg_product_already_in_cart, Toast.LENGTH_SHORT).show();


            }


        } catch (Exception e) {
            e.printStackTrace();
        }
*/

    }


    private void RefreshDataRequest() {


        if (ToolsUtils.isNetworkAvailable()) {

            requestGetCategories();
            requestGetPrices();
            requestGetColors();
            getProductsRequest();
        } else {
            swipe.setRefreshing(false);
            Toast.makeText(getActivity(), R.string.err_msg_no_internet_connection, Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    public void onDestroy() {
        getActivity().unregisterReceiver(broad);
        super.onDestroy();
    }

    private void LoadMoreWithoutSort() {
        if (ToolsUtils.isNetworkAvailable()) {
            pageId = products.getProducts().getCurrent_page() + 1;
            System.out.println("Current_page" + products.getProducts().getCurrent_page());
            System.out.println("pageId" + pageId);

            productsList.add(null);
            adapter_products.notifyItemInserted(productsList.size() - 1);

            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            Call<Products> call = apiService.getProducts(pageId);
            call.enqueue(new Callback<Products>() {
                @Override
                public void onResponse(Call<Products> call, Response<Products> response) {
                    System.out.println("Code" + response.code());
                    System.out.println("raw" + response.raw());
                    System.out.println("Code" + response.message());

                    rv_products_list.post(new Runnable() {
                        public void run() {
                            productsList.remove(productsList.size() - 1);
                            adapter_products.notifyItemRemoved(productsList.size());
                            adapter_products.setLoaded();
                        }
                    });


                    if (response.code() == 200) {
                        products = response.body();
                        if (products.getProducts().getData().size() > 0) {
                            rv_products_list.post(new Runnable() {
                                public void run() {
                                    productsList.addAll(products.getProducts().getData());
                                    adapter_products.notifyDataSetChanged();
                                }
                            });
                        }
                    } else {
                        Toast.makeText(getActivity(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<Products> call, Throwable t) {
                    rv_products_list.post(new Runnable() {
                        public void run() {
                            productsList.remove(productsList.size() - 1);
                            adapter_products.notifyItemRemoved(productsList.size());
                            adapter_products.setLoaded();
                        }
                    });
                    Toast.makeText(getActivity(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(getActivity(), R.string.err_msg_no_internet_connection, Toast.LENGTH_SHORT).show();
        }
    }

    private void LoadMoreWithSort() {
        if (ToolsUtils.isNetworkAvailable()) {
            pageSortId = products.getProducts().getCurrent_page() + 1;
            rv_products_list.post(new Runnable() {
                public void run() {
                    productsList.add(null);
                    adapter_products.notifyItemInserted(productsList.size() - 1);
                }
            });

            final AVLoadingIndicatorDialog dialog = ToolsUtils.CreateDialog((AppCompatActivity) getActivity(), null);
            dialog.show();
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            Map<String, RequestBody> map = new HashMap<>();
            if (SelectCategoryId != -1) {
                RequestBody CategoryId = RequestBody.create(MediaType.parse("text/plain"), "" + SelectCategoryId);
                map.put("category", CategoryId);
            }
            if (SelectedColorId != -1) {
                RequestBody ColorId = RequestBody.create(MediaType.parse("text/plain"), "" + SelectedColorId);
                map.put("color", ColorId);
            }
            if (SelectedPriceId != -1) {
                RequestBody PriceId = RequestBody.create(MediaType.parse("text/plain"), "" + SelectedPriceId);
                map.put("price", PriceId);
            }
            Call<Products> call = apiService.SortProducts(map, pageSortId);
            call.enqueue(new Callback<Products>() {
                @Override
                public void onResponse(Call<Products> call, Response<Products> response) {
                    System.out.println("Code" + response.code());
                    System.out.println("raw" + response.raw());
                    System.out.println("Code" + response.message());


                    rv_products_list.post(new Runnable() {
                        public void run() {
                            productsList.remove(productsList.size() - 1);
                            adapter_products.notifyItemRemoved(productsList.size());

                        }
                    });

                    adapter_products.setLoaded();
                    dialog.hide();
                    if (response.code() == 200) {
                        products = response.body();
                        if (products.getProducts().getData().size() > 0) {
                            productsList.addAll(products.getProducts().getData());
                            adapter_products.notifyDataSetChanged();
                        }
                    } else {
                        Toast.makeText(getActivity(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<Products> call, Throwable t) {
                    rv_products_list.post(new Runnable() {
                        public void run() {
                            productsList.remove(productsList.size() - 1);
                            adapter_products.notifyItemRemoved(productsList.size());

                        }
                    });
                    adapter_products.setLoaded();

                    dialog.hide();
                    Toast.makeText(getActivity(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();

                }
            });

        } else {
            Toast.makeText(getActivity(), R.string.err_msg_no_internet_connection, Toast.LENGTH_SHORT).show();
        }

    }

    private void SortProducts() {
        if (ToolsUtils.isNetworkAvailable()) {

            swipe.setRefreshing(true);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            Map<String, RequestBody> map = new HashMap<>();
            if (SelectCategoryId != -1) {
                RequestBody CategoryId = RequestBody.create(MediaType.parse("text/plain"), "" + SelectCategoryId);
                map.put("category", CategoryId);
            }
            if (SelectedColorId != -1) {
                RequestBody ColorId = RequestBody.create(MediaType.parse("text/plain"), "" + SelectedColorId);
                map.put("color", ColorId);
            }
            if (SelectedPriceId != -1) {
                RequestBody PriceId = RequestBody.create(MediaType.parse("text/plain"), "" + SelectedPriceId);
                map.put("price", PriceId);
            }
            Call<Products> call = apiService.SortProducts(map, pageSortId);
            call.enqueue(new Callback<Products>() {
                @Override
                public void onResponse(Call<Products> call, Response<Products> response) {
                    System.out.println("Code" + response.code());
                    System.out.println("raw" + response.raw());
                    System.out.println("Code" + response.message());
                    swipe.setRefreshing(false);
                    if (response.code() == 200) {
                        products = response.body();
                        if (products.getProducts().getData().size() > 0) {
                            productsList.clear();
                            rv_products_list.post(new Runnable() {
                                public void run() {
                                    productsList.addAll(products.getProducts().getData());
                                    adapter_products.notifyDataSetChanged();
                                }
                            });


                        } else {

                            rv_products_list.post(new Runnable() {
                                public void run() {
                                    productsList.clear();
                                    adapter_products.notifyDataSetChanged();
                                }
                            });
                            Toast.makeText(getActivity(), R.string.err_products_empty, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<Products> call, Throwable t) {
                    swipe.setRefreshing(false);
                    Toast.makeText(getActivity(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(getActivity(), R.string.err_msg_no_internet_connection, Toast.LENGTH_SHORT).show();
        }

    }

    private void getProductsRequest() {
        if (ToolsUtils.isNetworkAvailable()) {

            swipe.setRefreshing(true);

            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            Call<Products> call = apiService.getProducts(pageId);
            call.enqueue(new Callback<Products>() {
                @Override
                public void onResponse(Call<Products> call, Response<Products> response) {
                    System.out.println("Code" + response.code());
                    System.out.println("raw" + response.raw());
                    System.out.println("Code" + response.message());
                    swipe.setRefreshing(false);
                    if (response.code() == 200) {
                        products = response.body();
                        if (products.getProducts().getData().size() > 0) {
                            rv_products_list.post(new Runnable() {
                                public void run() {
                                    adsList.clear();
                                    productsList.clear();
                                    productsList.addAll(products.getProducts().getData());
                                    adsList.addAll(products.getAds());
                                    adapter_products.notifyDataSetChanged();
                                }
                            });

                        } else {
                            Toast.makeText(getActivity(), R.string.err_products_empty, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<Products> call, Throwable t) {
                    swipe.setRefreshing(false);
                    Toast.makeText(getActivity(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            swipe.setRefreshing(false);

            if (getActivity() != null) {
                Toast.makeText(getActivity(), R.string.err_msg_no_internet_connection, Toast.LENGTH_SHORT).show();

            }
        }
    }

    private void LikeProductRequest(final int pos) {


        final Products.ProductsBean.DataBean item = productsList.get(pos);

        if (ToolsUtils.isNetworkAvailable()) {
            final AVLoadingIndicatorDialog dialog = ToolsUtils.CreateDialog((AppCompatActivity) getActivity(), null);
            dialog.show();
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            Call<RequestResponse> call = apiService.likeProduct(item.getProduct_id());
            call.enqueue(new Callback<RequestResponse>() {
                @Override
                public void onResponse(Call<RequestResponse> call, Response<RequestResponse> response) {
                    System.out.println("Code" + response.code());
                    System.out.println("raw" + response.raw());
                    System.out.println("Code" + response.message());
                    dialog.hide();
                    if (response.code() == 200) {
                        if (response.body().isStatus()) {
                            Toast.makeText(getActivity(), "" + response.body().getDisplay_message(), Toast.LENGTH_SHORT).show();
                            item.setIs_liked("1");
                            item.setLikes("" + ((Integer.parseInt(item.getLikes())) + 1));
                            if (item.getCategory_id() != null) {
                                item.setCat_Json(new Gson().toJson(item.getCategory_id()));
                            }
                            item.save();
                            adapter_products.notifyItemChanged(pos);
                        } else {
                            if (getActivity() != null) {

                                Toast.makeText(getActivity(), "" + response.body().getDisplay_message(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        if (getActivity() != null) {
                            Toast.makeText(getActivity(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<RequestResponse> call, Throwable t) {
                    dialog.hide();
                    if (getActivity() != null) {

                        Toast.makeText(getActivity(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();
                    }
                }
            });

        } else {
            if (getActivity() != null) {
                Toast.makeText(getActivity(), R.string.err_msg_no_internet_connection, Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void unLikeProductRequest(final int pos) {

        final Products.ProductsBean.DataBean item = productsList.get(pos);

        if (ToolsUtils.isNetworkAvailable()) {
            final AVLoadingIndicatorDialog dialog = ToolsUtils.CreateDialog((AppCompatActivity) getActivity(), null);
            dialog.show();
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            Call<RequestResponse> call = apiService.unLikeProduct(item.getProduct_id());
            call.enqueue(new Callback<RequestResponse>() {
                @Override
                public void onResponse(Call<RequestResponse> call, Response<RequestResponse> response) {
                    System.out.println("Code" + response.code());
                    System.out.println("raw" + response.raw());
                    System.out.println("Code" + response.message());
                    dialog.hide();
                    if (response.code() == 200) {
                        if (response.body().isStatus()) {
                            if (getActivity() != null) {
                                Toast.makeText(getActivity(), "" + response.body().getDisplay_message(), Toast.LENGTH_SHORT).show();
                            }
                            // change isLiked for you
                            item.setIs_liked("0");
                            item.setLikes("" + ((Integer.parseInt(item.getLikes())) - 1));
                            item.save();
                            adapter_products.notifyItemChanged(pos);
                        } else {
                            if (getActivity() != null) {

                                Toast.makeText(getActivity(), "" + response.body().getDisplay_message(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        if (getActivity() != null) {

                            Toast.makeText(getActivity(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<RequestResponse> call, Throwable t) {
                    dialog.hide();
                    if (getActivity() != null) {

                        Toast.makeText(getActivity(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();
                    }
                }
            });

        } else {
            if (getActivity() != null) {

                Toast.makeText(getActivity(), R.string.err_msg_no_internet_connection, Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void requestGetCategories() {

        if (ToolsUtils.isNetworkAvailable()) {
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            Call<Categories> call = apiService.getCategories();
            call.enqueue(new Callback<Categories>() {
                @Override
                public void onResponse(Call<Categories> call, Response<Categories> response) {

                    System.out.println("Code" + response.code());
                    System.out.println("raw" + response.raw());
                    System.out.println("Code" + response.message());


                    if (response.code() == 200 && response.body().isStatus()) {
                        categories = response.body();
                    }
                }

                @Override
                public void onFailure(Call<Categories> call, Throwable t) {
                    t.printStackTrace();
                }
            });

        } else {
            if (getActivity() != null) {

                Toast.makeText(getActivity(), R.string.err_msg_no_internet_connection, Toast.LENGTH_SHORT).show();
            }
        }


    }

    private void requestGetPrices() {
        if (ToolsUtils.isNetworkAvailable()) {


            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            Call<Prices> call = apiService.getPrices();
            call.enqueue(new Callback<Prices>() {
                @Override
                public void onResponse(Call<Prices> call, Response<Prices> response) {

                    System.out.println("Code" + response.code());
                    System.out.println("raw" + response.raw());
                    System.out.println("Code" + response.message());

                    if (response.code() == 200 && response.body().isStatus()) {
                        prices = response.body();
                    }
                }

                @Override
                public void onFailure(Call<Prices> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        } else {
            if (getActivity() != null) {

                Toast.makeText(getActivity(), R.string.err_msg_no_internet_connection, Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void requestGetColors() {

        if (ToolsUtils.isNetworkAvailable()) {
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            Call<Colors> call = apiService.getColors();
            call.enqueue(new Callback<Colors>() {
                @Override
                public void onResponse(Call<Colors> call, Response<Colors> response) {
                    System.out.println("Code" + response.code());
                    System.out.println("raw" + response.raw());
                    System.out.println("Code" + response.message());
                    if (response.code() == 200 && response.body().isStatus()) {
                        colors = response.body();
                    }
                }

                @Override
                public void onFailure(Call<Colors> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        } else {
            if (getActivity() != null) {

                Toast.makeText(getActivity(), R.string.err_msg_no_internet_connection, Toast.LENGTH_SHORT).show();
            }
        }
    }


    @Override
    public void onRefresh() {

        SelectCategoryId = -1;
        SelectedColorId = -1;
        SelectedPriceId = -1;
        pageId = 1;
        pageSortId = 1;
        category_filter.setText(R.string.all_category);
        price_filter.setText(R.string.price);
        color_filter.setText(R.string.color_filter);
        adapter_products.setLoaded();
        RefreshDataRequest();
    }
}
