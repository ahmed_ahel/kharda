package com.khorda.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.khorda.Adapter.Adapter_walkthrough;
import com.khorda.Adapter.MyPurchasesAdapter;
import com.khorda.CustomViews.AVLoadingIndicatorDialog;
import com.khorda.Models.MyPurchases;
import com.khorda.Models.WalkThrough;
import com.khorda.R;
import com.khorda.Utils.ToolsUtils;
import com.khorda.WalkthroughActivity;
import com.khorda.interfaces.ApiClient;
import com.khorda.interfaces.ApiInterface;
import com.khorda.interfaces.OnItemClickListener;
import com.khorda.interfaces.OnLoadMoreListener;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyPurchasesFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, OnItemClickListener {

    private TextView textViewOrdersCounter;
    private SwipeRefreshLayout swipeRefreshLayoutPurchases;
    private RecyclerView recyclerViewPurchases;
    private MyPurchasesAdapter myPurchasesAdapter;
    private List<MyPurchases.PurchasesBean.DataBean> mlist;
    private MyPurchases myPurchases;
    private int pageId = 1;


    public MyPurchasesFragment() {
    }

    public static MyPurchasesFragment newInstance() {
        MyPurchasesFragment fragment = new MyPurchasesFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_my_purchases, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mlist = new ArrayList<>();

        textViewOrdersCounter = (TextView) view.findViewById(R.id.textViewOrdersCounter);
        swipeRefreshLayoutPurchases = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayoutPurchases);
        swipeRefreshLayoutPurchases.setColorSchemeResources(R.color.colorPrimary);
        swipeRefreshLayoutPurchases.setOnRefreshListener(this);

        recyclerViewPurchases = (RecyclerView) view.findViewById(R.id.recyclerViewPurchases);
        recyclerViewPurchases.setLayoutManager(new LinearLayoutManager(getActivity()));

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerViewPurchases.getContext(),
                DividerItemDecoration.VERTICAL);
        recyclerViewPurchases.addItemDecoration(dividerItemDecoration);

        myPurchasesAdapter = new MyPurchasesAdapter(getActivity(), mlist, recyclerViewPurchases);
        myPurchasesAdapter.setOnItemClickListener(this);
        myPurchasesAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {

                if (myPurchases.getPurchases().getNext_page_url() != null) {
                    if (ToolsUtils.isNetworkAvailable()) {
                        pageId = myPurchases.getPurchases().getCurrent_page() + 1;
                        recyclerViewPurchases.post(new Runnable() {
                            public void run() {
                                mlist.add(null);
                                myPurchasesAdapter.notifyItemInserted(mlist.size() - 1);
                            }
                        });

                        ApiInterface apiService =
                                ApiClient.getClient().create(ApiInterface.class);
                        Call<MyPurchases> call = apiService.getMyPurchases(pageId);
                        call.enqueue(new Callback<MyPurchases>() {
                            @Override
                            public void onResponse(Call<MyPurchases> call, final Response<MyPurchases> response) {
                                System.out.println("Code" + response.code());
                                System.out.println("raw" + response.raw());
                                System.out.println("Code" + response.message());

                                recyclerViewPurchases.post(new Runnable() {
                                    public void run() {
                                        mlist.remove(mlist.size() - 1);
                                        myPurchasesAdapter.notifyItemRemoved(mlist.size());
                                        myPurchasesAdapter.setLoaded();
                                    }
                                });

                                if (response.code() == 200 && response.body().isStatus()) {
                                    myPurchases = response.body();
                                    if (response.body().getPurchases().getData().size() > 0) {

                                        recyclerViewPurchases.post(new Runnable() {
                                            public void run() {
                                                mlist.addAll(response.body().getPurchases().getData());
                                                myPurchasesAdapter.notifyDataSetChanged();
                                            }
                                        });
                                        calculateTotalPrice();
                                    }
                                } else {
                                    Toast.makeText(getActivity(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<MyPurchases> call, Throwable t) {
                                recyclerViewPurchases.post(new Runnable() {
                                    public void run() {
                                        mlist.remove(mlist.size() - 1);
                                        myPurchasesAdapter.notifyItemRemoved(mlist.size());
                                        myPurchasesAdapter.setLoaded();
                                    }
                                });
                                Toast.makeText(getActivity(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();
                            }
                        });
                    } else {
                        Toast.makeText(getActivity(), R.string.err_msg_no_internet_connection, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        recyclerViewPurchases.setAdapter(myPurchasesAdapter);

        RequestMyPurchases();
    }

    private void calculateTotalPrice() {
        float total   = 0 ;
        System.out.println("mlist size"+mlist.size());

        for (MyPurchases.PurchasesBean.DataBean item : mlist){

            if(item.getProduct()!= null){
                total += item.getProduct().getPrice() * item.getProduct().getQuantity();
            }
         }

        textViewOrdersCounter.setText("" + total);

    }

    private void RequestMyPurchases() {

        if (ToolsUtils.isNetworkAvailable()) {
            swipeRefreshLayoutPurchases.setRefreshing(true);

            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            Call<MyPurchases> call = apiService.getMyPurchases(pageId);
            call.enqueue(new Callback<MyPurchases>() {
                @Override
                public void onResponse(Call<MyPurchases> call, Response<MyPurchases> response) {
                    System.out.println("Code" + response.code());
                    System.out.println("raw" + response.raw());
                    System.out.println("Code" + response.message());
                    swipeRefreshLayoutPurchases.setRefreshing(false);
                    if (response.code() == 200 && response.body().isStatus()) {

                        myPurchases = response.body();
                        if (response.body().getPurchases().getData().size() > 0) {
                            mlist.clear();
                            mlist.addAll(response.body().getPurchases().getData());
                            myPurchasesAdapter.notifyDataSetChanged();
                            calculateTotalPrice();
                        } else {
                            Toast.makeText(getActivity(), R.string.err_products_empty, Toast.LENGTH_SHORT).show();
                            mlist.clear();
                            myPurchasesAdapter.notifyDataSetChanged();
                        }
                    } else {
                        Toast.makeText(getActivity(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();
                    }
                }
                @Override
                public void onFailure(Call<MyPurchases> call, Throwable t) {
                    swipeRefreshLayoutPurchases.setRefreshing(false);
                    Toast.makeText(getActivity(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            swipeRefreshLayoutPurchases.setRefreshing(false);
            Toast.makeText(getActivity(), R.string.err_msg_no_internet_connection, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRefresh() {
         pageId = 1;
        myPurchasesAdapter.setLoaded();
        RequestMyPurchases();
    }

    @Override
    public void onItemClick(View itemView, int position) {

    }
}
