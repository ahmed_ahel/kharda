package com.khorda.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.khorda.Adapter.Adapter_liked_products;
import com.khorda.CustomViews.AVLoadingIndicatorDialog;
import com.khorda.Models.Products;
import com.khorda.Models.RequestResponse;
import com.khorda.R;
import com.khorda.Utils.ToolsUtils;
import com.khorda.interfaces.ApiClient;
import com.khorda.interfaces.ApiInterface;
import com.khorda.interfaces.OnItemClickListener;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class LikedProductsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {


    private Adapter_liked_products adapter_liked_products;
    private List<Products.ProductsBean.DataBean> productsList;
    private RecyclerView rv_liked_products;
    private SwipeRefreshLayout swipe;

    public static LikedProductsFragment newInstance() {
        LikedProductsFragment fragment = new LikedProductsFragment();
        return fragment;
    }


    public LikedProductsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_liked_products, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        rv_liked_products = (RecyclerView) getView().findViewById(R.id.rv_liked_products);
        swipe = (SwipeRefreshLayout) getView().findViewById(R.id.swipeRefreshLayout);
        swipe.setOnRefreshListener(this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rv_liked_products.setLayoutManager(layoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rv_liked_products.getContext(),
                layoutManager.getOrientation());
        rv_liked_products.addItemDecoration(dividerItemDecoration);

        productsList = new ArrayList<>();

        productsList.addAll(ToolsUtils.getAllLikedProducts());

        System.out.println("productsList" + productsList.size());
        adapter_liked_products = new Adapter_liked_products(getActivity(), productsList);
        rv_liked_products.setAdapter(adapter_liked_products);

        if (productsList.size() == 0) {
            Toast.makeText(getActivity(), R.string.err_products_empty, Toast.LENGTH_LONG).show();
        }


        adapter_liked_products.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int position) {

                if (itemView.getId() == R.id.iv_unlike) {
                    unLikeProductRequest(position);
                }
            }
        });
    }


    private void unLikeProductRequest(final int pos) {

        final Products.ProductsBean.DataBean item = productsList.get(pos);

        if (ToolsUtils.isNetworkAvailable()) {
            final AVLoadingIndicatorDialog dialog = ToolsUtils.CreateDialog((AppCompatActivity) getActivity(), null);
            dialog.show();
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            Call<RequestResponse> call = apiService.unLikeProduct(item.getProduct_id());
            call.enqueue(new Callback<RequestResponse>() {
                @Override
                public void onResponse(Call<RequestResponse> call, Response<RequestResponse> response) {
                    System.out.println("Code" + response.code());
                    System.out.println("raw" + response.raw());
                    System.out.println("Code" + response.message());
                    dialog.hide();
                    if (response.code() == 200) {
                        if (response.body().isStatus()) {
                            if (getActivity() != null) {
                                Toast.makeText(getActivity(), "" + response.body().getDisplay_message(), Toast.LENGTH_SHORT).show();
                            }
                            // change isLiked for you
                            item.setIs_liked("0");
                            item.setLikes("" + ((Integer.parseInt(item.getLikes())) - 1));
                            item.save();
                            productsList.remove(pos);
                            adapter_liked_products.notifyItemRemoved(pos);
                        } else {
                            if (getActivity() != null) {

                                Toast.makeText(getActivity(), "" + response.body().getDisplay_message(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        if (getActivity() != null) {

                            Toast.makeText(getActivity(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<RequestResponse> call, Throwable t) {
                    dialog.hide();
                    if (getActivity() != null) {

                        Toast.makeText(getActivity(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();
                    }
                }
            });

        } else {
            if (getActivity() != null) {

                Toast.makeText(getActivity(), R.string.err_msg_no_internet_connection, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onRefresh() {

        swipe.setRefreshing(false);
        productsList.clear();
        productsList.addAll(ToolsUtils.getAllLikedProducts());
        adapter_liked_products.notifyDataSetChanged();
    }
}
