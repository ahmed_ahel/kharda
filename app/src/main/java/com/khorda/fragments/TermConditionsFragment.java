package com.khorda.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Toast;

import com.khorda.CustomViews.AVLoadingIndicatorDialog;
import com.khorda.Models.Countries;
import com.khorda.Models.TermsConditions;
import com.khorda.R;
import com.khorda.Utils.ToolsUtils;
import com.khorda.interfaces.ApiClient;
import com.khorda.interfaces.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ez on 8/18/2017.
 */

public class TermConditionsFragment extends Fragment {


    private WebView web_view;

    public TermConditionsFragment() {
    }

    public static TermConditionsFragment newInstance() {
        TermConditionsFragment fragment = new TermConditionsFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_term_conditions, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        web_view = (WebView) getView().findViewById(R.id.web_view);


        if (ToolsUtils.isNetworkAvailable()) {

            final AVLoadingIndicatorDialog dialog = ToolsUtils.CreateDialog((AppCompatActivity) getActivity(), null);
            dialog.show();


            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<TermsConditions> call = apiService.getTermsConditions();

            call.enqueue(new Callback<TermsConditions>() {
                @Override
                public void onResponse(Call<TermsConditions> call, Response<TermsConditions> response) {

                    System.out.println("Code" + response.code());
                    System.out.println("raw" + response.raw());
                    System.out.println("Code" + response.message());
                    dialog.hide();
                    if (response.code() == 200 && response.body().isStatus()) {
                        ToolsUtils.setupWebView(web_view);
                        web_view.loadData(response.body().getTerms_conditions(), "text/html; charset=UTF-8", null);
                    } else {
                        Toast.makeText(getActivity(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<TermsConditions> call, Throwable t) {
                    dialog.hide();
                    Toast.makeText(getActivity(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();

                }
            });

        } else {

            Toast.makeText(getActivity(), R.string.err_msg_no_internet_connection, Toast.LENGTH_SHORT).show();

        }

    }

}
