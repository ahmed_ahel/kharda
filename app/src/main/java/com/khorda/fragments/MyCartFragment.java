package com.khorda.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.khorda.Adapter.Adapter_shopping_list;
import com.khorda.CustomViews.Button;
import com.khorda.CustomViews.TextView;
import com.khorda.DeliveryActivity;
import com.khorda.HomeActivity;
import com.khorda.Models.Products;
import com.khorda.R;
import com.khorda.Utils.QuickstartPreferences;
import com.khorda.Utils.ToolsUtils;
import com.khorda.interfaces.OnItemClickListener;
import com.khorda.interfaces.OnNotifyCartItemsChanged;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ez on 8/18/2017.
 */

public class MyCartFragment extends Fragment {


    private RecyclerView rv_shopping_list;
    private List<Products.ProductsBean.DataBean> shoppingItemList;
    private Adapter_shopping_list adapter_shopping_list;
    private Button check_out_btn;
    private TextView textViewOrdersCounter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private OnNotifyCartItemsChanged listener;
    private AppCompatImageView header_image;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (OnNotifyCartItemsChanged) context;
    }

    public MyCartFragment() {

    }

    public static MyCartFragment newInstance() {
        MyCartFragment fragment = new MyCartFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_my_cart, container, false);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        header_image = (AppCompatImageView) getView().findViewById(R.id.image);
        textViewOrdersCounter = (TextView) getView().findViewById(R.id.textViewOrdersCounter);
        swipeRefreshLayout = (SwipeRefreshLayout) getView().findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setEnabled(false);
        check_out_btn = (Button) getView().findViewById(R.id.check_out_btn);
        rv_shopping_list = (RecyclerView) getView().findViewById(R.id.shopping_list);

        if (ToolsUtils.getDeviceLang().equalsIgnoreCase(QuickstartPreferences.AR_LAN)) {
            header_image.setImageResource(R.drawable.rtl_ic_shopping_cart);
        } else {
            header_image.setImageResource(R.drawable.ic_shopping_cart);
        }


        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rv_shopping_list.setLayoutManager(layoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rv_shopping_list.getContext(),
                layoutManager.getOrientation());
        rv_shopping_list.addItemDecoration(dividerItemDecoration);

        shoppingItemList = new ArrayList<>();
        adapter_shopping_list = new Adapter_shopping_list(getActivity(), shoppingItemList);

        rv_shopping_list.setAdapter(adapter_shopping_list);

        getView().findViewById(R.id.shop_now_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    ((HomeActivity) getActivity()).Navigate(R.id.nav_home);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        adapter_shopping_list.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                switch (view.getId()) {
                    case R.id.trash:

                        showConfirmDeleteItem(position);
                        break;
                    case R.id.increase_product:

                        if ((shoppingItemList.get(position).getSelectedQuantity() + 1) <= shoppingItemList.get(position).getQuantity()) {

                            int q = shoppingItemList.get(position).getSelectedQuantity();
                            q++;
                            shoppingItemList.get(position).setSelectedQuantity(q);
                            adapter_shopping_list.notifyItemChanged(position);
                            UpdateCartList(shoppingItemList.get(position).getId(), shoppingItemList.get(position).getSelectedQuantity());
                            CalculateTotalPrice();
                        } else {

                            Toast.makeText(getActivity(), R.string.err_msg_add_quantity, Toast.LENGTH_SHORT).show();

                        }
                        break;

                    case R.id.decrease_product:
                        int qq = shoppingItemList.get(position).getSelectedQuantity();
                        if (qq < 2) {
                            showConfirmDeleteItem(position);
                            return;
                        }
                        qq--;
                        shoppingItemList.get(position).setSelectedQuantity(qq);
                        adapter_shopping_list.notifyItemChanged(position);
                        UpdateCartList(shoppingItemList.get(position).getId(), shoppingItemList.get(position).getSelectedQuantity());
                        CalculateTotalPrice();
                        break;
                }

            }
        });


        check_out_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), DeliveryActivity.class));
            }
        });

        getAllCartItems();
    }

    private void UpdateCartList(long id, int value) {

        Products.ProductsBean.DataBean dataBean = Products.ProductsBean.DataBean.findById(Products.ProductsBean.DataBean.class, id);
        dataBean.setSelectedQuantity(value);
        dataBean.save();
    }

    private void showConfirmDeleteItem(final int position) {
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity(), R.style.MyAlertDialogStyle)
                .setMessage(R.string.msg_delete_item)
                .setCancelable(true)
                .setPositiveButton(R.string.yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();

                                Products.ProductsBean.DataBean product = Products.ProductsBean.DataBean.findById(Products.ProductsBean.DataBean.class, shoppingItemList.get(position).getId());
                                product.delete();

                                shoppingItemList.remove(position);
                                adapter_shopping_list.notifyItemRemoved(position);
                                CalculateTotalPrice();
                                if (shoppingItemList.size() == 0) {
                                    getView().findViewById(R.id.shopping_list_layout).setVisibility(View.GONE);
                                    getView().findViewById(R.id.empty_shopping_cart_layout).setVisibility(View.VISIBLE);
                                }

                                if (listener != null) {
                                    listener.onCartChanged();
                                }

                            }
                        }).setNegativeButton(R.string.cancel, null).show();
        //   Typeface face = Typeface.createFromAsset(getAssets(), "mohand_font.ttf");
        // Typeface face1 = Typeface.createFromAsset(getAssets(), "mohand_font.ttf");
        alertDialog.show();
        // alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTypeface(face1);
        //  alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTypeface(face1);

        //((android.widget.TextView) alertDialog.findViewById(android.R.id.message)).setTypeface(face);

    }


    private void getAllCartItems() {
        try {

            if (ToolsUtils.getAllCartProducts().size() > 0) {
                getView().findViewById(R.id.shopping_list_layout).setVisibility(View.VISIBLE);
                getView().findViewById(R.id.empty_shopping_cart_layout).setVisibility(View.GONE);

                shoppingItemList.clear();
                shoppingItemList.addAll(ToolsUtils.getAllCartProducts());
                adapter_shopping_list.notifyDataSetChanged();
                CalculateTotalPrice();

            } else {
                getView().findViewById(R.id.shopping_list_layout).setVisibility(View.GONE);
                getView().findViewById(R.id.empty_shopping_cart_layout).setVisibility(View.VISIBLE);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void CalculateTotalPrice() {
        float total = 0;

        for (Products.ProductsBean.DataBean item : ToolsUtils.getAllCartProducts()) {
            total += item.getPrice() * item.getSelectedQuantity();
        }
        textViewOrdersCounter.setText("" + total);


    }

}
