package com.khorda.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.khorda.CustomViews.AVLoadingIndicatorDialog;
import com.khorda.CustomViews.Button;
import com.khorda.CustomViews.EditText;
import com.khorda.Models.RequestResponse;
import com.khorda.R;
import com.khorda.Utils.ToolsUtils;
import com.khorda.interfaces.ApiClient;
import com.khorda.interfaces.ApiInterface;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by ahmed on 8/19/17.
 */

public class ChangePasswordFragment extends Fragment {

    private Button save_btn;
    private EditText old_password,
            new_password;


    public ChangePasswordFragment() {
    }

    public static ChangePasswordFragment newInstance() {
        ChangePasswordFragment fragment = new ChangePasswordFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_change_password, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        old_password = (EditText) getView().findViewById(R.id.old_password);
        new_password = (EditText) getView().findViewById(R.id.new_password);
        save_btn = (Button) getView().findViewById(R.id.save_btn);


        save_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (old_password.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), R.string.err_msg_old_password, Toast.LENGTH_SHORT).show();
                    return;
                }


                if (new_password.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), R.string.err_msg_new_password, Toast.LENGTH_SHORT).show();
                    return;
                }


                RequestChangePassword();


            }
        });

    }

    private void RequestChangePassword() {
        if (ToolsUtils.isNetworkAvailable()) {

            final AVLoadingIndicatorDialog dialog = ToolsUtils.CreateDialog((AppCompatActivity) getActivity(), null);
            dialog.show();
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);


            Call<RequestResponse> call = apiService.changePassword(old_password.getText().toString(), new_password.getText().toString());
            call.enqueue(new retrofit2.Callback<RequestResponse>() {
                @Override
                public void onResponse(Call<RequestResponse> call, Response<RequestResponse> response) {

                    System.out.println("Code" + response.code());
                    System.out.println("raw" + response.raw());
                    System.out.println("Code" + response.message());
                    dialog.hide();

                    if (response.code() == 200) {

                        if (response.body().isStatus()) {
                            new_password.getText().clear();
                            old_password.getText().clear();
                            Toast.makeText(getActivity(), response.body().getDisplay_message(), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), response.body().getDisplay_message(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<RequestResponse> call, Throwable t) {
                    t.printStackTrace();
                    dialog.hide();
                    System.out.println("onFailure");
                    Toast.makeText(getActivity(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();

                }
            });

        } else {
            Toast.makeText(getActivity(), R.string.err_msg_no_internet_connection, Toast.LENGTH_SHORT).show();
        }

    }
}
