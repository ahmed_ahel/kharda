package com.khorda;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.khorda.CustomViews.AVLoadingIndicatorDialog;
import com.khorda.CustomViews.Button;
import com.khorda.CustomViews.TextView;
import com.khorda.Models.Payment;
import com.khorda.Models.Products;
import com.khorda.Models.RequestResponse;
import com.khorda.Models.UserInfo;
import com.khorda.Utils.MyContextWrapper;
import com.khorda.Utils.PreferenceEditor;
import com.khorda.Utils.QuickstartPreferences;
import com.khorda.Utils.ToolsUtils;
import com.khorda.fragments.ChangePasswordFragment;
import com.khorda.fragments.LikedProductsFragment;
import com.khorda.fragments.MyCartFragment;
import com.khorda.fragments.MyProfileFragment;
import com.khorda.fragments.MyPurchasesFragment;
import com.khorda.fragments.PrivacyPolicyFragment;
import com.khorda.fragments.ProductsFragment;
import com.khorda.fragments.TermConditionsFragment;
import com.khorda.interfaces.ApiClient;
import com.khorda.interfaces.ApiInterface;
import com.khorda.interfaces.OnItemChangedListener;
import com.khorda.interfaces.OnNotifyCartItemsChanged;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnNotifyCartItemsChanged, OnItemChangedListener {

    private Toolbar toolbar;
    private NavigationView navigationView;
    private DrawerLayout drawer;
    private Fragment fragment = null;
    private int flag = 0;
    private TextView toolbar_title;
    private FrameLayout frameLayoutCart;
    private AppCompatTextView textViewCartCounter;
    private UserInfo UserData;
    private String lang;
    private AppCompatImageView iv_liked_products ;

    @Override
    protected void attachBaseContext(Context newBase) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String lang = PreferenceEditor.getInstance().getStringPreference(QuickstartPreferences.CURRENT_LANGUAGE);
            super.attachBaseContext(MyContextWrapper.wrap(newBase, lang));
        } else {
            super.attachBaseContext(newBase);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        frameLayoutCart = (FrameLayout) findViewById(R.id.frameLayoutCart);
        textViewCartCounter = (AppCompatTextView) findViewById(R.id.textViewCartCounter);
        iv_liked_products = (AppCompatImageView) findViewById(R.id.iv_liked_products);
        UserData = new Gson().fromJson(PreferenceEditor.getInstance().getStringPreference(QuickstartPreferences.USER_INFO), UserInfo.class);

        setSupportActionBar(toolbar);
        textViewCartCounter.setText(""+ToolsUtils.getAllCartProducts().size());

        frameLayoutCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onNavigationItemSelected(navigationView.getMenu().findItem(R.id.nav_my_cart));

            }
        });



        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        toggle.setDrawerIndicatorEnabled(false);
        toggle.setHomeAsUpIndicator(R.drawable.ic_nav_menu);
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(Gravity.START);
            }
        });

        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        iv_liked_products.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onNavigationItemSelected(navigationView.getMenu().findItem(R.id.nav_liked_products));
            }
        });


        checkUserLogin();


        if (getIntent().getExtras() == null) {
            onNavigationItemSelected(navigationView.getMenu().findItem(R.id.nav_home));
        } else if (getIntent().getExtras().getInt("nav") == R.id.nav_my_cart) {
            onNavigationItemSelected(navigationView.getMenu().findItem(R.id.nav_my_cart));
        }


    }

    private void checkUserLogin() {
        if (UserData != null) {

            navigationView.getMenu().findItem(R.id.nav_my_profile).setVisible(true);
            navigationView.getMenu().findItem(R.id.nav_change_password).setVisible(true);
            navigationView.getMenu().findItem(R.id.nav_my_purchases).setVisible(true);
             navigationView.getMenu().findItem(R.id.nav_logout).setVisible(true);
            navigationView.getMenu().findItem(R.id.nav_login).setVisible(false);
        } else {

            navigationView.getMenu().findItem(R.id.nav_my_profile).setVisible(false);
            navigationView.getMenu().findItem(R.id.nav_change_password).setVisible(false);
            navigationView.getMenu().findItem(R.id.nav_my_purchases).setVisible(false);
             navigationView.getMenu().findItem(R.id.nav_logout).setVisible(false);
            navigationView.getMenu().findItem(R.id.nav_login).setVisible(true);
        }

    }


    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (fragment instanceof ProductsFragment) {
            Log.e("flag", flag + "");
            switch (flag) {
                case 0:
                    flag = 1;
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.back_again_message), Toast.LENGTH_SHORT).show();
                    break;
                case 1:
                    finish();
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                    break;
            }
        } else {
            onNavigationItemSelected(navigationView.getMenu().findItem(R.id.nav_home));
        }
    }

    public void Navigate(int id) {
        onNavigationItemSelected(navigationView.getMenu().findItem(id));

    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        int id = item.getItemId();


        if (id == R.id.nav_home) {
            fragment = ProductsFragment.newInstance();

            toolbar_title.setText(R.string.app_name);
        } else if (id == R.id.nav_my_profile) {
            fragment = MyProfileFragment.newInstance();
            toolbar_title.setText(R.string.my_profile);
        } else if (id == R.id.nav_my_cart) {
            fragment = MyCartFragment.newInstance();
            toolbar_title.setText(R.string.my_cart);
        } else if (id == R.id.nav_my_purchases) {
            fragment = MyPurchasesFragment.newInstance();
            toolbar_title.setText(R.string.my_purchases);
        } else if (id == R.id.nav_liked_products) {
            fragment = LikedProductsFragment.newInstance();
            toolbar_title.setText(R.string.my_liked_products);
        } else if (id == R.id.nav_privacy_policy) {
            fragment = PrivacyPolicyFragment.newInstance();
            toolbar_title.setText(R.string.privacy_policy);
        } else if (id == R.id.nav_terms_conditions) {
            fragment = TermConditionsFragment.newInstance();
            toolbar_title.setText(R.string.terms_conditions);
        } else if (id == R.id.nav_change_password) {
            fragment = ChangePasswordFragment.newInstance();
            toolbar_title.setText(R.string.change_password);
        } else if (id == R.id.nav_logout) {
            showConfirmLogout();
        } else if (id == R.id.nav_login) {
            Login();
        } else if (id == R.id.nav_lang) {
            showSelectLanguageDialog();
        }


        //replacing the fragment
        if (fragment != null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.frameLayout, fragment)
                    .commit();
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void showSelectLanguageDialog() {

        Dialog dialog = new Dialog(HomeActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.select_language_dialog_layout);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        final RadioGroup lang_rg = (RadioGroup) dialog.findViewById(R.id.lang_rg);
        RadioButton arLang = (RadioButton) dialog.findViewById(R.id.ar_lang);
        RadioButton enLang = (RadioButton) dialog.findViewById(R.id.en_lang);
        Button saveSettingBtn = (Button) dialog.findViewById(R.id.save_setting_btn);
        lang = PreferenceEditor.getInstance().getStringPreference(QuickstartPreferences.CURRENT_LANGUAGE);

        if (lang == null) {
            ToolsUtils.changeAppLanguage(QuickstartPreferences.EN_LAN);
            lang = QuickstartPreferences.EN_LAN;
            enLang.setChecked(true);

        } else if (lang.equalsIgnoreCase(QuickstartPreferences.AR_LAN)) {
            arLang.setChecked(true);
        } else if (lang.equalsIgnoreCase(QuickstartPreferences.EN_LAN)) {
            enLang.setChecked(true);
        }


        saveSettingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String selectedText;

                switch (lang_rg.getCheckedRadioButtonId()) {
                    case R.id.ar_lang:
                        selectedText = QuickstartPreferences.AR_LAN;
                        break;
                    case R.id.en_lang:
                        selectedText = QuickstartPreferences.EN_LAN;
                        break;
                    default:
                        selectedText = QuickstartPreferences.EN_LAN;
                        break;
                }


                if (selectedText.equalsIgnoreCase(lang)) {
                    Toast.makeText(HomeActivity.this, R.string.no_data_changed, Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    ToolsUtils.changeAppLanguage(selectedText);
                    finish();
                    Intent i = new Intent(getApplicationContext(), SplashActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                    System.gc();

                }

            }
        });


        dialog.show();

    }

    private void Login() {
        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
    }

    private void Logout() {
        if (ToolsUtils.isNetworkAvailable()) {
            final AVLoadingIndicatorDialog dialog = ToolsUtils.CreateDialog(this, null);
            dialog.show();
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            Call<RequestResponse> call = apiService.logout();
            call.enqueue(new Callback<RequestResponse>() {
                @Override
                public void onResponse(Call<RequestResponse> call, Response<RequestResponse> response) {
                    System.out.println("Code" + response.code());
                    System.out.println("raw" + response.raw());
                    System.out.println("Code" + response.message());
                    dialog.hide();
                    if (response.code() == 200 && response.body().isStatus()) {
                        UserData = null;
                        PreferenceEditor.getInstance().removePreference(QuickstartPreferences.USER_INFO);
                        RemoveAllCartItems();
                        Payment.deleteAll(Payment.class);
                        checkUserLogin();
                        Navigate(R.id.nav_home);
                        Toast.makeText(HomeActivity.this, R.string.msg_logout_success, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<RequestResponse> call, Throwable t) {
                    dialog.hide();
                    Toast.makeText(getApplicationContext(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(getApplication(), R.string.err_msg_no_internet_connection, Toast.LENGTH_SHORT).show();
        }
    }

    private void showConfirmLogout() {
        AlertDialog alertDialog = new AlertDialog.Builder(this, R.style.MyAlertDialogStyle)
                .setMessage(R.string.msg_logout)
                .setCancelable(true)
                .setPositiveButton(R.string.yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();

                                Logout();

                            }
                        }).setNegativeButton(R.string.cancel, null).show();
        //   Typeface face = Typeface.createFromAsset(getAssets(), "mohand_font.ttf");
        // Typeface face1 = Typeface.createFromAsset(getAssets(), "mohand_font.ttf");
        alertDialog.show();
        // alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTypeface(face1);
        //  alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTypeface(face1);

        //((android.widget.TextView) alertDialog.findViewById(android.R.id.message)).setTypeface(face);

    }


    @Override
    protected void onResume() {
        super.onResume();

        checkUserLogin();
        textViewCartCounter.setText(""+ToolsUtils.getAllCartProducts().size());
    }

    private void RemoveAllCartItems() {
        try {
            List<Products.ProductsBean.DataBean> list = Products.ProductsBean.DataBean.listAll(Products.ProductsBean.DataBean.class);

            if (list == null) {
                list = new ArrayList<>();
            }

            Products.ProductsBean.DataBean.deleteAll(Products.ProductsBean.DataBean.class);

            textViewCartCounter.setText("" + 0);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onCartChanged() {
        textViewCartCounter.setText(""+ToolsUtils.getAllCartProducts().size());
    }

    @Override
    public void onItemChanged(int position, Products.ProductsBean.DataBean p) {
        Toast.makeText(this, "onItemChanged", Toast.LENGTH_SHORT).show();
    }
}
